using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;


[System.Serializable]
public class bool_xyz
{
    public bool width = false;
    public bool depth = false;
    public bool height = false;
}


public enum w_placement : int
{
    top_front = 0,
    bottom_front = 1,
    top_back = 2,
    bottom_back = 3
}
;
public enum d_placement
{
    top_right = 0,
    bottom_right = 1,
    top_left = 2,
    bottom_left = 3
}
;
public enum h_placement
{
    front_right = 0,
    front_left = 1,
    back_right = 2,
    back_left = 3
}
;

public class DimBoxes_DimBox : MonoBehaviour
{

    public bool colliderBased = false;
    public bool permanent = false; //permanent/onMouseDown
    public w_placement widthPlacement;
    public d_placement depthPlacement;
    public h_placement heightPlacement;

    public bool_xyz extensions;
    public bool_xyz faceCamera;

    public Color lineColor = new Color(0f, 1f, 0.4f, 0.74f);
    public float extentionDist = 0.1f;
    public Material letterMaterial;
    public Font Font1;

    private Vector3 topFrontLeft;
    private Vector3 topFrontRight;
    private Vector3 topBackLeft;
    private Vector3 topBackRight;
    private Vector3 bottomFrontLeft;
    private Vector3 bottomFrontRight;
    private Vector3 bottomBackLeft;
    private Vector3 bottomBackRight;

    private Vector3[] widthExt;

    private Vector3[] depthExt;
    private Vector3 depthLookAt;
    private Vector3[] heightExt;

    public GameObject hDimension;
    public GameObject dDimension;
    public GameObject wDimension;
    private GameObject hDimensionMesh;
    private GameObject wDimensionMesh;

    public string hDimensionText = null;
    public string dDimensionText = null;
    public string wDimensionText = null;


    private float alpha;

    private Bounds bound;

    private Vector3[] corners;

    private Vector3[,] lines;

    private Quaternion quat;

    private Camera mcamera;

    private BoundBoxes_drawLines cameralines;

    private Renderer[] renderers;
    private MeshFilter[] meshes;

    private Material[][] Materials;

    private DimBoxes_DimBoxSettings settingScript;

    private bool highlighted = false;

    void Awake()
    {
        DoStartup(false);
    }

    public void DoStartup(bool doInit)
    {
        renderers = GetComponentsInChildren<Renderer>();
        meshes = GetComponentsInChildren<MeshFilter>();
        Materials = new Material[renderers.Length][];
        for (int i = 0; i < renderers.Length; i++)
        {
            Materials[i] = renderers[i].materials;
        }
        if (doInit)
        {
            init();
        }
    }

    void Start()
    {
        ChangeCamera(Camera.main, false);
        init();
    }

    public void ChangeCamera(Camera newCamera, bool doInit)
    {
        mcamera = Camera.main;
        settingScript = FindObjectOfType(typeof(DimBoxes_DimBoxSettings)) as DimBoxes_DimBoxSettings;
        cameralines = mcamera.GetComponent<BoundBoxes_drawLines>();
        calculateBounds();
        if (doInit)
        {
            init();
        }
    }

    public void init()
    {
        if (mcamera != Camera.main)
        {
            ChangeCamera(Camera.main, false);
        }
        else
        {
            calculateBounds();
        }
        if (hDimension)
            DestroyImmediate(hDimension);
        if (wDimension)
            DestroyImmediate(wDimension);
        if (dDimension)
            DestroyImmediate(dDimension);
        setPoints();
        setLines();
        cameralines.setOutlines(lines, lineColor);
        addText();
    }
    void LateUpdate()
    {
        cameralines.setOutlines(lines, lineColor);
    }

    void calculateBounds()
    {
        quat = transform.rotation;//object axis AABB
        if (renderers.Length > 0)
        {
            if (renderers[0].isPartOfStaticBatch)
                quat = Quaternion.Euler(0f, 0f, 0f);//world axis

            if (colliderBased)
            {
                BoxCollider coll = GetComponent<BoxCollider>();
                if (coll)
                {
                    if (!renderers[0].isPartOfStaticBatch)
                    {
                        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                    }
                    bound = coll.bounds;
                    transform.rotation = quat;
                }
                else
                {
                    //Debug.Log("No collider attached");
                }
                return;
            }
            bound = new Bounds();
            if (renderers[0].isPartOfStaticBatch)
            {
                bound = renderers[0].bounds;
                for (int i = 1; i < renderers.Length; i++)
                {
                    bound.Encapsulate(renderers[i].bounds);
                }
                return;
            }
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            for (int i = 0; i < meshes.Length; i++)
            {
                Mesh ms = meshes[i].mesh;
                Vector3 tr = meshes[i].gameObject.transform.position;
                Vector3 ls = meshes[i].gameObject.transform.lossyScale;
                Quaternion lr = meshes[i].gameObject.transform.rotation;
                int vc = ms.vertexCount;
                for (int j = 0; j < vc; j++)
                {
                    if (i == 0 && j == 0)
                    {
                        bound = new Bounds(tr + lr * Vector3.Scale(ls, ms.vertices[j]), Vector3.zero);
                    }
                    else
                    {
                        bound.Encapsulate(tr + lr * Vector3.Scale(ls, ms.vertices[j]));
                    }
                }
            }
        }
        transform.rotation = quat;
    }

    void setPoints()
    {

        Vector3 bc = transform.position + quat * (bound.center - transform.position);

        topFrontRight = bc + quat * Vector3.Scale(bound.extents, new Vector3(1, 1, 1));
        topFrontLeft = bc + quat * Vector3.Scale(bound.extents, new Vector3(-1, 1, 1));
        topBackLeft = bc + quat * Vector3.Scale(bound.extents, new Vector3(-1, 1, -1));
        topBackRight = bc + quat * Vector3.Scale(bound.extents, new Vector3(1, 1, -1));
        bottomFrontRight = bc + quat * Vector3.Scale(bound.extents, new Vector3(1, -1, 1));
        bottomFrontLeft = bc + quat * Vector3.Scale(bound.extents, new Vector3(-1, -1, 1));
        bottomBackLeft = bc + quat * Vector3.Scale(bound.extents, new Vector3(-1, -1, -1));
        bottomBackRight = bc + quat * Vector3.Scale(bound.extents, new Vector3(1, -1, -1));
        corners = new Vector3[] {
						topFrontRight,
						topFrontLeft,
						topBackLeft,
						topBackRight,
						bottomFrontRight,
						bottomFrontLeft,
						bottomBackLeft,
						bottomBackRight
				};

        widthExt = new Vector3[4];
        switch (widthPlacement)
        {//top_front,bottom_front,top_back,bottom_back
            case w_placement.top_front:
                widthExt[0] = corners[1];
                widthExt[1] = corners[1] + (extensions.width ? extentionDist : 0) * (corners[1] - corners[0]).normalized;
                widthExt[2] = corners[2] + (extensions.width ? extentionDist : 0) * (corners[2] - corners[3]).normalized;
                widthExt[3] = corners[2];
                break;
            case w_placement.bottom_front:
                widthExt[0] = corners[5];
                widthExt[1] = corners[5] + (extensions.width ? extentionDist : 0) * (corners[5] - corners[4]).normalized;
                widthExt[2] = corners[6] + (extensions.width ? extentionDist : 0) * (corners[6] - corners[7]).normalized;
                widthExt[3] = corners[6];
                break;
            case w_placement.top_back:
                widthExt[0] = corners[3];
                widthExt[1] = corners[3] + (extensions.width ? extentionDist : 0) * (corners[3] - corners[2]).normalized;
                widthExt[2] = corners[0] + (extensions.width ? extentionDist : 0) * (corners[0] - corners[1]).normalized;
                widthExt[3] = corners[0];
                break;
            case w_placement.bottom_back:
                widthExt[0] = corners[7];
                widthExt[1] = corners[7] + (extensions.width ? extentionDist : 0) * (corners[7] - corners[6]).normalized;
                widthExt[2] = corners[4] + (extensions.width ? extentionDist : 0) * (corners[4] - corners[5]).normalized;
                widthExt[3] = corners[4];
                break;
            default:
                break;
        }
        depthExt = new Vector3[4];
        switch (depthPlacement)
        {//top_right=0,bottom_right=1,top_left=2,bottom_left=3
            case d_placement.top_right:
                depthExt[0] = corners[2];
                depthExt[1] = corners[2] + (extensions.depth ? extentionDist : 0) * (corners[2] - corners[1]).normalized;
                depthExt[2] = corners[3] + (extensions.depth ? extentionDist : 0) * (corners[3] - corners[0]).normalized;
                depthExt[3] = corners[3];
                break;
            case d_placement.bottom_right:
                depthExt[0] = corners[6];
                depthExt[1] = corners[6] + (extensions.depth ? extentionDist : 0) * (corners[6] - corners[5]).normalized;
                depthExt[2] = corners[7] + (extensions.depth ? extentionDist : 0) * (corners[7] - corners[4]).normalized;
                depthExt[3] = corners[7];
                break;
            case d_placement.top_left:
                depthExt[0] = corners[0];
                depthExt[1] = corners[0] + (extensions.depth ? extentionDist : 0) * (corners[0] - corners[3]).normalized;
                depthExt[2] = corners[1] + (extensions.depth ? extentionDist : 0) * (corners[1] - corners[2]).normalized;
                depthExt[3] = corners[1];
                break;
            case d_placement.bottom_left:
                depthExt[0] = corners[4];
                depthExt[1] = corners[4] + (extensions.depth ? extentionDist : 0) * (corners[4] - corners[7]).normalized;
                depthExt[2] = corners[5] + (extensions.depth ? extentionDist : 0) * (corners[5] - corners[6]).normalized;
                depthExt[3] = corners[5];
                break;
            default:
                break;
        }

        heightExt = new Vector3[4];
        switch (heightPlacement)
        {//front_right=0,front_left=1,back_right=2,back_left=3
            case h_placement.front_right:
                heightExt[0] = corners[2];
                heightExt[1] = corners[2] + (extensions.height ? extentionDist : 0) * (corners[2] - corners[3]).normalized;
                heightExt[2] = corners[6] + (extensions.height ? extentionDist : 0) * (corners[6] - corners[7]).normalized;
                heightExt[3] = corners[6];
                break;
            case h_placement.front_left:
                heightExt[0] = corners[5];
                heightExt[1] = corners[5] + (extensions.height ? extentionDist : 0) * (corners[5] - corners[4]).normalized;
                heightExt[2] = corners[1] + (extensions.height ? extentionDist : 0) * (corners[1] - corners[0]).normalized;
                heightExt[3] = corners[1];
                break;
            case h_placement.back_right:
                heightExt[0] = corners[3];
                heightExt[1] = corners[3] + (extensions.height ? extentionDist : 0) * (corners[3] - corners[0]).normalized;
                heightExt[2] = corners[7] + (extensions.height ? extentionDist : 0) * (corners[7] - corners[4]).normalized;
                heightExt[3] = corners[7];
                break;
            case h_placement.back_left:
                heightExt[0] = corners[4];
                heightExt[1] = corners[4] + (extensions.height ? extentionDist : 0) * (corners[4] - corners[5]).normalized;
                heightExt[2] = corners[0] + (extensions.height ? extentionDist : 0) * (corners[0] - corners[1]).normalized;
                heightExt[3] = corners[0];
                break;
            default:
                break;
        }
    }

    void setLines()
    {

        List<Vector3[]> _lines = new List<Vector3[]>();
        //int linesCount = 12;
        //linesCount += (extensions.depth ? 3:0) + (extensions.width ? 3:0) + (extensions.height ? 3:0);
        //int i1;
        //return;
        //lines = new Vector3[linesCount,2];
        Vector3[] _line;
        for (int i = 0; i < 4; i++)
        {
            //width
            _line = new Vector3[] { corners[2 * i], corners[2 * i + 1] };
            _lines.Add(_line);
            //height
            _line = new Vector3[] { corners[i], corners[i + 4] };
            _lines.Add(_line);
            //depth
            _line = new Vector3[] { corners[2 * i], corners[2 * i + 3 - 4 * (i % 2)] };
            _lines.Add(_line);

            //i1 = i + 4;//heightlines
            //lines[i+4,0] = corners[i];
            //lines[i+4,1] = corners[i1];
            //bottom rectangle
            //lines[i+8,0] = corners[i1];
            //i1 = 4 + (i+1)%4;
            //lines[i+8,1] = corners[i1];
        }
        //int j = 12;
        if (extensions.width)
        {
            _lines.Add(new Vector3[] { widthExt[0], widthExt[1] });
            _lines.Add(new Vector3[] { widthExt[1], widthExt[2] });
            _lines.Add(new Vector3[] { widthExt[2], widthExt[3] });
            //lines[12,0] = widthExt[0]; lines[12,1] = widthExt[1];
            //lines[13,0]= widthExt[1]; lines[13,1]= widthExt[2];
            //lines[14,0]= widthExt[2]; lines[14,1]= widthExt[3];
            //j += 3;
        }
        if (extensions.depth)
        {
            _lines.Add(new Vector3[] { depthExt[0], depthExt[1] });
            _lines.Add(new Vector3[] { depthExt[1], depthExt[2] });
            _lines.Add(new Vector3[] { depthExt[2], depthExt[3] });
            //lines[j,0]= depthExt[0]; lines[j,1] = depthExt[1];
            //lines[j+1,0]= depthExt[1]; lines[j+1,1] = depthExt[2];
            //lines[j+2,0]= depthExt[2]; lines[j+2,1] = depthExt[3];
            //j += 3;
        }
        if (extensions.height)
        {
            _lines.Add(new Vector3[] { heightExt[0], heightExt[1] });
            _lines.Add(new Vector3[] { heightExt[1], heightExt[2] });
            _lines.Add(new Vector3[] { heightExt[2], heightExt[3] });
        }
        lines = new Vector3[_lines.Count, 2];
        for (int j = 0; j < _lines.Count; j++)
        {
            lines[j, 0] = _lines[j][0];
            lines[j, 1] = _lines[j][1];
        }
    }
    void OnDisable()
    {
        if (hDimension != null)
        {
            hDimension.SetActive(false);
        }
        if (wDimension != null)
        {
            wDimension.SetActive(false);
        }
        if (dDimension != null)
        {
            dDimension.SetActive(false);
        }
    }

    void OnEnable()
    {
        if (hDimension != null)
        {
            hDimension.SetActive(true);
        }
        if (wDimension != null)
        {
            wDimension.SetActive(true);
        }
        if (dDimension != null)
        {
            dDimension.SetActive(true);
        }
    }


    void addText()
    {

        hDimension = new GameObject("hDimension");  // the height Dimension 
        hDimensionMesh = new GameObject();
        hDimension.transform.position = Vector3.Lerp(heightExt[1], heightExt[2], 0.5f);

        hDimension.transform.parent = transform;
        hDimension.transform.localRotation = Quaternion.Euler(Vector3.zero);

        hDimensionMesh.transform.position = Vector3.Lerp(heightExt[1], heightExt[2], 0.5f);

        hDimensionMesh.transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
        TextMesh hm = hDimensionMesh.AddComponent<TextMesh>();
        hm.anchor = TextAnchor.LowerCenter;
        hm.font = Font1;
        hm.GetComponent<Renderer>().material = letterMaterial;
        hm.text = !string.IsNullOrEmpty(hDimensionText) ? hDimensionText : (100 * Vector3.Distance(heightExt[1], heightExt[2])).ToString("#0.0") + " cm";
        hDimension.transform.parent = transform;
        hDimensionMesh.transform.parent = hDimension.transform;
        hDimension.transform.LookAt(heightExt[2], hDimension.transform.right);
        hDimensionMesh.transform.localRotation = Quaternion.Euler(new Vector3(90f, 90f, 0));
        if (faceCamera.height)
        {
            hDimensionMesh.transform.localRotation = Quaternion.Euler(new Vector3(180f, 0, -90f));
        }
        else
        {
            hDimensionMesh.transform.localRotation = Quaternion.Euler(new Vector3(90f, 90f, 0));
        }



        wDimension = new GameObject("wDimension");  // the width Dimension
        wDimensionMesh = new GameObject();
        wDimension.transform.position = Vector3.Lerp(widthExt[1], widthExt[2], 0.5f);
        wDimensionMesh.transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
        TextMesh wm = wDimensionMesh.AddComponent<TextMesh>();
        wm.anchor = TextAnchor.LowerCenter;
        wm.font = Font1;
        wm.GetComponent<Renderer>().material = letterMaterial;
        wm.text = !string.IsNullOrEmpty(wDimensionText) ? wDimensionText : (100 * Vector3.Distance(widthExt[1], widthExt[2])).ToString("#0.0") + " cm";
        wDimension.transform.parent = transform;
        wDimensionMesh.transform.parent = wDimension.transform;
        wDimensionMesh.transform.localPosition = Vector3.zero;
        wDimension.transform.LookAt(widthExt[2]);
        if (faceCamera.width)
        {
            wDimensionMesh.transform.localRotation = Quaternion.Euler(new Vector3(180f, 0, -90f));
        }
        else
        {
            wDimensionMesh.transform.localRotation = Quaternion.Euler(new Vector3(90f, 90f, 0));
        }

        dDimension = new GameObject("dDimension");  // the depth Dimension
        GameObject dDimensionMesh = new GameObject();
        dDimension.transform.position = Vector3.Lerp(depthExt[1], depthExt[2], 0.5f);
        dDimensionMesh.transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
        TextMesh dm = dDimensionMesh.AddComponent<TextMesh>();
        dm.anchor = TextAnchor.LowerCenter;
        dm.font = Font1;
        dm.GetComponent<Renderer>().material = letterMaterial;
        dm.text = !string.IsNullOrEmpty(dDimensionText) ? dDimensionText : (100 * Vector3.Distance(depthExt[1], depthExt[2])).ToString("#0.0") + " cm";
        dDimension.transform.parent = transform;
        dDimensionMesh.transform.parent = dDimension.transform;
        dDimensionMesh.transform.localPosition = Vector3.zero;
        dDimension.transform.LookAt(depthExt[2]);
        dDimensionMesh.transform.localRotation = Quaternion.Euler(new Vector3(90f, 90f, 0));
        if (faceCamera.depth)
        {
            dDimensionMesh.transform.localRotation = Quaternion.Euler(new Vector3(180f, 0, -90f));
        }
        else
        {
            dDimensionMesh.transform.localRotation = Quaternion.Euler(new Vector3(90f, 90f, 0));
        }
    }

    void Update()
    {

        if (mcamera != Camera.main)
        {
            ChangeCamera(Camera.main, true);
        }
        Vector3 campos = mcamera.transform.position;

        if (faceCamera.height && hDimension)
        {
            hDimension.transform.LookAt(campos, heightExt[2] - heightExt[1]);
            //if(mcamera.transform.InverseTransformDirection(hDimension.transform.up).x<0) hDimension.transform.Rotate(0,0,180f);
        }
        if (faceCamera.width && wDimension)
        {
            wDimension.transform.LookAt(campos, widthExt[2] - widthExt[1]);
            if (mcamera.transform.InverseTransformDirection(wDimension.transform.up).x < 0)
                wDimension.transform.Rotate(0, 0, 180f);
        }
        if (faceCamera.depth && dDimension)
        {
            dDimension.transform.LookAt(campos, depthExt[2] - depthExt[1]);
            if (mcamera.transform.InverseTransformDirection(dDimension.transform.up).x < 0)
                dDimension.transform.Rotate(0, 0, 180f);
        }

    }

    //		void OnMouseOver ()
    //		{
    //				if (Input.GetMouseButtonDown (1)) {
    //						if (settingScript)
    //								settingScript.newSettings (gameObject);
    //				}
    //		}	
    //	
    //		void OnMouseDown ()
    //		{
    //				if (permanent || highlighted)
    //						return;
    //				enabled = !enabled;
    //				hDimension.SetActive (enabled);
    //				dDimension.SetActive (enabled);
    //				wDimension.SetActive (enabled);
    //		}

    public void highlight(bool val)
    {
        highlighted = val;
        if (val)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                Material[] rmats = renderers[i].materials;
                int n = rmats.Length;
                Material[] hmats = new Material[n];
                for (int j = 0; j < n; j++)
                {
                    hmats[j] = new Material(Materials[i][j]);
                    Color hcolor = new Vector4(0, 0, 0, 0);
                    hcolor = hmats[j].color + Color.green;
                    hmats[j].color = hcolor;
                }
                renderers[i].materials = hmats;
            }
        }
        else
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].materials = Materials[i];
            }
        }
    }
}
