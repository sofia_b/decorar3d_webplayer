﻿using UnityEngine;
using System.Collections;

public class DimBoxes_DimBoxSettings : MonoBehaviour {
	private GameObject toBeDimensioned;
	public GUISkin skin1;
	private DimBoxes_DimBox dimbox;
	private bool inAxis;//colliderBased
	private bool[] extensions = new bool[3]{false,false,false};
	private bool[] faceCamera = new bool[3]{false,false,true};
	private int width_placement = 0;
	private int depth_placement = 0;
	private int height_placement = 0;
	private GUIContent[] width_options;// {"top_front","bottom_front","top_back","bottom_back"};
	private GUIContent[] depth_options;//top_right=0,bottom_right=1,top_left=2,bottom_left=3
	private GUIContent[] height_options;//front_right=0,front_left=1,back_right=2,back_left=3
	
	private string tempName = "";
	// Use this for initialization
	void Start () {
		if(toBeDimensioned) readSettings(toBeDimensioned);
	}
	
	public void newSettings (GameObject go) {
		if(dimbox) dimbox.highlight(false);
		if(go == toBeDimensioned) {
			toBeDimensioned = null;
			dimbox = null;
			}else{
			toBeDimensioned = go;
			readSettings(toBeDimensioned);
		}
	}
	
	void readSettings (GameObject go) {
		tempName = go.name;
		if(tempName.Length>16) tempName = tempName.Substring(0,16);
		dimbox = go.GetComponent<DimBoxes_DimBox>();
		if(dimbox) {
			dimbox.highlight(true);
			inAxis = dimbox.colliderBased;
			w_placement wd = dimbox.widthPlacement;
			width_placement = (int)wd;
			width_options = new GUIContent[]{new GUIContent("top_front"),new GUIContent("bottom_front"),new GUIContent("top_back"),new GUIContent("bottom_back")};
			depth_options = new GUIContent[]{new GUIContent("top_right"),new GUIContent("bottom_right"),new GUIContent("top_left"),new GUIContent("bottom_left")};
			height_options = new GUIContent[]{new GUIContent("front_right"),new GUIContent("front_left"),new GUIContent("back_right"),new GUIContent("back_left")};
			extensions[0] = dimbox.extensions.width;
			extensions[1] = dimbox.extensions.depth;
			extensions[2] = dimbox.extensions.height;
			faceCamera[0] = dimbox.faceCamera.width;
			faceCamera[1] = dimbox.faceCamera.depth;
			faceCamera[2] = dimbox.faceCamera.height;
		}
	}
	
	// Update is called once per frame
	void OnGUI () {
		
		GUI.skin = skin1;
		if(!dimbox) {
				GUI.Box(new Rect(20,20,380,40),"right-click on the GameObject to open the settings \n left-click to toggle on/off the dimension boxes");
			}else{	
			Rect grouprect = new Rect(20,20,380,165);
			GUI.BeginGroup (grouprect);
				GUI.Box(new Rect(0,0,380,200),"DIMENSION BOX SETTINGS on " + tempName);
				inAxis = GUI.Toggle (new Rect (10, 25, 60, 25), inAxis, "inAxis");//colliderBased
				GUI.Label (new Rect (105, 25, 80, 25), "extensions");
				GUI.Label (new Rect (235, 25, 80, 25), "faceCamera");
				
				extensions[0]=GUI.Toggle (new Rect (100, 47, 30, 25), extensions[0], "W"); 
				extensions[1]=GUI.Toggle (new Rect (130, 47, 30, 25), extensions[1], "D"); 
				extensions[2]=GUI.Toggle (new Rect (160, 47, 30, 25), extensions[2], "H"); 
				
				faceCamera[0]=GUI.Toggle (new Rect (230, 47, 30, 25), faceCamera[0], "W"); 
				faceCamera[1]=GUI.Toggle (new Rect (260, 47, 30, 25), faceCamera[1], "D"); 
				faceCamera[2]=GUI.Toggle (new Rect (290, 47, 30, 25), faceCamera[2], "H");
				GUI.Label (new Rect (55, 75, 280, 20), "DIMENSION PLACEMENT");
				GUI.Label (new Rect (10, 97, 40, 18), "Width");
				width_placement = GUI.SelectionGrid(new Rect(50, 97, 300, 18), width_placement, width_options,4);
				GUI.Label (new Rect (10, 117, 40, 18), "Depth");
				depth_placement = GUI.SelectionGrid(new Rect(50, 117, 300, 18), depth_placement, depth_options,4);
				GUI.Label (new Rect (10, 137, 40, 18), "Height");
				height_placement = GUI.SelectionGrid(new Rect(50, 137, 300, 18), height_placement, height_options,4);
			
			GUI.EndGroup ();
			if(GUI.changed) {
				dimbox.colliderBased = inAxis;
				dimbox.extensions.width = extensions[0];
				dimbox.extensions.depth = extensions[1];
				dimbox.extensions.height = extensions[2];
				dimbox.faceCamera.width = faceCamera[0];
				dimbox.faceCamera.depth = faceCamera[1];
				dimbox.faceCamera.height = faceCamera[2];
				dimbox.widthPlacement = (w_placement)width_placement;
				dimbox.depthPlacement = (d_placement)depth_placement;
				dimbox.heightPlacement = (h_placement)height_placement;
				dimbox.init();
			
			}
		}
	}
	
}
