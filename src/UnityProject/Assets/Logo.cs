﻿using UnityEngine;
using System.Collections;
//using SimpleJSON;
using System;
using System.Linq;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
//using LitJson;


public class Logo : MonoBehaviour {
	//Usuario/vendedor
	
	
	public string IdVendedor;
	public string LogoVendedor;
	//public string idpresupuesto;
	//public GameObject Perfil;
	public GameObject SM;
	//public Image ImagenPerfil;
//	public Sprite exampleSprite;
//	public Color color1;
	string Test1 ;
	SceneManager ManagerScript;
	// Use this for initialization
	
	void start(){
		//IdVendedor = SM.GetComponent<SceneManager> ().LogoId;
	
		//A URL where the image is stored
	//	LogoVendedor = SM.GetComponent<SceneManager> ().LogoImagen;

		//StartCoroutine(RealLoadImage());
		//String urlTest = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Orange_trademark.svg/64px-Orange_trademark.svg.png";
		Test1 = "ResourcescolorP.png";
	}
	public void CargarLogo(){
		IdVendedor = SM.GetComponent<SceneManager> ().LogoId;
		
		//A URL where the image is stored
		LogoVendedor = SM.GetComponent<SceneManager> ().LogoImagen;
		StartCoroutine(RealLoadImage());

	}
	//This is where the actual code is executed
	private IEnumerator RealLoadImage () {
			//Call the WWW class constructor
		WWW imageURLWWW = new WWW(LogoVendedor);  
	
		
		//Wait for the download
		yield return imageURLWWW;        
		
		//Simple check to see if there's indeed a texture available

		if(imageURLWWW.texture != null) {
			
			
			//Construct a new Sprite
			Sprite sprite = new Sprite();    
			float Height;
			float Width;
			//Create a new sprite using the Texture2D from the url.
			//Note that the 400 parameter is the width and height.
			//Adjust accordingly
			// sprite = Sprite.Create(imageURLWWW.texture, new Rect(0, 0, 700, 700), Vector2.zero);  
			sprite = Sprite.Create(imageURLWWW.texture, new Rect(0, 0, imageURLWWW.texture.width, imageURLWWW.texture.height), Vector2.zero);  
			
			//Assign the sprite to the Image Component
			GetComponent<UnityEngine.UI.Image>().sprite = sprite;  

			this.GetComponent<Image>().sprite = sprite;

		}
		
		yield return null;
	}
	
}
