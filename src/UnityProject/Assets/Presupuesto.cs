﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;
using System.Linq;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LitJson;



public class Presupuesto : MonoBehaviour {
	
	//Datos del comerciante
	public UnityEngine.UI.InputField Fecha;
	public UnityEngine.UI.InputField Vendedor;
	//Datos del cliente
	public UnityEngine.UI.InputField Nombre;
	public UnityEngine.UI.InputField Apellido;
	public UnityEngine.UI.InputField Telefono;
	public UnityEngine.UI.InputField Direccion;
	public UnityEngine.UI.InputField email;
	
	/////////////////////////////////
	
	// 200x300 px window will apear in the center of the screen.
	private Rect windowRect = new Rect ((Screen.width - 200)/2, (Screen.height - 300)/2, 200, 300);
	// Only show it if needed.
	private bool show = false;
	
	/////////////////////	
	
	string CatalogueURLPres;
	public string FechaFinal;
	public string VendedorFinal;
	public string NombreFinal;
	public string ApellidoFinal;
	public string TelefonoFinal;
	public string DireccionFinal;
	public string EmailFinal;
	public string DataVendedor;
	public string DataCliente;
	public string DataToExportFinal;
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	GameObject Items1;
	public GameObject childNombre;
	public GameObject childID;
	public GameObject childCantidad;
	public GameObject childPrecio;
	public GameObject childWidht;
	public GameObject childHeight;
	public GameObject childDeep;
	
	public GameObject TotalText;
	
	
	public List<ListItemPresupuesto> listaDeItemsPresupuesto = new List<ListItemPresupuesto>();
	public string DataToExportToDatabase;
	
	//captura Presupuesto
	
	private string _data = string.Empty;
	public Texture2D bg1;
	public string FeaturedPropertyName1 = "Featured";
	//---screenshot smb
	public string imageName1 = "Screenshot_";
	public string customPath1 = "";/*"C:/Users/default/Desktop/UnityScreenshots/";*/ // leave blank for project file location
	public int resolution1 = 3; // 1= default, 2= 2x default, etc.
	
	//fin captura presupuesto
	
	public Text Date;
	String theDate;
	int idint;
	
	
	//LitJson
	public JsonData itemData;

	public GameObject AuxPress;

	public bool DecoracionAux;
	public bool AberturasAux;
	public bool IluminacionAux;
	public bool ElectrodomesticosAux;
	public bool MueblesAux;

	//
	/*public bool DecoracionAux;
	public bool Aberturas;
	public bool Iluminacion;
	public bool Electrodomesticos;
	//AuxPres auxValue;
*/

	public void UpdatePanelPresupuestos () {

		DecoracionAux = AuxPress.gameObject.GetComponent<AuxPres> ().Decoracion;
		AberturasAux = AuxPress.gameObject.GetComponent<AuxPres> ().Aberturas;
		IluminacionAux = AuxPress.gameObject.GetComponent<AuxPres> ().Iluminacion;
		ElectrodomesticosAux = AuxPress.gameObject.GetComponent<AuxPres> ().Electrodomesticos;
		MueblesAux = AuxPress.gameObject.GetComponent<AuxPres> ().Muebles;
		
		theDate	= System.DateTime.Now.ToString("dd-MM-yyyy"); 
		Date.text = theDate;
		
		Items1 = GameObject.FindWithTag("Items");
		Items1.GetComponent<PresupuestoItems>().UpdatePresupuestoItems();
		listaDeItemsPresupuesto.Clear();
		foreach (ListItems ElementoItemsLista in (Items1.GetComponent<PresupuestoItems>().listaDeItems)) 
		{
			bool contar = false;
			if(ElementoItemsLista.Cat=="Decoracion" && DecoracionAux){
				contar=true;
			}else{
				if(ElementoItemsLista.Cat=="Aberturas" && AberturasAux){
					contar=true;
				}else{
					if(ElementoItemsLista.Cat=="Iluminacion" && IluminacionAux){
						contar=true;
					}else{
						if(ElementoItemsLista.Cat=="Electrodomesticos" && ElectrodomesticosAux){
							contar=true;
						}else{
							if(ElementoItemsLista.Cat=="Muebles" && MueblesAux){
								contar=true;
							}else{
							}
						}
					}
				}
			}
			/*
//this.tag = "Decoracion";
//this.tag = "Aberturas";
this.tag = "Electrodomesticos";
//this.tag = "Iluminacion";
this.tag = "Muebles"
			  */
			if(contar){
			if(listaDeItemsPresupuesto.Count==0){
				listaDeItemsPresupuesto.Add(new ListItemPresupuesto(ElementoItemsLista.Nombre,ElementoItemsLista.idI,1,ElementoItemsLista.Width, ElementoItemsLista.Height, ElementoItemsLista.Deep));
			}else{
				idint=0;
				bool NeedAddNewItem=true;
				foreach (ListItemPresupuesto ElementoItemPresupuesto in listaDeItemsPresupuesto)
				{
					if(ElementoItemsLista.idI==ElementoItemPresupuesto.idI){
						listaDeItemsPresupuesto[idint].Cantidad++;
						
						NeedAddNewItem=false;
					}else{
					}
					idint++;
				}
				if(NeedAddNewItem){
					listaDeItemsPresupuesto.Add(new ListItemPresupuesto(ElementoItemsLista.Nombre,ElementoItemsLista.idI,1, ElementoItemsLista.Width, ElementoItemsLista.Height, ElementoItemsLista.Deep));
					NeedAddNewItem=false;
				}
			}
			}
		}
		
		childNombre.GetComponent<ThumbGridPresupNombre>().InitListNombres();
		childID.GetComponent<ThumbGridPresupID>().InitListID();
		childCantidad.GetComponent<ThumbGridPresupCantidad>().InitListCantidad();
		childPrecio.GetComponent<ThumbGridPresupPrecio>().InitListPrecio();
		
		childNombre.GetComponent<ThumbGridPresupNombre>().LoadThumbs();
		childID.GetComponent<ThumbGridPresupID>().LoadThumbs();
		childCantidad.GetComponent<ThumbGridPresupCantidad>().LoadThumbs();
		childPrecio.GetComponent<ThumbGridPresupPrecio>().LoadThumbs();
	}
	
	public float TotalPres;// valor total de la sumatoria de los precios
	int cantidadMueble; //Variable para almacenar la cantidad de muebles del mismo tipo

	void Update(){
		GenerateDataToDatabase();
	
	}
	public void GenerateDataToDatabase () {
		DataToExportToDatabase = "";
		TotalPres = 0;
		foreach (ListItemPresupuesto ElementoItemPresupuesto in listaDeItemsPresupuesto)
		{
			
			//DataToExportToDatabase=DataToExportToDatabase+ElementoItemPresupuesto.idI+"_"+ElementoItemPresupuesto.Cantidad+"_"+ElementoItemPresupuesto.Widht+"_"+ElementoItemPresupuesto.Height+"_"+ElementoItemPresupuesto.Deep;
			DataToExportToDatabase=DataToExportToDatabase+ElementoItemPresupuesto.idI+"_"+ElementoItemPresupuesto.Cantidad;
			
			bool a=true;
			cantidadMueble = ElementoItemPresupuesto.Cantidad;//Tomo la cantidad de de muebles con el mismo id y lo asigno a una nueva variable entera
			
			foreach(Transform child in childPrecio.transform)
			{
				//Debug.Log(child.gameObject.name);
				if(child.gameObject.name == "Precio-"+ElementoItemPresupuesto.Nombre ){
					DataToExportToDatabase=DataToExportToDatabase+"_"+child.GetComponentInChildren<Text>().text;
					a=false;
					//
					//Debug.Log("cantidad= "+ cantidadMueble);
					float precio = float.Parse(child.GetComponentInChildren<Text>().text);//Convierto el precio en entero
					
					//Debug.Log("Precio = "+ precio);
					
					TotalPres = TotalPres + precio * cantidadMueble ;// sumo los precios en base a la cantidad de los mismos y los sumo en el total
					//Debug.Log("Total =" +TotalPres );
					
					string TotalTextName ;
					
					TotalTextName  = TotalText.GetComponent<Text>().text;//Tomo el texto del total
					TotalText.GetComponent<Text>().text =  TotalPres.ToString();//Designo valor a texto del total convertido en String
					
					
				}
				
				
			}
			DataToExportToDatabase=DataToExportToDatabase+"_"+ElementoItemPresupuesto.Widht+"_"+ElementoItemPresupuesto.Height+"_"+ElementoItemPresupuesto.Deep;
			if (a){
				DataToExportToDatabase=DataToExportToDatabase+"_0";
				
			}
			DataToExportToDatabase=DataToExportToDatabase+",";
			
		}
	}
	
	public void DeleteItemsTable () {
		childNombre.GetComponent<ThumbGridPresupNombre>().DeleteItems();
		childID.GetComponent<ThumbGridPresupID>().DeleteItems();
		childCantidad.GetComponent<ThumbGridPresupCantidad>().DeleteItems();
		childPrecio.GetComponent<ThumbGridPresupPrecio>().DeleteItems();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	
/*	public void SetCatalogueUrl(string url)
	{
		CatalogueURLPres = url;
		//Debug.Log("Setting Url: " + CatalogueURLPres);
	}
*/	
	public bool SendToCatalogueURL()
	{
		//aca se deberia de pasar el codigo para que lo mande a JSON al Path y con toda la info chequeada antes
		//esta funcion es un bool, porque puede que halla algun tema por el cual no pueda conectarse contra JSON
		//y con esto le indicamos que va todo bien (true) o paso algun error (false).
		return true;
	}
	
	
	public void DatosPresupuesto()
	{
		
		GenerateDataToDatabase ();
		VendedorFinal = Vendedor.text.ToString ();
		NombreFinal = Nombre.text.ToString ();
		ApellidoFinal = Apellido.text.ToString ();
		TelefonoFinal = Telefono.text.ToString ();
		DireccionFinal = Direccion.text.ToString ();
		EmailFinal = email.text.ToString ();
		
		DataVendedor =/* FechaFinal*/ theDate+ ";" + VendedorFinal+ ";";
		DataCliente =  NombreFinal +";"+ ApellidoFinal+";" + TelefonoFinal +";" + DireccionFinal +";" + EmailFinal + ";";
		
		DataToExportFinal = DataVendedor + DataCliente + DataToExportToDatabase+";";
	}
	
	public const string MatchEmailPattern =
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
			+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
			+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
			+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
	public static bool IsEmail(string email)
	{
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}
	
	public string CheckInputRequeriments () {
		
		
		
		if (VendedorFinal != "") {
			if (NombreFinal != "") {
				if (ApellidoFinal != "") {
					if (TelefonoFinal != "") {
						if (DireccionFinal != "") {
							
							
							
							
							if (IsEmail(EmailFinal)) {
								
								return "ok";
								DatosOk = true;
							} else {
								//	WindowManager.Instance.ShowAlert ("Error7", "Error Email","Falta completar campo Email de cliente.", "Aceptar");
								return "Falta completar campo Email de cliente.";
								
								//	DisplayDialog ("error.", "Falta completar Campo email de cliente", "Ok"); 
							}
							
							
							
						} else {
							//WindowManager.Instance.ShowAlert ("Error6", "Error direccion","Falta completar campo de Direccion de cliente.", "Aceptar");
							return "Falta completar campo de Direccion de cliente.";
							
						}
					} else {
						//WindowManager.Instance.ShowAlert ("Error5", "Error Telefono","Falta completar campo Telefono de cliente.", "Aceptar");
						return "Falta completar campo de Telefono de cliente.";
						
					}
				} else {
					//WindowManager.Instance.ShowAlert ("Error4", "Error Apellido","Falta completar campo Apellido del cliente", "Aceptar");
					return "Falta completar campo Apellido del cliente.";
					
				}
			} else {
				//WindowManager.Instance.ShowAlert ("Error3", "Error Nombre","Falta completar campo Nombre del cliente.", "Aceptar");
				return "Falta completar campo Nombre del cliente.";
				
			}
		} else {
			//WindowManager.Instance.ShowAlert ("Error2", "Error Vendedor","Falta completar campo Vendedor.", "Aceptar");
			return "Falta completar campo de Datos de Vendedor.";
			
		}
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	public GameObject Camara3D;
	public GameObject CanvasMenu;
	public GameObject CanvasVirtualPad;
	public GameObject CanvasPanelModelo;
	public GameObject CanvasScreenShotBut;
	public GameObject CanvasPrintBut;
	public GameObject RoomToDeactive;
	
	public bool CanvasMenuActive;
	public bool CanvasVirtualPadActive;
	public bool CanvasPanelModeloActive;
	public bool CanvasScreenShotButActive;
	public bool CanvasPrintButActive;
	public bool RoomToDeactiveActive;
	
	/*public void BlockAnotherEnviroments () {
		//aca deberia de bloquear el uso del resto del GUI y de las interfaces
		CanvasMenuActive = CanvasMenu.activeSelf;
		CanvasVirtualPadActive = CanvasVirtualPad.activeSelf;
		CanvasPanelModeloActive = CanvasPanelModelo.activeSelf;
		CanvasScreenShotButActive = CanvasScreenShotBut.activeSelf;
		CanvasPrintButActive = CanvasPrintBut.activeSelf;
		RoomToDeactiveActive = RoomToDeactive.activeSelf;

		CanvasMenu.SetActive(false);
		CanvasVirtualPad.SetActive(false);
		CanvasPanelModelo.SetActive(false);
		CanvasScreenShotBut.SetActive(false);
		CanvasPrintBut.SetActive(false);
		RoomToDeactive.SetActive(false);


	}*/
	/*
	public void ActiveAnotherEnviroments () {
		//aca deberia de re-habilitar el uso del resto del GUI y de las interfaces
		CanvasMenu.SetActive(CanvasMenuActive);
		CanvasVirtualPad.SetActive(CanvasVirtualPadActive);
		CanvasPanelModelo.SetActive(CanvasPanelModeloActive);
		CanvasScreenShotBut.SetActive(CanvasScreenShotButActive);
		CanvasPrintBut.SetActive(CanvasPrintButActive);
		RoomToDeactive.SetActive(RoomToDeactiveActive);
	}*/
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	/// 

	public void ReseteoBotonPrint(){
	if (this.gameObject.activeSelf != true) {
			Comenzar ();
		}
	
	}
	
	public void Comenzar () {
		gameObject.SetActive (true);
		Camara3D.GetComponent<ExtendedFlycam>().enabled=false;
		UpdatePanelPresupuestos ();
		//BlockAnotherEnviroments ();
	}
	
	public void ClickBotonCancelar () {
		TextoFaltaCampos.SetActive(false);
		DeleteItemsTable ();
		gameObject.SetActive (false);
		Camara3D.GetComponent<ExtendedFlycam>().enabled=true;
		//	ActiveAnotherEnviroments();
	}
	
	public GameObject TextoFaltaCampos;
	public GameObject EnvioCorrecto;
	public bool Enviado = false;
	public bool DatosOk = false;
	
	
	public string UrlPrintPres= "";//"http://sistema.decorar3d.com/index.php/api/presupuestoprint/id/";
	public string UrlPrintPresIndex = "";
	
	public string IdPresup;
	
	private float tiempoEntreEnvio = 5;
	private float tiempoUltimoEnvio = 0;
	
	public bool decorandoP = true;
	public GameObject Time;
	public void ClickBotonAceptar () {
		decorandoP = false;
		DatosPresupuesto ();
		if (CheckInputRequeriments()=="ok") {
			if(SendToCatalogueURL()){
				if ((Enviado = true) && (DatosOk = true))
				{
					//no hubo error al grabar el JSON.
					//Debug.Log("no hubo error al grabar el JSON.");
					TextoFaltaCampos.SetActive(false);
					//decorandoP = false;
					
					//CountObject.GetComponent<TimeCountScript>().decorando = false;
					
					Enviar();
					
					//gameObject.SetActive (false);
					EnvioCorrecto.SetActive(true);
				}
			}else{
				//algo salio mal al querer grabar el JSON.
				
				//Debug.Log("algo salio mal al querer grabar el JSON.");
			}
			DeleteItemsTable();
			
			Camara3D.GetComponent<ExtendedFlycam>().enabled=true;
			//	ActiveAnotherEnviroments();
		} else {
			TextoFaltaCampos.SetActive(true);
			TextoFaltaCampos.GetComponent<Text>().text=CheckInputRequeriments();
			//aca deberia de mostrar un cuadro que solo muestre el error y con un boton de Aceptar
			//	Debug.Log(CheckInputRequeriments());
		}
	}

	public void decorandoReset(){

		decorandoP = true;
	}

	/*public void decorandoFalse(){
		
		decorandoP = false;
	}*/
	public void Clean(){
		
		Fecha.text = "";
		Vendedor.text = "";
		Nombre.text = "";
		Apellido.text = "";
		Telefono.text = "";
		Direccion.text = "";
		email.text = "";
		TextoFaltaCampos.SetActive(false);
	}
	
	
	public void Enviar(){
		TakeScreenshotPresupuesto ();
		
		
	/*	if (Enviado == true) {
			StartCoroutine(SendEmail());

			Debug.Log("envio email  ");
		}*/
		
		
		
		
		
		
	}
	
	
	
	public void TakeScreenshotPresupuesto()
	{
		EsPosibleEnviarMail = false;
		StartCoroutine(CaptureAndSavePresupuesto());
		StartCoroutine(SendEmail());
		//yield return new WaitForSeconds(5);

		
		
	}
	
	
	
	
	public string screenShotURL;//="http://test.decorar3d.com/index.php/api/presupuesto/"; 
	public string URLEmail;
	public string URLEmailFinal;
	//Captura de pantalla
	
	
	IEnumerator CaptureAndSavePresupuesto()
	{
		yield return new WaitForEndOfFrame();
		//------------------------------------------
		
		var tarWidth = Screen.width;//SMB: Esto setea el ancho de la pantalla *****
		var tarHeight = Screen.height;//SMB: Esto setea el alto de la pantalla *****
		
		//------------------------------------------
		
		//--------------------
		var newTexture = ScreenShootPresupuesto(Camera.main, tarWidth, tarHeight);//SMB: Esto setea cual sera la imagen que captura
		
		//--------------------
		
		//------------------------------------------
		LerpTexturePresupuesto(bg1, ref newTexture);//SMB: Esto hace un quitado de los colores de fondo respecto de la textura que se capturo antes. *****
		//Debug.Log ("new texture: " + newTexture);
		
		
		var bytes1 = newTexture.EncodeToJPG ();
	//	Debug.Log ("bytes1: " + bytes1.Length);
		
		Destroy(newTexture);
		
		
		var postForm = new WWWForm();
		postForm.AddBinaryData ("file", bytes1, "ImgPresupuesto.jpg", "image/jpg");
		postForm.AddField ("data", DataToExportFinal);
		
		
		
		var upload = new WWW(screenShotURL,postForm);
		
		//Debug.Log ("PostForm: "+ postForm.ToString ());
		
		yield return upload;
		//intento de id presupuesto
		
		
		//
		//Testeo	
		//para la correcta subida de la imagen al servidor.
		
		if (upload.error == null)
		{
			//Debug.Log("Upload: "+ upload.text);
			
			
			
			/*
			//Esto va para test
			Debug.Log ("error at '<': "+upload.text.IndexOf("<"));//163
			int CutInicio = upload.text.IndexOf("<");
			Debug.Log ("Length: "+upload.text.Length);//892
			int CutFinal = upload.text.Length;
			string TextoAcotado = upload.text.Remove(CutInicio,(CutFinal-CutInicio));
			Debug.Log ("TextoAcotado: "+TextoAcotado);

			itemData = JsonMapper.ToObject(TextoAcotado);

			Debug.Log (itemData["mensaje"][0]["presupuesto_id"]);
			IdPresup =itemData["mensaje"][0]["presupuesto_id"].ToString();
		
			UrlPrintPresIndex=UrlPrintPres+IdPresup;
			Application.OpenURL(UrlPrintPresIndex);
			Debug.Log ("Envio presupuesto: " +UrlPrintPresIndex.ToString());
			//Fin test
			*/
			
			
			
			//Esto va para sistema
			itemData = JsonMapper.ToObject(upload.text);
			
			//Debug.Log (itemData["mensaje"][0]["presupuesto_id"]);
			IdPresup =itemData["mensaje"][0]["presupuesto_id"].ToString();
			
			UrlPrintPresIndex=UrlPrintPres+IdPresup;
			//Debug.Log("id presup: " + IdPresup);
			//Debug.Log("print presup: " + UrlPrintPresIndex);
			Application.OpenURL(UrlPrintPresIndex);
			//Fin Sistema
			
			
			
			Enviado = true;
			
			EsPosibleEnviarMail=true;
		}
		else
		{
			Debug.Log("Error during upload: " + upload.error);
		}
	}
	bool EsPosibleEnviarMail=false;
	
	IEnumerator SendEmail()
	{
		//yield return new WaitForEndOfFrame();
		yield return new WaitForSeconds(5);
		
		//Debug.Log ("SendEmail");
		if (EsPosibleEnviarMail) {
			URLEmailFinal = URLEmail + IdPresup;
		//	Application.OpenURL (URLEmailFinal);
			//Application.ExternalCall( "SendEmailW", "URLEmailFinal" );
			Application.ExternalCall( "SendEmailW", URLEmailFinal );
			//Debug.Log ("Envio Email");
		}
		
	}
	


	//Fin testeo
	private static Texture2D ScreenShootPresupuesto(Camera srcCameraP, int width, int height)
	{
		//		Debug.Log(string.Format("Screen: {0}x{1}. Texture: {2}x{3}", Screen.width, Screen.height, width, height));
		var renderTextureP = new RenderTexture(Screen.width, Screen.height, 24);
		var targetTextureP = new Texture2D(width, height, TextureFormat.RGB24, false);
		srcCameraP.targetTexture = renderTextureP;
		srcCameraP.Render();
		RenderTexture.active = renderTextureP;
		targetTextureP.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		targetTextureP.Apply();
		
		srcCameraP.targetTexture = null;//SMB: Se anula para testeo *****
		RenderTexture.active = null;//SMB: Se anula para testeo *****
		srcCameraP.ResetAspect();
		return targetTextureP;
	}
	private static void LerpTexturePresupuesto(Texture2D alphaTextureP, ref Texture2D textureP)
	{
		//SMB: Anulamos el Lerp... *****
		var bgColorsP = alphaTextureP.GetPixels();
		var tarColsP = textureP.GetPixels();
		// Debug.Log(string.Format("Lerping the texture. Alpha: {0}. Texture: {1}.", bgColors.Length, tarCols.Length));
		for (var i = 0; i < tarColsP.Length; i++)
		{
			Color targColor = tarColsP[i];
			Color backColor = bgColorsP[i];
			tarColsP[i] = backColor.a > 0.99f ? backColor : Color.Lerp(targColor, backColor, backColor.a);
		}
		textureP.SetPixels(tarColsP);
		textureP.Apply();
		//SMB: Anulamos el Lerp... *****
	}
	
	
	
}

