﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SliderValueLabel : MonoBehaviour
{
    private Text TextField = null;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateText(float fov)
    {
        Debug.Log(string.Format("UpdateText:{0}", fov));
        TextField = TextField ?? this.GetComponent<Text>();
        if (TextField != null)
            TextField.text = fov.ToString();
    }
}
