using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SimpleJSON;

public class TimeCountScript : MonoBehaviour {
	public float CountTotal;
	public float CountPresupuesto;
	public float TimeDeco;//Aux de CountPresupuesto
	public bool decorando;

	public GameObject CountObject;

	public bool MostrarTutorial;

	public Text TxtSesion;
	public Text TxtDecoracion;




	void Start () {
		//CountTotal = 
		//decorando = scriptCount.decorandoP;
		decorando = CountObject.GetComponent<Presupuesto> ().decorandoP;
		//Debug.Log ("decorando= " + decorando);
	}
	
	// Update is called every frame, if the
	// MonoBehaviour is enabled.
	void Update () {

		CountTotal += Time.deltaTime; //Time.deltaTime will increase the value with 1 every second.
	//	Debug.Log ("CountTotal = " + CountTotal);
		ConteoPres ();

		decorando = CountObject.GetComponent<Presupuesto> ().decorandoP;
		//TxtTime.Text = CountTotal.ToString();
		//TxtTime = gameObject.GetComponent<Text> ();
	//	string TiempoSesion = CountTotal.ToString();
		TxtSesion.text = "Sesion: " + FloatToTime(CountTotal, "#00:00")  + " S";

	}

	public float Decorado;
	public string UrlTDecoracion;
	public string URLGralTime;
	int c= 0;
	public void ReseteoC(){
	//	decorando = true;
		c = 0;

	}

	public void ConteoPres(){

		if (decorando == true) {
						CountPresupuesto += Time.deltaTime; //Time.deltaTime will increase the value with 1 every second.

			TxtDecoracion.text = "Decoracion: " + FloatToTime(CountPresupuesto, "#00:00") + " S";
						//TxtDecoracion.text = "Decoracion: " +  FloatToTime (Time.deltaTime, string format) + "S";
			TimeDeco = CountPresupuesto;
		} else {

			if(c==0){
			StartCoroutine (SetTimeDecorationvalue());
				Debug.Log("c" + c);
				c = 1;
			}
		
			Decorado = CountPresupuesto;
			//envio a database
			CountPresupuesto = 0;
				
		}
	}

	public void SendTDecorationToDB(){
		StartCoroutine (SetTimeDecorationvalue());
	}
	//Envio de tiempo de decoracion
	IEnumerator SetTimeDecorationvalue(){
	//URLGralTime +UrlTDecoracion
		yield return new WaitForEndOfFrame ();
		var postFormMostrar = new WWWForm ();
		postFormMostrar.AddField ("Valor", TimeDeco.ToString ());
		var uploadPresupCat = new WWW (URLGralTime +UrlTDecoracion, postFormMostrar);
		yield return uploadPresupCat;
		Debug.Log("//---Valor" + URLGralTime +UrlTDecoracion );
		Debug.Log("//---Se envia valor"+ TimeDeco);

		}

	public string FloatToTime (float toConvert, string format){
		switch (format){
		case "00.0":
			return string.Format("{0:00}:{1:0}", 
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*10) % 10));//miliseconds
			break;
		case "#0.0":
			return string.Format("{0:#0}:{1:0}", 
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*10) % 10));//miliseconds
			break;
		case "00.00":
			return string.Format("{0:00}:{1:00}", 
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*100) % 100));//miliseconds
			break;
		case "00.000":
			return string.Format("{0:00}:{1:000}", 
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*1000) % 1000));//miliseconds
			break;
		case "#00.000":
			return string.Format("{0:#00}:{1:000}", 
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*1000) % 1000));//miliseconds
			break;
		case "#0:00":
			return string.Format("{0:#0}:{1:00}",
			                     Mathf.Floor(toConvert / 60),//minutes
			                     Mathf.Floor(toConvert) % 60);//seconds
			break;
		case "#00:00":
			return string.Format("{0:#00}:{1:00}", 
			                     Mathf.Floor(toConvert / 60),//minutes
			                     Mathf.Floor(toConvert) % 60);//seconds
			break;
		case "0:00.0":
			return string.Format("{0:0}:{1:00}.{2:0}",
			                     Mathf.Floor(toConvert / 60),//minutes
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*10) % 10));//miliseconds
			break;
		case "#0:00.0":
			return string.Format("{0:#0}:{1:00}.{2:0}",
			                     Mathf.Floor(toConvert / 60),//minutes
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*10) % 10));//miliseconds
			break;
		case "0:00.00":
			return string.Format("{0:0}:{1:00}.{2:00}",
			                     Mathf.Floor(toConvert / 60),//minutes
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*100) % 100));//miliseconds
			break;
		case "#0:00.00":
			return string.Format("{0:#0}:{1:00}.{2:00}",
			                     Mathf.Floor(toConvert / 60),//minutes
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*100) % 100));//miliseconds
			break;
		case "0:00.000":
			return string.Format("{0:0}:{1:00}.{2:000}",
			                     Mathf.Floor(toConvert / 60),//minutes
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*1000) % 1000));//miliseconds
			break;
		case "#0:00.000":
			return string.Format("{0:#0}:{1:00}.{2:000}",
			                     Mathf.Floor(toConvert / 60),//minutes
			                     Mathf.Floor(toConvert) % 60,//seconds
			                     Mathf.Floor((toConvert*1000) % 1000));//miliseconds
			break;
		}
		return "error";
	}
}
