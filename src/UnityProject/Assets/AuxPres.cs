﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SimpleJSON;



public class AuxPres : MonoBehaviour {

	public Text Debuging;

	public bool Decoracion;
	public bool Aberturas;
	public bool Iluminacion;
	public bool Electrodomesticos;
	public bool Muebles=true;

	public Toggle Dec;
	public Toggle Ab;
	public Toggle Il;
	public Toggle El;

	public bool ValidarSitio;//Es necesario crear un parametro en DB 
	public bool ValidadoSimulador ;//Es necesario crear un parametro en DB
	public GameObject PanelPresupuestar;
	public string URLGral;


	PresupuestoItems Cotizar;

	//Panel de Ayuda
	//No volver a mostrar.
	public GameObject Tutorial;
	public Toggle NoVolverAmostrar;
	public int NoMostrarMas;



	void Awake(){
		
		//StartCoroutine(YaPresupuestadas());
		StartCoroutine(GetMostrar());
		StartCoroutine(GetDecoracionwww());
		StartCoroutine(GetAberturaswww());
		StartCoroutine(GetIluminacionwww());
		StartCoroutine(GetElectrodomesticoswww());



	}



	// Use this for initialization
	void Start () {

		//StartCoroutine(NoMostrar());

	/*	if (ValidadoSimulador  == true) {
			PanelPresupuestar.SetActive (false);
		} else {
			PanelPresupuestar.SetActive (true);
		}*/
		
		if (Decoracion == true) {
			Dec.isOn = true;
		} else {
			Dec.isOn = false;
		}
		
		if (Aberturas == true) {
			Ab.isOn = true;
		} else {
			Ab.isOn = false;
		}
		
		if (Iluminacion == true) {
			Il.isOn = true;
		} else {
			Il.isOn = false;
		}
		
		if (Electrodomesticos == true) {
			El.isOn = true;
		} else {
			El.isOn = false;
		}

	}
	
	// Update is called once per frame
	void Update () {

 	}


	//Validacion de Panel de ayuda "No volver a mostrar"
	public string MostrarGet;
	public string MostrarSet;


	IEnumerator GetMostrar(){
		yield return new WaitForEndOfFrame ();
		
		var mostrar = new WWW (URLGral+MostrarGet);
		yield return mostrar;
		
		var M = JSON.Parse (mostrar.text);
		//AuxValor = M ["mensaje"][0]["valor"];
	NoMostrarMas = M ["mensaje"][0]["valor"].AsInt;
	
//		Debug.Log ("Valor Mostrar tutorial " + NoMostrarMas);
		Debuging.text = NoMostrarMas.ToString();
	//	Debug.Log (mostrar.text);
	//	Debug.Log (M);
	//	Debug.Log(NoMostrarMas);
		if (NoMostrarMas == 1) {
			Tutorial.SetActive(false);
		} else {
			Tutorial.SetActive(true);
		}
	}




	public void ChangeValueMostrar(){

		if (NoVolverAmostrar.isOn) {
			NoMostrarMas = 1;
		} else {
			NoMostrarMas = 0;
		}

	//	Debug.Log("changeValueMosrtar");

		StartCoroutine (SetMostrar ());


	}


	IEnumerator SetMostrar(){
		yield return new WaitForEndOfFrame ();
		var postFormMostrar = new WWWForm ();
		postFormMostrar.AddField ("Valor", NoMostrarMas.ToString ());
		var uploadPresupCat = new WWW (URLGral + MostrarSet + NoMostrarMas, postFormMostrar);
		yield return uploadPresupCat;
	//	Debug.Log("Valor" + URLGral + MostrarSet+NoMostrarMas);
		//Debug.Log("Se envia valor");
	}


	public void Deco(){
		
		if (Dec.isOn) {
			Decoracion = true;
		} else {
			Decoracion = false;		
		}

		StartCoroutine(SetDecoracionwww());

		Debug.Log("Decoracion= " + Decoracion);
	}
	
	public void Abert(){
		if (Ab.isOn) {
			Aberturas = true;
		} else {
			Aberturas = false;		
		}
		StartCoroutine(SetAberturaswww());
	//	Debug.Log("AberturasSN= " + Aberturas);
	}
	public void Ilum(){
		if (Il.isOn) {
			Iluminacion = true;
		} else {
			Iluminacion = false;
		}
		StartCoroutine(SetIluminacionwww());
	//	Debug.Log("Iluminacion= " + Iluminacion);
	}
	public void Elect(){
		if (El.isOn) {
			Electrodomesticos = true;
		} else {
			Electrodomesticos = false;
		}
		StartCoroutine(SetElectrodomesticoswww());
		//Debug.Log("Electrodomesticos= " + Electrodomesticos);
	}


//-----////Categorias Presupuestadas
	string PresupuestadasURL;
	public string GetDecoracion;
	public string SetDecoracion;

	
	public string GetAberturas;
	public string SetAberturas;

	public string GetIluminacion;
	public string SetIluminacion;

	public string GetElectrodomesticos;
	public string SetElectrodomesticos;

/*	IEnumerator GetMostrar(){
		yield return new WaitForEndOfFrame ();
		
		var mostrar = new WWW (URLGral+MostrarGet);
		yield return mostrar;
		
		var M = JSON.Parse (mostrar.text);
		//AuxValor = M ["mensaje"][0]["valor"];
		NoMostrarMas = M ["mensaje"][0]["valor"].AsInt;
		
		//Debug.Log ("Valor Mostrar tutorial " + NoMostrarMas);
		Debuging.text = NoMostrarMas.ToString();
		//	Debug.Log (mostrar.text);
		//	Debug.Log (M);
		//	Debug.Log(NoMostrarMas);
		if (NoMostrarMas == 1) {
			Tutorial.SetActive(false);
		} else {
			Tutorial.SetActive(true);
		}
	}*/

	//Verificacion de valores traidos desde la base de datos
	IEnumerator GetDecoracionwww(){
		yield return new WaitForEndOfFrame();

		//PresupuestadasURL = URLGral + GetDecoracion;
		var TextPresupuestadas = new WWW(URLGral + GetDecoracion);
		yield return TextPresupuestadas;
		
		var Pres = JSON.Parse (TextPresupuestadas.text);
		//ValidarSitio = Pres ["Usuario"]["ValidarSitio"].AsBool;
		//ValidadoSimulador  = Pres["Usuario"]["ValidadoSimulador "].AsBool;
		Decoracion = Pres ["mensaje"][0]["valor"].AsBool;
		//Debug.Log("PresupuestadasURL" + PresupuestadasURL);
		//Debug.Log("Decoracion" + Decoracion);
		/*Aberturas = Pres ["Usuario"]["Aberturas"].AsBool;
		Iluminacion = Pres ["Usuario"]["Iluminacion"].AsBool;
		Electrodomesticos = Pres ["Usuario"]["Electrodomesticos"].AsBool;
*/
		Debug.Log("Decoracion" + Decoracion);

		if (Decoracion == true) {
			Dec.isOn = true;
		} else {
			Dec.isOn = false;
		}
		
	}

	IEnumerator GetAberturaswww(){
		yield return new WaitForEndOfFrame();
		PresupuestadasURL = URLGral + GetAberturas;

		var TextPresupuestadas = new WWW(URLGral + GetAberturas);
		yield return TextPresupuestadas;
		
		var Pres = JSON.Parse (TextPresupuestadas.text);
		//ValidarSitio = Pres ["Usuario"]["ValidarSitio"].AsBool;
		//ValidadoSimulador  = Pres["Usuario"]["ValidadoSimulador "].AsBool;

		Aberturas = Pres ["mensaje"][0]["valor"].AsBool;
	//	Debug.Log("PresupuestadasURL" + PresupuestadasURL);
		Debug.Log("Aberturas" + Aberturas);
		if (Aberturas == true) {
			Ab.isOn = true;
		} else {
			Ab.isOn = false;
		}

		
	}

	IEnumerator GetIluminacionwww(){
		yield return new WaitForEndOfFrame();
		PresupuestadasURL = URLGral + GetIluminacion;
		var TextPresupuestadas = new WWW(PresupuestadasURL);
		yield return TextPresupuestadas;
		
		var Pres = JSON.Parse (TextPresupuestadas.text);


		Iluminacion = Pres ["mensaje"][0]["valor"].AsBool;
	//	Debug.Log("PresupuestadasURL" + PresupuestadasURL);
		Debug.Log("Iluminacion" + Iluminacion);

		if (Iluminacion == true) {
			Il.isOn = true;
		} else {
			Il.isOn = false;
		}


	}

	IEnumerator GetElectrodomesticoswww(){
		yield return new WaitForEndOfFrame();
		PresupuestadasURL = URLGral + GetElectrodomesticos;
		var TextPresupuestadas = new WWW(PresupuestadasURL);
		yield return TextPresupuestadas;
		//Debug.Log("Presupuestadas.text " + TextPresupuestadas.text);
		var Pres = JSON.Parse (TextPresupuestadas.text);

		Electrodomesticos = Pres ["mensaje"][0]["valor"].AsBool;
	//	Debug.Log("PresupuestadasURL" + PresupuestadasURL);
		Debug.Log("Electrodomesticos" + Electrodomesticos);

		
		if (Electrodomesticos == true) {
			El.isOn = true;
		} else {
			El.isOn = false;
		}
		
	}

	//Seteo de Categorias a presupuestar

	public void EnviarDatosPresupuestar(){
		//StartCoroutine(SetDecoracionwww());
		PanelPresupuestar.SetActive (false);
		
	}
	public string PresupCategoriasURL;

	/*
	 IEnumerator SetMostrar(){
		yield return new WaitForEndOfFrame ();
		var postFormMostrar = new WWWForm ();
		postFormMostrar.AddField ("Valor", NoMostrarMas.ToString ());
		var uploadPresupCat = new WWW (URLGral + MostrarSet + NoMostrarMas, postFormMostrar);
		yield return uploadPresupCat;
	//	Debug.Log("Valor" + URLGral + MostrarSet+NoMostrarMas);
		//Debug.Log("Se envia valor");
	}
*/
	//Subida de valores a base de datos.
	IEnumerator SetDecoracionwww(){


	
		int DecoSend;
		if (Decoracion == true) {
						DecoSend = 1;
				} else {
			DecoSend = 0;
				}
		yield return new WaitForEndOfFrame();
		var postForm = new WWWForm();

		postForm.AddField ("Valor", DecoSend.ToString());
		//postForm.AddField ("Presupuestar_Ab", Aberturas.ToString());
		//postForm.AddField ("Presupuestar_Ilum", Iluminacion.ToString());
		//postForm.AddField ("Presupuestar_Electro", Electrodomesticos.ToString());
		//
		PresupuestadasURL = URLGral + 	SetDecoracion + DecoSend;
		Debug.Log("PresupuestadasURL" + PresupuestadasURL +DecoSend);
		var uploadPresupCat = new WWW (PresupuestadasURL, postForm);

	

		yield return uploadPresupCat;
	}
	/*
	 yield return new WaitForEndOfFrame ();
		var postFormMostrar = new WWWForm ();
		postFormMostrar.AddField ("Valor", NoMostrarMas.ToString ());
		var uploadPresupCat = new WWW (URLGral + MostrarSet+NoMostrarMas, postFormMostrar);
		yield return uploadPresupCat;
	 */
	IEnumerator SetAberturaswww(){
		
		yield return new WaitForEndOfFrame();
		var postForm = new WWWForm();
		int AbSend;
		if (Aberturas == true) {
			AbSend = 1;
		} else {
			AbSend = 0;
		}
		
		postForm.AddField ("valor", AbSend.ToString());
		//postForm.AddField ("Presupuestar_Ab", Aberturas.ToString());
		//postForm.AddField ("Presupuestar_Ilum", Iluminacion.ToString());
		//postForm.AddField ("Presupuestar_Electro", Electrodomesticos.ToString());
		PresupuestadasURL = URLGral + 	SetAberturas;
		
		Debug.Log("PresupuestadasURL" + PresupuestadasURL);
		var uploadPresupCat = new WWW (PresupuestadasURL, postForm);
		
		yield return uploadPresupCat;
	}
	IEnumerator SetIluminacionwww(){
		
		yield return new WaitForEndOfFrame();
		var postForm = new WWWForm();
		int IlSend;
		if (Iluminacion == true) {
			IlSend = 1;
		} else {
			IlSend = 0;
		}
		
		postForm.AddField ("valor", IlSend.ToString());
		//postForm.AddField ("Presupuestar_Ab", Aberturas.ToString());
		//postForm.AddField ("Presupuestar_Ilum", Iluminacion.ToString());
		//postForm.AddField ("Presupuestar_Electro", Electrodomesticos.ToString());
		PresupuestadasURL = URLGral + 	SetIluminacion;
		
		Debug.Log("PresupuestadasURL" + PresupuestadasURL);
		var uploadPresupCat = new WWW (PresupuestadasURL, postForm);
		
		yield return uploadPresupCat;
	}
	IEnumerator SetElectrodomesticoswww(){
		
		yield return new WaitForEndOfFrame();
		var postForm = new WWWForm();
		int ElectSend;
		if (Electrodomesticos == true) {
			ElectSend = 1;
		} else {
			ElectSend = 0;
		}
		
		postForm.AddField ("valor", ElectSend.ToString());
		//postForm.AddField ("Presupuestar_Ab", Aberturas.ToString());
		//postForm.AddField ("Presupuestar_Ilum", Iluminacion.ToString());
		//postForm.AddField ("Presupuestar_Electro", Electrodomesticos.ToString());
		PresupuestadasURL = URLGral + 	SetElectrodomesticos;
		
	//	Debug.Log("PresupuestadasURL" + PresupuestadasURL);
		var uploadPresupCat = new WWW (PresupuestadasURL, postForm);
		
		yield return uploadPresupCat;
	}


}
