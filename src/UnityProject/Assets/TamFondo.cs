using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TamFondo : MonoBehaviour {
	public GameObject Navigation;
	public GameObject Subtitle;

	public GameObject Categ;
	public GameObject Thumbnails;

	public GameObject PanelThmb;

	public float ContenedorT;

	public float NavHeight;
	public float NavHeight2;
	public float SubtHeight;

	public float CateHeight;
	public float TbsHeight;


	public float panelTam;
	public float tamtotal;
	public int CantAjust;

	void Start () {
	
	}
	
	void Update () {


				NavHeight = Navigation.GetComponent<RectTransform> ().rect.height;
				NavHeight2 = Navigation.GetComponent<RectTransform> ().rect.width;
				SubtHeight = Subtitle.GetComponent<RectTransform> ().rect.height;
				CateHeight = Categ.GetComponent<RectTransform> ().rect.height;
				TbsHeight = Thumbnails.GetComponent<RectTransform> ().rect.height;

				CantAjust = PanelThmb.GetComponent<ThumbGrid> ().CountItems;
				if (CantAjust == 0) {

						tamtotal = (NavHeight + SubtHeight + CateHeight +10);			

				} else {
						if (CantAjust > 0 && CantAjust <= 4) {
								tamtotal = (NavHeight + SubtHeight + CateHeight + TbsHeight-30);

						} else {
								if (CantAjust>4 && CantAjust <= 8) {
										tamtotal = (NavHeight + SubtHeight + CateHeight + TbsHeight + 60);

								} else {
				
										tamtotal = (NavHeight + SubtHeight + CateHeight + TbsHeight + 120);
								}
		
						}
				}
				this.gameObject.GetComponent<RectTransform> ().offsetMin = new Vector2 ((-NavHeight2 + 13), -tamtotal);


				/*
			//reference
		 	//[ left - bottom ]
			slideArea.gameObject.GetComponent<RectTransform>().offsetMin = new Vector2(10f, 10f);
			//[ right - top ]
			slideArea.gameObject.GetComponent<RectTransform>().offsetMax = new Vector2(-10f, -10f);
		 */


				//if ( scrollRect.verticalScrollbar != null )*/

		}

}
