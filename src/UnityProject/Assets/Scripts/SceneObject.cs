using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using System;
using UnityEngine.UI;

using UnityEngine.EventSystems;




public class SceneObject : MonoBehaviour
{

	private bool Selected;
	private bool _isNotColliding;
	public int AlturaModelo;
	public bool IsNotColliding
	{
		get { return _isNotColliding; }
		protected set { _isNotColliding = value; }
	}
	protected GameObject RotationAnchor = null;
	protected SceneManager SceneManager;
	
	public CatalogueItem Item;
	public CatalogueItemVariation CurrentVariation;
	public CatalogueMaterial mat;
	public CatalogueMaterialesSet matSet;


	public bool RequestUpdateInfo;
	public bool OverrideDimensionText = true;
	
	public IOperation<Quaternion> RotationOperation;
	public IOperation<Vector3> TranslationOperation;

	public float Rot;
	//-- Info Basica Modelo
	public string CategoriaM;
	public string idItem;
	public string nameItem;
	public string descItem;
	public string typeItem;
	public string textureItem;
	public string thumbnailItem;
	public string heightItem;
	public string widthItem;
	public string deepItem;
	public string modelItem;
	public bool modelUseMtlItem;
	public Vector3 scaleItem;
	public Vector3 RotationItem;


	//public List<string> Mmmm;
	//---


	GameObject anchor;
	Vector3 closestPoint;
	//public float largo;
	//public float ancho;
	//public float WidthPiso;
	//public float HeightPiso;
	public string Fijado;


	public GameObject materialMueble;
	public int materialLength;
	public Vector3 SizeCollider;
	public  Mesh viewedModel;
	public MeshCollider meshCollisionN;
	public BoxCollider BoxColision;
	
	Vector3 SizeColliderY;
	Vector3 PosColliderY;

	public float AlturaDefault=0.1125f;


	//Mesh Renderer
	public string MRENDERER;//?
	public int Mcount;
	public Material[] MMaterial;//?


	/*
	public CatalogueItemVariation Vmat;
	public CatalogueMaterial Mmat;
	public CatalogueMaterialesSet MSmat;
	public List<CatalogueItemVariation> SOmaterials = new List<CatalogueItemVariation>();//?
	public List<CatalogueMaterial> SetMats = new List<CatalogueMaterial>();
	*/




//----------Set de Colores---------------------
	public GameObject  BotonSetColores;
	public GameObject  Menu;//Testeo de toma de GameObjects in scene
	public GameObject  Canvas;//Testeo de toma de GameObjects in scene
	//SetMateriales
	public int SCount;//Conteo de cantidad de Sets de materiales
	//public string SName;
	//MaterialesInternos
	public int ROid;//id mueble
	public string NombreSet;//
	public  string MatInternos;
	//Material 
	public string materialID;
	public string MatSet;
	public string ColSet;
	public string TextureSet;
	//Cambio de nro
	public int NroChange;
	public Texture TextureChange;
	public Material MatChange;

	public Material testmat;
	public List<Material> listaTestmat;
	public CatalogueItemVariation VarMat;
	public MatSetObj[] CatalogoMaterialesFinal;
	public int CatMatToUse = 0;
	public int CatMatActual = -1;
	public int CatMatMax = 0;//Cantidad de sets de materiales del mueble
	public int LengthSetMat = 0;
	public string NombreSets;
	//public int CantRepChck = 0;
	//public int CantRepMax = 50;
	public float TimeInitRepChck = 0;
	public float TimeMaxRepChck = 10F;

	//Botones sets

	public string TextoSets;
	public int CantSetsMat;

	
//----------Fin Set de Colores---------------------


	

	void Start()
	{
		TimeInitRepChck = Time.time;


	//---Start-------MeshRenderer
		
		
		Mcount = this.GetComponent<MeshRenderer> ().materials.Count();
		
		
		MMaterial = this.GetComponent<MeshRenderer> ().materials.ToArray ();
		
	
	//---Start-------Fin MeshRenderer------------------------------------------------------------------

		//--Debug.Log ("SS "+this.name+": "+"----------------------------->>>>>>>>>>>>>>>>>> SceneObject.Start() => FirstPosition=True");
		SceneManager.FirstPosition = true;
		Init();

		//-- Info Basica Modelo
		this.CategoriaM = Item.category;
		nameItem = Item.name;
		descItem = Item.description;
		typeItem = Item.type.ToString ();

		thumbnailItem = Item.thumbnail;
		heightItem = Item.height;
		widthItem = Item.width;
		deepItem = Item.deep;
		modelItem = Item.model;
		modelUseMtlItem = Item.modelUseMtl;
		scaleItem = Item.scale;
		RotationItem = Item.rotation;


		AlturaModelo = Item.Altura;
		Fijado = Item.fijadoA;
		idItem = Item.id;



	/*	if (Item.category == "624" || Item.category == "620" || Item.category == "615") {
						this.tag = "Decoracion";
				} else {
						if (Item.category == "610" || Item.category == "611" || Item.category == "612") {
								this.tag = "Aberturas";
						} else {
								if (Item.category == "615") {//||Item.category=="611"||Item.category=="612"){
										this.tag = "Electrodomesticos";
								} else {
										if (Item.category == "620") {//||Item.category=="611"||Item.category=="612"){
												this.tag = "Iluminacion";
										} else {
												this.tag = "Muebles";//ojo, hay que tocar cada una de las categorias para poder despejar bien que elementos tienen que venir a nivel del suelo y cuales no
												AlturaDefault = 0.1125f;
										}

								}

						}
				}
		*/
		switch (Item.category) {
		case "624":
			this.tag = "Decoracion";
			break;
	
		case "610":
			this.tag = "Aberturas";
			break;

		case "611":
			//Puertas
			this.tag = "Aberturas";
			break;

		case "612":
			//Ventanas
			this.tag = "Aberturas";

		
			break;

		case "615":
			this.tag = "Electrodomesticos";
			break;

		case "620":
			this.tag = "Iluminacion";
			break;
		case "639":
			this.tag = "TV";
			break;
		default:
			this.tag = "Muebles";
			AlturaDefault = 0.1125f;
			break;

			

		

		}




		//----Start------BotonesSetColores----------------------------------------------------------
		
		BotonSetColores =  GameObject.Find("ScriptSets");
		Menu = GameObject.Find("Menu");
		Canvas = GameObject.Find("Canvas");
		
	//	BotonSetColores.GetComponent<BotonesSetsColores> ().SetsColores ();

		//BotonesSetsColores SceneScript = BotonSetColores.GetComponent<BotonesSetsColores>();
		//SceneScript.GetNroMat();


		GameObject BotonSets = GameObject.Find("ScriptSets");
		BotonesSetsColores SceneScript = BotonSets.GetComponent<BotonesSetsColores>();
		SceneScript.GetNroMat();
		
		
		
		//Valores de JSon
		
		foreach (CatalogueItemVariation var in Item.variations) {
			VarMat = var;
		}
		
		CatMatToUse = 0;
		//CantRepChck = 0;
		LoadMaterialSets ();
		//CantRepMax=LengthSetMat* CantRepMax;
		
		//BotonSetColores.GetComponent<BotonesSetsColores> ().SetsColores ();
		
		
		//----------Fin BotonesSetColores----------------------------------------------------------




		
		//End MeshRenderer
		
		MeshFilter viewedModelFilter = ((MeshFilter)gameObject.GetComponent("MeshFilter"));
		
		viewedModel=viewedModelFilter.mesh; 
		meshCollisionN = gameObject.AddComponent<MeshCollider>();
		if (this.tag == "Decoracion") {

			meshCollisionN.convex = true;
		
		}
		//meshCollisionN.convex = true;// la colision es convexa
		//meshCollisionN.isTrigger = true;// la colision es trigger. eso implica que puede ejercer acciones mayores a la de colision en base a la accion que le apliquemos.
		SizeCollider =  GetComponent<Renderer>().bounds.size;

		
		//boxcollision
		BoxColision = gameObject.AddComponent<BoxCollider>();
		
		SizeColliderY.x = BoxColision.size.x;
		SizeColliderY.y = 0.2f;
		SizeColliderY.z = BoxColision.size.z;
		BoxColision.size = SizeColliderY;
		
		
		PosColliderY.x = BoxColision.center.x;
		PosColliderY.y = 0.1f;
		PosColliderY.z = BoxColision.center.z;
		//collider.size = Vector3(1.2,2.5,1.2);
		BoxColision.center = PosColliderY;
		
		BoxColision.isTrigger =true;
		
		//End boxcollision
		if (Fijado == "pared") {
			float posZ = this.transform.position.z;
			posZ = 0.25f;
		}
		
		LoadMaterialSets ();
		//LoadNamesSets ();
		//
		SceneManager.GetComponent<SceneManager> ().PerformMatButtomSet (this.gameObject);
		
		//botones sets 
		CantSetsMat = CatMatMax;



	//	ToUpdateColor = GameObject.FindGameObjectWithTag("ScriptLuzTag");

	//	ToUpdateColor.GetComponent<ControlLuz> ().Force ();
		//GameObject.FindGameObjectWithTag ("ScriptLuzTag").GetComponent<ControlLuz>().ForceUpdateAberturas=true;

	}
	public GameObject ToUpdateColor;

	public Color TranspColor;

	void LoadMaterialSets()
	{

		CatalogoMaterialesFinal = new MatSetObj[VarMat.Setmaterials.Count];
		int i = 0;
		//Debug.Log ("**CatalogoMaterialesFinal.Length: " + CatalogoMaterialesFinal.Length);
		CatMatMax = CatalogoMaterialesFinal.Length;


	


		foreach (CatalogueMaterial mat in VarMat.Setmaterials) {
			CatalogoMaterialesFinal[i] = new MatSetObj();
			CatalogoMaterialesFinal[i].roomObjet_id = mat.roomObject_id;
			CatalogoMaterialesFinal[i].nombreSets = mat.nombreSets;

			CatalogoMaterialesFinal[i].Materiales = new Material[mat.materialesInternos.Count];


			//boton
			TextoSets = mat.nombreSets;//agregado 5-7



			if(mat.materialesInternos.Count > LengthSetMat){
				LengthSetMat = mat.materialesInternos.Count;
			}
			//Debug.Log ("****CatalogoMaterialesFinal["+i+"].roomObject_id: " + CatalogoMaterialesFinal[i].roomObjet_id);
			int j = 0;
			foreach (CatalogueMaterialesSet set in mat.materialesInternos) {
				Material TempMat;
				if(set.tipoSet=="transparent"){
					if(this.tag != "Aberturas"  ){
					TempMat = new Material(Shader.Find("Transparent/Diffuse"));

					TranspColor =  new Color32(255, 255,255,50);
					TempMat.color = TranspColor;
						//Debug.Log ("TempMat"+TempMat.name);
					

					}else{
						TempMat = new Material(Shader.Find("Custom/WindowCut"));
						TranspColor =  new Color32(255, 255,255,50);
						TempMat.color = TranspColor;
						TempMat.name="VIDRIO";

					
						/* //--Vidrio liso
						TempMat = new Material(Shader.Find("Diffuse"));
						TempMat.name="VIDRIO";
						TempMat = new Material(Shader.Find("Diffuse"));
						*/
						
					}


				}else{
					TempMat = new Material(Shader.Find("Diffuse"));
				}

				Texture TempTexture = new Texture();
				TempTexture = SceneManager.GetTexture(this.gameObject,set.textureSet);
				if(TempTexture == null){
				}else{
					TempMat.mainTexture = TempTexture;
					CatalogoMaterialesFinal[i].Materiales[j] = new Material(TempMat);
				}

				if(set.colorSet!=null){
					TempMat.color = set.colorSet.Value;
				}else{
					Debug.Log ("**********Color NULL!!!!!!......");
				}

				if(CatalogoMaterialesFinal[i].Materiales[j] == null){
				//	Debug.Log ("**********CatalogoMaterialesFinal["+i+"].Materiales["+j+"] = null......");
				}else{
					//Debug.Log ("**********CatalogoMaterialesFinal["+i+"].Materiales["+j+"] = " + CatalogoMaterialesFinal[i].Materiales[j]);
					if(this.CategoriaM == "612"){
						Debug.Log ("Vidrio Ventana");
					}
				}
				
				j++;
			}
			i++;
		}
	}

	public string[] NombresArray;
	
	void LoadNamesSets()
	{
		int i = 0;
		NombresArray = new string[CatMatMax];

		foreach (CatalogueMaterial mat in VarMat.Setmaterials) {
			//NombresArray[i] = new string();
			NombresArray[i] = mat.nombreSets;
			i++;
		}
	}

	public string[] GetNombresArray()
	{
		LoadNamesSets ();
		return NombresArray;
	}

	public void Init()
	{
		RotationOperation = RotationOperation ?? new RotationOperation(this);
		TranslationOperation = TranslationOperation ?? new TranslationOperation(this);
		SceneManager = GameObject.FindGameObjectWithTag(SceneManager.DefaultTag).GetComponent<SceneManager>();//coloca el modelo en la escena
		RotationAnchor = RotationAnchor ?? CreateRotationAnchor();
	}
	
	private GameObject CreateRotationAnchor()
	{
		float yScale = this.transform.lossyScale.y;
		/* if (yScale == 0)
        {
            yScale = 1;
        }
        else if (yScale != 1)
        {
            yScale = 1 / yScale + 1;
        }*/
		
		anchor = new GameObject();
		anchor.name = "Rotator Anchor";
		anchor.transform.parent = this.transform;
		anchor.transform.localPosition = new Vector3(0, yScale, 0);
		anchor.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
		anchor.AddComponent<MeshCollider>();
		return anchor;
	}
	
	// Update is called once per frame
	void Update()
	{
		PositionMueble = transform.position;
		Rot = this.transform.eulerAngles.y;
		//Debug.Log(DateTime.Now.TimeOfDay.ToString() + " SceneObject Update");
		if (RequestUpdateInfo)
		{
			UpdateInfo();
		}
		if (Selected)
		{
			SceneManager.UIManager.UpdateRotationButton(this);
		}
		ExecuteOperations();
		/*
		 //Testeo de Borrado de Muebles
		if(Input.GetKey(KeyCode.Space)){

			BorrarSceneObject();
		}*/
		
	








		//CatMatToUse
		//LengthSetMat
		bool NeedToReload = false;

		if ((Time.time-TimeInitRepChck) <= TimeMaxRepChck) {
			for (int i=0; i < CatalogoMaterialesFinal[CatMatToUse].Materiales.Length; i++) {
				if (CatalogoMaterialesFinal [CatMatToUse].Materiales [i] == null) {
					NeedToReload = true;
					//Debug.Log ("CatalogoMaterialesFinal[" + CatMatToUse + "].Materiales[" + i + "] = NEED TO REOAD!!!!!!");
				}
			}
		}
		if (LengthSetMat != CatalogoMaterialesFinal[CatMatToUse].Materiales.Length) {
			Material[] MatTemsToResolve = new Material[LengthSetMat];
			for(int z=0;z<LengthSetMat;z++){
				if(CatalogoMaterialesFinal[CatMatToUse].Materiales.Length<=z){
					MatTemsToResolve[z] = null;
				}else{
					MatTemsToResolve[z] = CatalogoMaterialesFinal[CatMatToUse].Materiales[z];
				}

			}
			CatalogoMaterialesFinal[CatMatToUse].Materiales=MatTemsToResolve;
		}

		if (NeedToReload){
			LoadMaterialSets();
			CatMatActual=-60;
		}

		if(CatMatActual!=CatMatToUse){
			this.GetComponent<MeshRenderer> ().materials = CatalogoMaterialesFinal [CatMatToUse].Materiales;
			CatMatActual=CatMatToUse;
		}


	}

	public void AssetsDownloaded(KeyValuePair<string, Texture2D> textureKVP)
	{
		//--Debug.Log("SS "+this.name+": "+System.DateTime.Now.ToString() + "AssetDownloaded, Applying variation");
		ApplyVariation();
	}
	public void ApplyVariation()
	{
		/*Aplicacion Material - Probado*/	SceneManager.ApplyVariationToObject(CurrentVariation, gameObject);
//		SceneManager.ApplyVariationToObject(CurrentVariation.Setmaterials, gameObject);
	}
	public void UpdateInfo()
	{
		RequestUpdateInfo = false;
	}
	
	public void SetSelected(bool selected)
	{
		this.Selected = selected;
		this.UpdateInfo();
	}
	
	public void UpdateBounds()
	{
	}
	
	public List<Collider> colliding = new List<Collider>();
	public Collider[] colliders;
	public int ConteoColl;
	
	// OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider (Since v1.0)
	
	
	// OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider (Since v1.0)
	void OnCollisionEnter(Collision collision)
	{
		//--	Debug.Log("SS "+this.name+": "+string.Format("OnCollisionEnter: {0}. Name: {1}", colliding.Count, collision.gameObject.name));
		
		if (this.SceneManager.IsConflictingCollider(this, collision.collider))
		{
			colliding.Add(collision.collider);
			//--	Debug.Log("SS "+this.name+": "+string.Format("OnCollisionEnter: {0}. Name: {1}", colliding.Count, collision.gameObject.name));
			colliders = colliding.ToArray();
		}
		
	}
	
	// OnCollisionExit is called when this collider/rigidbody has stopped touching another rigidbody/collider (Since v1.0)
	void OnCollisionExit(Collision collision)
	{
		//--	Debug.Log("SS "+this.name+": "+string.Format("OnCollisionExit: {0}. Name: {1} - PRE", colliding.Count, collision.gameObject.name));
		if (this.SceneManager.IsConflictingCollider(this, collision.collider))
		{
			colliding.Remove(collision.collider);
			//--		Debug.Log("SS "+this.name+": "+string.Format("OnCollisionExit: {0}. Name: {1}", colliding.Count, collision.gameObject.name));
			colliders = colliding.ToArray();
		}
	}
	
	//test
	
	//
	public int TCOL;
	public int NCOL2;
	public float PosicionE;
	
	void OnTriggerEnter(Collider other)
	{
		//Debug.Log(string.Format("OnTriggerEnter: {0}. Name: {1}", colliding.Count, other.gameObject.name));
		if (this.SceneManager.IsConflictingCollider (this, other.GetComponent<Collider> ())) {
			colliding.Add (other);
			//--	Debug.Log ("SS " + this.name + ": " + string.Format ("OnTriggerEnter: {0}. Name: {1}", colliding.Count, other.gameObject.name));
			colliders = colliding.ToArray ();
			//other.On
			Vector3 resvaltransf = ValidateTransform ();
			//TranslationOperation.FixedUpdate();//agregado .smb
			//--	Debug.Log ("SS " + this.name + ": " + "TAG del atacante: " + other.gameObject.tag);
			if (other.gameObject.tag == "Muebles") {
				//--		Debug.Log ("SS " + this.name + ": " + "ENTRA EN EL IF!!!");
				other.gameObject.GetComponent<SceneObject> ().TransfrCollider (this.tag);//esta es la condicion que transmite al atacante que esta interactuando con otro mueble
			//	Debug.Log ("Collision Mueble 1111");

			}else{
				//Habitacion Cuadrada



				switch (other.gameObject.tag)
				{
					case "East":
					TCOL =1;
					break;

					case "West":
					TCOL =2;
					break;
					case "South":
					TCOL =3;
					break;
				case "North":
					TCOL =4;
					break;
				/*case "Decoracion":
					int Estantes = 1;
					Debug.Log("Colision Decoracion");
					other.gameObject.
					break;*/
				}
			
				switch (other.gameObject.name)
				{
					//Cuadrada
				case"Centro West Wall":
						NCOL2 = 1;
					break;
				case"Centro East Wall"://
						NCOL2 = 2;
					break;
				case"Centro South Wall":
						NCOL2 = 3;
					break;
				case"Centro North Wall":
						NCOL2 = 4;
					break;
				//--------------------------
					//L
					//abajo

				case"Abajo West Wall":
						NCOL2 = 5;

					break;
				case"Abajo East Wall"://
						NCOL2 = 6;
					break;
				case"Abajo South Wall":
						NCOL2 = 7;
					break;
					//Derecha
				case"Derecha East Wall"://
						NCOL2 = 8;

					PosicionE = other.gameObject.transform.position.x;
					break;

				case"Derecha South Wall":
						NCOL2 = 9;
					break;

				case"Derecha North Wall":
						NCOL2 = 10;
					break;
					//Centro

			
					//---------------------------------

					//T
					//Izquierda
				case"Izquierda West Wall":
						NCOL2 = 11;
					PosicionE = other.gameObject.transform.position.x;
					break;

						
				case"Izquierda South Wall":
					NCOL2 = 12;
					break;
				
				case"Izquierda North Wall":
						NCOL2 = 13;
					break;

				}


				
				
			}
			
			
			/*else{
				other.gameObject.GetComponent<SceneObject> ().TransfrCollider (other.gameObject.tag);
				
				
			}*/
		}
	}
	
	public bool ForceColliderMuebles = false;
	
	
	
	
	
	// OnTriggerExit is called when the Collider other has stopped touching the trigger (Since v1.0)
	void OnTriggerExit(Collider other)
	{
		//Debug.Log("SsssssssssS "+this.name+": "+string.Format("OnTriggerExit: {0}. Name: {1} - PRE ", colliding.Count, other.gameObject.name));
		if (this.SceneManager.IsConflictingCollider(this, other.GetComponent<Collider>()))
		{
			other.gameObject.GetComponent<SceneObject> ().TransfrCollider (null);//esta es la condicion que transmite al atacante que esta interactuando con otro mueble
			//			other.gameObject.GetComponent<SceneObject> ().TransfrCollider( null);//esta es la condicion que transmite al atacante que esta interactuando con otro mueble
			
			colliding.Remove(other);
			//--	Debug.Log("SS "+this.name+": "+string.Format("OnTriggerExit: {0}. Name: {1}", colliding.Count, other.gameObject.name));
			//Tener cuidado con el OnTriggerEnter y Exit, esto deberia de corregir este comportamiento, pero hey que analizarlo
			colliders = colliding.ToArray();
			
			Vector3 resvaltransf = ValidateTransform();
		}
		
		
	}
	
	public void TransfrCollider(String tag){
		
		//--Debug.Log ("SS " + this.name + ": " + "Se transmite al atacante: " + tag);//+" valor: "+tag == "Muebles");
		if (tag == "Muebles" || tag == "TV") {
			//--	Debug.Log ("SS " + this.name + ": " + "Entra!!!!!!!!... MUEBLES==" + tag);//+" valor: "+tag == "Muebles");
			ForceColliderMuebles=true;
		} else {
			
			//--	Debug.Log ("SS " + this.name + ": " + "NO ENTRA :( al atacante... MUEBLES==" + tag);//+" valor: "+tag == "Muebles");
			ForceColliderMuebles=false;
			
		}
	}
	//
	
	
	
	
	
	
	
	
	
	public bool Altura0;
	public Vector3 SafePlace;
	public Vector3 PositionMueble;
	public Vector3 SafeAux;
	public float MuebleX;
	public float MuebleZ;
	public float WestString;
	public float EastString;

	public float NorthString;
	public float SouthString;
	public float RotacionMueble;
	
	public Vector3 Safe;
	
	public float EastString2;
	public float SouthString2;
	public float NorthString2;
	public float WestString2;	

	public float EastStringDerecha;
	
	
	public Vector3 ValidateTransform ()
	{
		
		MuebleX = this.SizeCollider.x;
		MuebleZ = this.SizeCollider.z;
		RotacionMueble = this.transform.rotation.y;
//		Debug.Log ("Rotacion Mueble Y : " + RotacionMueble);
		
		WestString = (GameObject.FindWithTag ("West").transform.position.x + MuebleX);
		WestString2 = (GameObject.FindWithTag ("West").transform.position.x + (MuebleX/2));
		 
		EastString = (GameObject.FindWithTag ("East").transform.position.x - MuebleX);
		EastString2 = (GameObject.FindWithTag ("East").transform.position.x - (MuebleX/2));



		NorthString = (GameObject.FindWithTag ("North").transform.position.z - (MuebleZ*2));
		NorthString2 = (GameObject.FindWithTag ("North").transform.position.z - (MuebleZ));

		SouthString = (GameObject.FindWithTag ("South").transform.position.z + (MuebleZ*2));
		SouthString2 = (GameObject.FindWithTag ("South").transform.position.z + (MuebleZ));
		
		
		//SMB: ------>>>>> AttachedToWall: Modificacion de Validate Transform 
		if (Item.fijadoA == "pared") { //SMB: AttachedToWall: Anula el chequeo de transformacion para que este en la pared
			return new Vector3 (0, -100, 0);//SMB: AttachedToWall: Anula el chequeo de transformacion para que este en la pared
			
			
			
		} else {
			
			
			
			//------------------------------------------------------			
			if (this.tag == "Muebles") {
				SafePlace.y = 0.1125f;
				//--	Vector3 nuevo = this.transform.position;
				//--	nuevo.y = AlturaDefault;
				//--this.transform.position = nuevo;
			}
			
			
			
			
			if(ForceColliderMuebles == false){
				
				SafeAux = transform.position;
			//	print ("NOCOLISION");
				
				
				switch(TCOL){

		/**/	case 1 :
					
					//EAST
					switch(NCOL2){
						//centro
						case 2:
							//Debug.Log("centro");
							if((RotacionMueble == -1 ) || (RotacionMueble == 1 ))
								{
								SafePlace.x = EastString2;
					
								}else{
									if(!RotationOperation.IsActive)
									{
										SafePlace.x = EastString- MuebleX/10;

									}else{
									SafePlace.x = EastString ;
						
							
									}
						
								}
							
						NCOL2 = 0;
						SafePlace.z = transform.position.z;
						SafePlace.y = 0.1125f;
					//	Debug.Log("Safe East: " +SafePlace);
					//	Debug.Log ("EAST");
						TCOL =0;
						break;


						//abajo

						case 6:
					//	Debug.Log("Abajo");
						if((RotacionMueble == -1 ) || (RotacionMueble == 1 ))
						{
							SafePlace.x = EastString2;
							
						}else{
							if(!RotationOperation.IsActive)
							{
								SafePlace.x = EastString- MuebleX/10;
								
							}else{
								SafePlace.x = EastString ;
								
								
							}
							
						}
						SafePlace.z = transform.position.z;
						SafePlace.y = 0.1125f;
						//Debug.Log("Safe East: " +SafePlace);
						///Debug.Log ("EAST");
						TCOL =0;
						NCOL2 = 0;
						break;


						//derecha
						case 8:
							//Debug.Log("Derecha");
					
							if((RotacionMueble == -1 ) || (RotacionMueble == 1 ))
							{
							SafePlace.x = (PosicionE- MuebleX/2)-MuebleX/10;
							
							}else{
								if(!RotationOperation.IsActive)
								{
								SafePlace.x = (PosicionE- MuebleX)- MuebleX/10;
								//Debug.Log ("Safe Derecha East: " + SafePlace.x);
							}else{
								SafePlace.x = PosicionE - MuebleX;
								//Debug.Log ("Safe Derecha East: " + SafePlace.x);
								
								
							}

							
						}

						SafePlace.z = transform.position.z;
						SafePlace.y = 0.1125f;
						//Debug.Log("Safe East: " +SafePlace);
						//Debug.Log ("EAST");
						TCOL =0;
						NCOL2 = 0;
						break;



					}//FInSwitch East


				break;


	/**/	case 2 :
					
					//west

					switch(NCOL2){
					
						//Centro
						case 1:
						if((RotacionMueble == -1 ) || (RotacionMueble == 1 ))
						{
							SafePlace.x = WestString2;
					
						}else{
							if(!RotationOperation.IsActive)
							   {
								SafePlace.x = WestString;
					
							}else{
								SafePlace.x = WestString + MuebleX/10;
					

							}
							
						}
					
			
						SafePlace.z = transform.position.z;
						SafePlace.y = 0.1125f;
					//	Debug.Log ("WEST");
						TCOL =0;



						break;//fin N1

					//Abajo
					case 5:
						if((RotacionMueble == -1 ) || (RotacionMueble == 1 ))
						{
							SafePlace.x = WestString2;
							
						}else{
							if(!RotationOperation.IsActive)
							{
								SafePlace.x = WestString;
								
							}else{
								SafePlace.x = WestString + MuebleX/10;
								
								
							}
							
						}
						
						
						SafePlace.z = transform.position.z;
						SafePlace.y = 0.1125f;
					//	Debug.Log ("WEST");
						TCOL =0;
						
						
						
						break;

						//Izquierda
					case 11:


						if((RotacionMueble == -1 ) || (RotacionMueble == 1 ))
						{
							SafePlace.x = PosicionE+ MuebleX/2;
							
						}else{
							if(!RotationOperation.IsActive)
							{
								SafePlace.x = PosicionE+ MuebleX;
								
							}else{
									SafePlace.x = (PosicionE+ MuebleX )+ MuebleX/10;
								
								
							}
							
						}
						
						
						SafePlace.z = transform.position.z;
						SafePlace.y = 0.1125f;
					//	Debug.Log ("WEST");
						TCOL =0;
						
						
						
						break;

					}

//					fsdfsfsdfsdfsdf Lo deje aca!!!!!!!
					break;
	/**/		case 3 :
					
					//s

					
					if((RotacionMueble == -1 ) || (RotacionMueble == 1 ))
					{
						SafePlace.z = SouthString2 ;

					}else{
						if(!RotationOperation.IsActive)
						{
							SafePlace.z = SouthString2 -MuebleZ/10;
						}else{
							SafePlace.z = SouthString2 + MuebleZ/10;
						
							
						}
						
					}
					//------

			
					SafePlace.x = transform.position.x;

					SafePlace.y = 0.1125f;
				//	Debug.Log ("SOUTH");
					TCOL =0;
					break;//fin case 3
	/**/		case 4 :
					
					//North
					//--

					if((RotacionMueble == -1 ) || (RotacionMueble == 1 ))
					{
						SafePlace.z = NorthString2 ;

					}else{
						if(!RotationOperation.IsActive)
						{
							SafePlace.z = NorthString+ MuebleZ/10;//- MuebleZ/10;
						
						}else{
							SafePlace.z = NorthString2- MuebleZ/10;
						
							
						}
						
					}
					//--
					SafePlace.x = transform.position.x;

					SafePlace.y = 0.1125f;
				//	Debug.Log ("NORTH");
					TCOL =0;
					break;//fin case 4

					//Fin Habitacion Cuadrada
					//Habitacion L
					//Fin Habitacion L
					//Habitacion T
					//Fin Habitacion T
/**/			default:
					return new Vector3 (0, -100, 0);
					//Debug.Log("NewVector3");
					break;
				}
				
				
				
				
				
				
				
				
			}else {
				
				
				
				
				SafePlace = SafeAux;
				
				
				
				
			}
			this.transform.position = SafePlace;
			return SafePlace;	
			
		}
	}
	
	
	
	
	
	
	private void ExecuteOperations()
	{
		if (Selected)
		{
			
			if (TranslationOperation.IsActive || RotationOperation.IsActive)
			{
				if (RotationOperation.IsActive) RotationOperation.Validate();
				if (TranslationOperation.IsActive) TranslationOperation.Validate();
				//	OnDrawGizmosSelected();
			}
			else
			{
				TranslationOperation.FixedUpdate();//Activado por smb
				//RotationOperation.FixedUpdate();
				//OnDrawGizmosSelected();
			}
		}
	}
	
	internal void ValidateSpawn(Vector3 position)
	{
	}
	
	internal GameObject GetRotationAnchor()
	{
		return RotationAnchor;
	}
	
	/*void OnDrawGizmosSelected()
    {
        for (int i = 0; i < colliding.Count; i++)
        {
            closestPoint = colliding[i].ClosestPointOnBounds(transform.position);
            Debug.DrawLine(this.GetComponent<Collider>().ClosestPointOnBounds(closestPoint), closestPoint);

        }
    }*/
	
	
	
	internal void IsSafe()
	{
		colliding.Clear();
		colliders = colliding.ToArray();
		Vector3 resvaltransf = ValidateTransform();
		
	}
	
	public void BorrarSceneObject(){
		Destroy (gameObject);
		//SceneManager.AssetsSuscribers.Values.ToList().ForEach(v => v.Remove(this));
		
	}
	
	
}
