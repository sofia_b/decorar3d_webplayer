﻿using UnityEngine;
using System.Collections;

public class RoomSelectButton : MonoBehaviour {
    public int Index=-1;
    public RoomSelector Selector;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Select() {
        Selector.SelectRoom(Index);
    }

	public void ResetSelectButton(){

		Index = -1;
		Select ();
	}
}
