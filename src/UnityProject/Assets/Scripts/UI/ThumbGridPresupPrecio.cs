﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class ThumbGridPresupPrecio : MonoBehaviour {
	public GameObject thumbPrefabItems;//crea gameobject para las imagenes Thumbnails
	public List<float> ActiveItems = new List<float>();//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
	public Dictionary<float, GameObject> MappedThumbs = new Dictionary<float, GameObject>();//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
	public int CountItems;
	
	public void InitListPrecio() {
		ClearThumbs();
		ActiveItems.Clear ();
		foreach (ListItemPresupuesto valores in GameObject.FindWithTag("PresupuestoTag").GetComponent<Presupuesto>().listaDeItemsPresupuesto) {
			ActiveItems.Add(0);//valores.Cantidad);//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
		}
		CountItems = ActiveItems.Count();
	}
	
	public void DeleteItems() {
		ClearThumbs ();
		foreach(Transform child in transform) {
			Destroy(child.gameObject);
		}
		ActiveItems.Clear ();
	}
	
	public void ClearThumbs() {
		foreach (var item in ActiveItems) {
			if (MappedThumbs.ContainsKey(item)) {
				MappedThumbs[item].SetActive(false);
				MappedThumbs.Remove(item);
			}
		}
	}
	
	public GameObject GetThumb(int item) {
		if (MappedThumbs.ContainsKey(item)) {
			return MappedThumbs[item];
		}else {
			return null;
		}
	}
	
	public GameObject CreateThumb(int id,float item) {//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
		GameObject thumb = Instantiate(thumbPrefabItems) as GameObject;
		thumb.name = "Precio-"+GameObject.FindWithTag("PresupuestoTag").GetComponent<Presupuesto>().listaDeItemsPresupuesto[id].Nombre;

		thumb.GetComponentInChildren<Text> ().text = item.ToString();
		thumb.GetComponentInChildren<RectTransform>().SetParent(transform);

		if (MappedThumbs.ContainsKey(item)) {
			MappedThumbs[item] = thumb;
		}
		else {
			MappedThumbs.Add(item, thumb);
		}
		return thumb;
	}
	
	public void LoadThumbs() {
		if (ActiveItems != null)
		{
			for (int i = 0; i < ActiveItems.Count(); i++)
			{
				float item = ActiveItems[i];//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
				GameObject thumb = CreateThumb(i,item);
				thumb.SetActive(true);
			}
		}
	}
}