﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

//Boton que muestra thumbnail de mueble en categorias
public class GridThumbButton : VariationButton {

  public override void Load() {

        if (Variation != null) {
            LoadThumbail();



            if (!IsThumbnailLoaded) {
                if (!string.IsNullOrEmpty(Variation.thumbnail)) {
                    SceneManager.SceneManagerInstance.QueueForDownload(gameObject, new List<AssetInfo>() { new AssetInfo(AssetInfo.AssetType.Thumbnail, Variation.thumbnail) }.ToArray());

                    ShowLoading();
                }
                else {
                    IsThumbnailLoaded = true;// si cargo
                    HideLoading();// oculta el elemento que rota.

                }
            }



        }
    }
    protected override void OnEnable()
    {
        base.OnEnable();
    }
    public void OnDisable()
    {
    }

	public string MuebleNombre;
	public string MuebleID;
	public string MuebleImage;
	//public string MuebleImage2;
	void Start(){
		//MuebleNombre = this.Item.name;
		//MuebleID = this.Item.id;
		//MuebleImage = this.Item.thumbnail;
		MuebleNombre = "Nombre";
		MuebleID = "0";
		MuebleImage = "";
	}


	public GameObject PanelInfo;
	public Image ImagePanelInfo;
	public void OnMouseOver(){
		//Debug.Log("mouseover en thumb");
		MuebleNombre = this.Item.name;
		MuebleID = this.Item.id;
		//MuebleImage = this.Item.thumbnail;
		MuebleImage = this.Variation.thumbnail.ToString();
	

//		Debug.Log ("Nombre= " + MuebleNombre);
//		
//		Debug.Log ("ID= " + MuebleID);
		
//		Debug.Log ("Image= " + MuebleImage);
		PanelInfo.GetComponentInChildren<Text> ().text = MuebleNombre;

//		ImagePanelInfo.GetComponent<Image> ().mainTexture = MuebleImage;
		DownloadI ();


	}
	void DownloadI(){
		StartCoroutine (downloadThumbnail ());
	}


	IEnumerator downloadThumbnail(){



		yield return new WaitForEndOfFrame();
		//CatalogueURL
		var ThumbForm = new WWWForm ();
		
		var ThumbUp = new WWW(MuebleImage,ThumbForm);
		
		yield return ThumbUp;



		//Renderer renderer = ImagePanelInfo.GetComponent<Renderer>();
		//renderer.material.mainTexture = ThumbUp.texture;

		Image ImgThumb = ImagePanelInfo.GetComponent<Image> ();
		//ImgThumb = ThumbUp;
		ImgThumb.overrideSprite = Sprite.Create(ThumbUp.texture, new Rect(0, 0, ThumbUp.texture.width, ThumbUp.texture.height), new Vector2(0.5f, 0.5f));
		                                        
		//renderer.material.mainTexture.
		/*
		textureSpriteOVER = ImagePanelInfo.GetComponentInChildren<Image>();
		textureSpriteOVER.mainTexture = ThumbUp.texture;
*/
	}



}
