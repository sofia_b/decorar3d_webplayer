﻿using UnityEngine;
using System.Collections;

public class Draggable : MonoBehaviour {

    protected Vector3 point;
    protected Vector3 offset;
    public Transform target;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void StartDragging() {
        point = Input.mousePosition;

        offset = target.position - point;
    }
    public void Dragging() {

        target.position = Input.mousePosition + offset;
    }

}
