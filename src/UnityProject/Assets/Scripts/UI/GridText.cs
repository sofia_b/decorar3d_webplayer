﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class GridText : MonoBehaviour {
	
	
	int rowIndex;
	int colIndex;
	float height;
	float rowHeight;
	int i;
	GameObject thumb;
	RectTransform rect;
	
	public interface IGridTextInfo {
		Guid UID { get; set; }
		string Text { get; set; }
		object Data { get; set; }
	}
	public class GridTextInfo : IGridTextInfo {
		public GridTextInfo() {
			UID = Guid.NewGuid();
		}
		public GridTextInfo(Guid uid) {
			UID = uid;
		}
		public virtual Guid UID {
			get;
			set;
		}
		
		public virtual string Text {
			get;
			set;
		}
		public object Data { get; set; }
	}
	public bool calculateHorizontalMargin = false;
	public float horizontalMargin = 10;
	public float verticalMargin = 10;
	
	public GameObject thumbPrefab;
	private RectTransform thumbPrefabRect;
	private RectTransform gridRectTransform;
	
	private IList<IGridTextInfo> ActiveItems = new List<IGridTextInfo>();
	private Dictionary<string, GameObject> MappedThumbs = new Dictionary<string, GameObject>();
	// Use this for initialization
	void Start() {
		gridRectTransform = gridRectTransform ?? this.GetComponent<RectTransform>();
		thumbPrefabRect = thumbPrefabRect ?? thumbPrefab.GetComponent<RectTransform>();
		
		Build();
	}
	
	// Update is called once per frame
	void Update() {
		
	}
	
	[ContextMenu("Build")]
	private void Build() {
		
		BuildGrid(ActiveItems);
	}
	
	public void BuildGrid(IList<IGridTextInfo> items) {
		ClearThumbs();
		ActiveItems = items;
		LoadThumbs(items);
	}
	
	private void ClearThumbs() {
		Debug.Log("Clearing " + ActiveItems.Count + " items");
		foreach (var item in ActiveItems) {
			if (MappedThumbs.ContainsKey(item.UID.ToString()))
			{
				MappedThumbs[item.UID.ToString()].SetActive(false);
			}
		}
	}
	
	private void LoadThumbs(IEnumerable<IGridTextInfo> items) {
		gridRectTransform = gridRectTransform ?? this.GetComponent<RectTransform>();
		thumbPrefabRect = thumbPrefabRect ?? thumbPrefab.GetComponent<RectTransform>();
		var thumbsByRow = Mathf.FloorToInt(gridRectTransform.rect.width / (thumbPrefabRect.rect.width + (calculateHorizontalMargin ? 0 : horizontalMargin)));
		if (calculateHorizontalMargin) {
			horizontalMargin = ((gridRectTransform.rect.width - (thumbPrefabRect.rect.width * thumbsByRow)) / thumbsByRow) / 2;
		}
		
		rowIndex = 0;
		colIndex = 0;
		height = 0;
		rowHeight = 0;
		i = 0;
		foreach (IGridTextInfo item in items) {
			
			thumb = GetThumb(item);
			if (thumb == null) {
				thumb = CreateThumb(item);
			}
			else {
				thumb.SetActive(true);
			}
			
			thumb.transform.SetSiblingIndex(i);
			thumb.transform.localPosition = new Vector3(0, 0, transform.localPosition.z);
			thumb.transform.localScale = Vector3.one;
			rect = thumb.GetComponent<RectTransform>();
			var x = (colIndex * (horizontalMargin * 2)) + colIndex * thumbPrefabRect.rect.width + thumbPrefabRect.rect.width / 2 + horizontalMargin;
			var y = (rowIndex * verticalMargin) + rowIndex * thumbPrefabRect.rect.height + thumbPrefabRect.rect.height / 2 + ((rowIndex > 0) ? verticalMargin : verticalMargin / 2);
			if (rowHeight < thumbPrefabRect.rect.height)
				rowHeight = thumbPrefabRect.rect.height;
			rect.pivot = new Vector2(0.5f, 0.5f);
			rect.anchorMin = new Vector2(0, 1);
			rect.anchorMax = new Vector2(0, 1);
			rect.anchoredPosition = new Vector2(x, y * -1);
			
			colIndex++;
			if (colIndex >= thumbsByRow) {
				colIndex = 0;
				rowIndex++;
				height += rowHeight;
				height += verticalMargin;
				rowHeight = 0;
			}
			i++;
		}
		if (rowHeight > 0) {
			height += rowHeight;
			height += verticalMargin;
			rowHeight = 0;
		}
		//gridRectTransform.sizeDelta = new Vector2(gridRectTransform.sizeDelta.x, height);
	}
	private GameObject GetThumb(IGridTextInfo item) {
		if (MappedThumbs.ContainsKey(item.UID.ToString())) {
			return MappedThumbs[item.UID.ToString()];
		}
		else {
			return null;
		}
	}
	private GameObject CreateThumb(IGridTextInfo item) {
		thumb = Instantiate(thumbPrefab) as GameObject;
		thumb.name = "Thumb " + item.UID.ToString();
		thumb.transform.parent = transform;
		thumb.SendMessage("LoadData", item, SendMessageOptions.DontRequireReceiver);
		UnityEngine.UI.Text textComponent = thumb.GetComponent<UnityEngine.UI.Text>();
		textComponent.text = item.Text;
		if (MappedThumbs.ContainsKey(item.UID.ToString())) {
			MappedThumbs[item.UID.ToString()] = thumb;
		}
		else {
			MappedThumbs.Add(item.UID.ToString(), thumb);
		}
		return thumb;
	}
}
