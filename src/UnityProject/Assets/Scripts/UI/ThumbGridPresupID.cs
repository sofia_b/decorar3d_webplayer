﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class ThumbGridPresupID : MonoBehaviour {
	public GameObject thumbPrefabItems;//crea gameobject para las imagenes Thumbnails
	public List<string> ActiveItems = new List<string>();//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
	public Dictionary<string, GameObject> MappedThumbs = new Dictionary<string, GameObject>();//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
	public int CountItems;

	public void InitListID() {
		ClearThumbs();
		ActiveItems.Clear ();
		foreach (ListItemPresupuesto valores in GameObject.FindWithTag("PresupuestoTag").GetComponent<Presupuesto>().listaDeItemsPresupuesto) {
			ActiveItems.Add(valores.idI);//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
		}
		CountItems = ActiveItems.Count();
	}

	public void DeleteItems() {
		ClearThumbs ();
		foreach(Transform child in transform) {
			Destroy(child.gameObject);
		}
		ActiveItems.Clear ();
	}
	
	public void ClearThumbs() {
		foreach (var item in ActiveItems) {
			if (MappedThumbs.ContainsKey(item)) {
				MappedThumbs[item].SetActive(false);
				MappedThumbs.Remove(item);
			}
		}
	}
	
	public GameObject GetThumb(string item) {
		if (MappedThumbs.ContainsKey(item)) {
			return MappedThumbs[item];
		}else {
			return null;
		}
	}
	
	public GameObject CreateThumb(int id,string item) {//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
		GameObject thumb = Instantiate(thumbPrefabItems) as GameObject;
		thumb.name = "ID-"+GameObject.FindWithTag("PresupuestoTag").GetComponent<Presupuesto>().listaDeItemsPresupuesto[id].Nombre;
		thumb.GetComponent<Text> ().text = item;
		thumb.GetComponent<RectTransform>().SetParent(transform);
		if (MappedThumbs.ContainsKey(item)) {
			MappedThumbs[item] = thumb;
		}
		else {
			MappedThumbs.Add(item, thumb);
		}
		return thumb;
	}
	
	public void LoadThumbs() {
		if (ActiveItems != null)
		{
			for (int i = 0; i < ActiveItems.Count(); i++)
			{
				string item = ActiveItems[i];//ACA ESDONDE SECAMBIA EL TIPO DE VALOR
				GameObject thumb = CreateThumb(i,item);
				thumb.SetActive(true);
			}
		}
	}
}