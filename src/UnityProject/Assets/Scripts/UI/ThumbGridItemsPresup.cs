﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class ThumbGridItemsPresup : MonoBehaviour {
	//    public GameObject thumbPrefabItems;//crea gameobject para las imagenes Thumbnails
    private RectTransform gridRectTransform;//Crea
	//public GameObject InfoPrev;
	//private RectTransform gridInfoPrev;




	//Crea una lista que contiene las variantes de los items del catalogo
  //  private List<ga
		//> ActiveItems = new List<Assets.Scripts.Catalogue.CatalogueItemVariation>();
/*   
	private Dictionary<string, GameObject> MappedThumbs = new Dictionary<string, GameObject>();
    private UnityEngine.UI.GridLayoutGroup gridGroup;
    public UnityEngine.UI.Scrollbar scrollbar;

    public int MaxAmount = 0;

	public int CountItems;
    // Use this for initialization
    void Start() {
        Init();
    }
    void OnEnable()
    {
        Init();
    }
    
    

    private void Init() {
        Debug.Log("Init ThumbGrid", this);
        gridRectTransform = gridRectTransform ?? this.GetComponent<RectTransform>();
        gridGroup = this.GetComponent<UnityEngine.UI.GridLayoutGroup>();
    }

    // Update is called once per frame
    void Update() {

    }

    [ContextMenu("Build")]
    private void Build() {

        BuildGrid(ActiveItems);
    }
	//Construye la Guia y carga los Thumbs
    public void BuildGrid(IList<Assets.Scripts.Catalogue.CatalogueItemVariation> items) {
        Debug.Log("Building grid " + items.Count, this);
        ClearThumbs();
        ActiveItems = items;
        LoadThumbs(items);
    }



    private void ClearThumbs() {
        Debug.Log("Clearing " + ActiveItems.Count + " items", this);
        foreach (var item in ActiveItems) {
            if (MappedThumbs.ContainsKey(item.UID.ToString())) {
                MappedThumbs[item.UID.ToString()].SetActive(false);
            }
        }
    }

    protected string[] loadedItems;
    private void LoadThumbs(IList<Assets.Scripts.Catalogue.CatalogueItemVariation> items) {
        if (gridGroup == null || gridRectTransform == null) Init();
        gridRectTransform = gridRectTransform ?? this.GetComponent<RectTransform>();
        var thumbsByRow = Mathf.FloorToInt(((gridRectTransform.rect.width - gridGroup.padding.right - gridGroup.padding.left) / (gridGroup.cellSize.x + gridGroup.spacing.x)));//Crea los recuadros y calcula el espacio para acomodarlos uno al lado del otro-smb
       Debug.Log("Per row: " + thumbsByRow.ToString(), this);
        int rowIndex = 0;
        int colIndex = 0;
        float height = 0;
        float rowHeight = 0;
        if (items != null)
        {
            loadedItems = items.ToList().Select(i => i.UID.ToString()).ToArray();
			CountItems = items.Count();
            for (int i = 0; i < items.Count(); i++)
			{
                Assets.Scripts.Catalogue.CatalogueItemVariation item = items[i];
                if (MaxAmount != 0 && i >= MaxAmount)
                {
                    break;
                }
                GameObject thumb = GetThumb(item);
                if (thumb == null) {
                    thumb = CreateThumb(item);
                }
                thumb.SetActive(true);

                //thumb.transform.SetSiblingIndex(i);
                thumb.transform.localPosition = new Vector3(0, 0, transform.localPosition.z);
                thumb.transform.localScale = Vector3.one;
                if (rowHeight < gridGroup.cellSize.y)
                    rowHeight = gridGroup.cellSize.y;
                //thumb.transform.localPosition = new Vector3(x, y*-1, transform.localPosition.z);


                colIndex++;
                if (colIndex >= thumbsByRow) {
                    colIndex = 0;
                    height += rowHeight + gridGroup.spacing.y;
                   Debug.Log("Row " + rowIndex.ToString() + ": " + rowHeight.ToString() + " / " + height.ToString(), this);
                    rowHeight = 0;
                    rowIndex++;
                }
                thumb.SetActive(true);
                thumb.GetComponent<GridThumbButton>().Load();

            }
        }
        if (rowHeight > 0) {
            height += rowHeight;
            rowHeight = 0;
        }
        height += gridGroup.padding.top + gridGroup.padding.bottom;
        Debug.Log("Total Height: " + height, this);
        //gridRectTransform.sizeDelta = new Vector2(gridRectTransform.sizeDelta.x, height);
        gridRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
        if (scrollbar != null) {
            if (scrollbar.size == 1) {
                
                scrollbar.GetComponent<CanvasGroup>().alpha = 0;
                scrollbar.GetComponent<CanvasGroup>().interactable = false;
                scrollbar.GetComponent<CanvasGroup>().blocksRaycasts = false;
            }
            else {
                scrollbar.GetComponent<CanvasGroup>().alpha = 1;
                scrollbar.GetComponent<CanvasGroup>().interactable = true;
                scrollbar.GetComponent<CanvasGroup>().blocksRaycasts = true;
            }
        }

    }
    private GameObject GetThumb(Assets.Scripts.Catalogue.CatalogueItemVariation item) {
        if (MappedThumbs.ContainsKey(item.UID.ToString())) {
            return MappedThumbs[item.UID.ToString()];
			Debug.Log("funciona?"+MappedThumbs[item.UID.ToString()]);
        }
        else {
            return null;
        }
    }
  

	private GameObject CreateThumb(Assets.Scripts.Catalogue.CatalogueItemVariation item) {
        GameObject thumb = Instantiate(thumbPrefab) as GameObject;
        thumb.name = "Thumb " + item.UID.ToString();
        thumb.GetComponent<RectTransform>().SetParent(transform);
		GridThumbButton button = thumb.GetComponent<GridThumbButton>();
		button.Variation = item;
        button.Item = item.Item;

	



        if (MappedThumbs.ContainsKey(item.UID.ToString())) {
            MappedThumbs[item.UID.ToString()] = thumb;
        }
        else {
            MappedThumbs.Add(item.UID.ToString(), thumb);
        }
        return thumb;
    }

*/




}
