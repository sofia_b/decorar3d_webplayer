﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class GridTextT : MonoBehaviour {
	/*
	
	int rowIndex;
	int colIndex;
	float height;
	float rowHeight;
	int i;
	GameObject thumb;
	RectTransform rect;
	
	public interface IGridTextInfo {
		Guid UID { get; set; }
		string Text { get; set; }
		object Data { get; set; }
	}
	public class GridTextInfo : IGridTextInfo {
		public GridTextInfo() {
			UID = Guid.NewGuid();
		}
		public GridTextInfo(Guid uid) {
			UID = uid;
		}
		public virtual Guid UID {
			get;
			set;
		}
		
		public virtual string Text {
			get;
			set;
		}
		public object Data { get; set; }
	}
	public bool calculateHorizontalMargin = false;
	public float horizontalMargin = 10;
	public float verticalMargin = 10;
	
	//public GameObject thumbPrefab;
	private RectTransform thumbPrefabRect;
	private RectTransform gridRectTransform;
	
	private IList<IGridTextInfo> ActiveItems = new List<IGridTextInfo>();
	private Dictionary<string, GameObject> MappedThumbs = new Dictionary<string, GameObject>();
	// Use this for initialization
	void Start() {
		gridRectTransform = gridRectTransform ?? this.GetComponent<RectTransform>();
		//	thumbPrefabRect = thumbPrefabRect ?? thumbPrefab.GetComponent<RectTransform>();
		
		Build();
	}
	
	// Update is called once per frame
	void Update() {
		
	}
	
	[ContextMenu("Build")]
	private void Build() {
		
		BuildGrid(ActiveItems);
	}
	
	public void BuildGrid(IList<IGridTextInfo> items) {
		ClearThumbs();
		ActiveItems = items;
		//LoadThumbs(items);
	}
	
	private void ClearThumbs() {
		//Debug.Log("Clearing " + ActiveItems.Count + " items");
		foreach (var item in ActiveItems) {
			if (MappedThumbs.ContainsKey(item.UID.ToString()))
			{
				MappedThumbs[item.UID.ToString()].SetActive(false);
			}
		}
	}

	private GameObject GetThumb(IGridTextInfo item) {
		if (MappedThumbs.ContainsKey(item.UID.ToString())) {
			return MappedThumbs[item.UID.ToString()];
		}
		else {
			return null;
		}
	}
*/
}
