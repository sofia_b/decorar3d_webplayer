﻿using UnityEngine;
using System.Collections;

public class ShowBox : MonoBehaviour {

    public Color lineColor = Color.red;

    static Material lineMaterial;
    static void CreateLineMaterial() {
	    if( !lineMaterial ) {
		    lineMaterial = new Material( "Shader \"Lines/Colored Blended\" {" +
			    "SubShader { Pass { " +
			    "    Blend SrcAlpha OneMinusSrcAlpha " +
			    "    ZWrite Off Cull Off Fog { Mode Off } " +
			    "    BindChannels {" +
			    "      Bind \"vertex\", vertex Bind \"color\", color }" +
			    "} } }" );
		    lineMaterial.hideFlags = HideFlags.HideAndDontSave;
		    lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
	    }
    }
	// Use this for initialization
	void Start () {

        CreateLineMaterial();
	}

    
	
	// Update is called once per frame
    void Update()
    {
        // set the current material
        lineMaterial.SetPass(0);

        MeshCollider collider = this.GetComponent<MeshCollider>();
        var boundPoint1 = collider.bounds.min;
        var boundPoint2 = collider.bounds.max;
        var boundPoint3 = new Vector3(boundPoint1.x, boundPoint1.y, boundPoint2.z);
        var boundPoint4 = new Vector3(boundPoint1.x, boundPoint2.y, boundPoint1.z);
        var boundPoint5 = new Vector3(boundPoint2.x, boundPoint1.y, boundPoint1.z);
        var boundPoint6 = new Vector3(boundPoint1.x, boundPoint2.y, boundPoint2.z);
        var boundPoint7 = new Vector3(boundPoint2.x, boundPoint1.y, boundPoint2.z);
        var boundPoint8 = new Vector3(boundPoint2.x, boundPoint2.y, boundPoint1.z);

        //if you want to visualize the box you can add to MrSkiz's code:
         
         // rectangular cuboid
         // top of rectangular cuboid (6-2-8-4)
         GL.Begin(GL.LINES);
         GL.Color(lineColor);
         GL.Vertex(boundPoint6);
         GL.Vertex(boundPoint2);
         GL.Vertex(boundPoint8);
         GL.Vertex(boundPoint4);
         GL.Vertex(boundPoint6);
         GL.End();

        /*
         Debug.DrawLine (boundPoint6, boundPoint2, lineColor);
         Debug.DrawLine (boundPoint2, boundPoint8, lineColor);
         Debug.DrawLine (boundPoint8, boundPoint4, lineColor);
         Debug.DrawLine (boundPoint4, boundPoint6, lineColor);
 
         // bottom of rectangular cuboid (3-7-5-1)
         Debug.DrawLine (boundPoint3, boundPoint7, lineColor);
         Debug.DrawLine (boundPoint7, boundPoint5, lineColor);
         Debug.DrawLine (boundPoint5, boundPoint1, lineColor);
         Debug.DrawLine (boundPoint1, boundPoint3, lineColor);
 
         // legs (6-3, 2-7, 8-5, 4-1)
         Debug.DrawLine (boundPoint6, boundPoint3, lineColor);
         Debug.DrawLine (boundPoint2, boundPoint7, lineColor);
         Debug.DrawLine (boundPoint8, boundPoint5, lineColor);
         Debug.DrawLine (boundPoint4, boundPoint1, lineColor);
        */
	}
}
