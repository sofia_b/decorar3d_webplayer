using UnityEngine;

public class MouseOrbitOnDemand : MonoBehaviour
{
    public Transform target;
    public float currentDistance = 5.0f;


    public float minDistance = 1f;
    public float maxDistance = 10f;

    public float xSpeed = 250.0f;
    public float ySpeed = 120.0f;
    public float zSpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    private float x = 0.0f;
    private float y = 0.0f;

    private Vector3 currentRotation;
    SceneManager manager;
    private bool IsPanning = false;
    void Start()
    {
        var managerObject = GameObject.FindGameObjectWithTag(SceneManager.DefaultTag);
        if (managerObject != null) manager = managerObject.GetComponent<SceneManager>();
        var angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        currentRotation = angles;
        // Make the rigid body not change rotation
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;


    }

    void LateUpdate()
    {
        if (Input.GetMouseButton(1))
        {
            IsPanning = true;
            if (manager != null) manager.SetCursor(Cursors.Pan);
            if (target)
            {
                x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
                y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

                y = ClampAngle(y, yMinLimit, yMaxLimit);
                currentRotation = new Vector3(y, x, 0f);

            }
        }
        else if (IsPanning)
        {
            if (manager != null) manager.SetCursor(Cursors.None);
            IsPanning = false;
        }
        var z = Input.GetAxis("Mouse ScrollWheel") * zSpeed * 0.02f * -1;
        if (z != 0.0f)
        {
            currentDistance += z;
        }

        UpdateRotationAndPosition();

    }

    void UpdateRotationAndPosition()
    {
        var rotation = Quaternion.Euler(currentRotation);
        currentDistance = Mathf.Clamp(currentDistance, minDistance, maxDistance);
        var position = rotation * new Vector3(0.0f, 0.0f, -currentDistance) + target.position;
        transform.rotation = rotation;
        transform.position = position;
    }

    float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360f)
            angle += 360f;
        if (angle > 360f)
            angle -= 360f;
        return Mathf.Clamp(angle, min, max);
    }
}