using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using System;

public class VariationButton : MonoBehaviour {
	
	
	public CatalogueItem Item;
	public CatalogueItemVariation Variation;
	public  CatalogueMaterial material;
	public  CatalogueMaterialesSet materialesSet;

	
	public bool IsThumbnailLoaded = false;
	public bool isContentLoaded = false;
	private bool loadingObject;
	private ObjReader.ObjData objData;
	public GameObject Loading;
	
	UITexture textureSprite;
	CatalogueItem item;
	GameObject obj;


	void start(){
		LoadThumbail ();
	}
	
	// Update is called every frame, if the
	// MonoBehaviour is enabled.
	void Update()
	{

	}
	
	public virtual void Load()
	{
		
		if (Variation != null)
		{
			LoadTexture(Variation.thumbnail);
			if (!IsThumbnailLoaded)
			{
				if (!string.IsNullOrEmpty(Variation.thumbnail))
				{
					ShowLoading();
					SceneManager.SceneManagerInstance.QueueForDownload(gameObject, new List<AssetInfo>() { new AssetInfo(AssetInfo.AssetType.Thumbnail, Variation.thumbnail) }.ToArray());
				}
				else
				{
					HideLoading();
					IsThumbnailLoaded = true;
				}
			}
		}
	}
	protected virtual void OnEnable()
	{
		if (!IsThumbnailLoaded) Load();
	}
	
	protected virtual void HideLoading()
	{
		Loading.SetActive(false);
	}
	
	protected virtual void ShowLoading()
	{
		Loading.SetActive(true);
	}
	public void TextureDownloaded(KeyValuePair<string, Texture2D> textureKVP)
	{
		
		//Debug.Log(string.Format("TextureDownloaded Received: {0}", textureKVP.Key));
		//if (Variation == null) return;
		if (Variation!=null && textureKVP.Key == Variation.thumbnail)
		{
			if (IsThumbnailLoaded) return;
			//Debug.Log(string.Format("TextureDownloaded Received: Expected {0} . Received: {1}. Matches? {2}", Variation.thumbnail, textureKVP.Key, (textureKVP.Key == Variation.thumbnail)));
			LoadTexture(textureKVP.Key);
		}
		else if (Variation != null &&  (Item.type == CatalogueItemType.WallPainting || Item.type == CatalogueItemType.FloorPainting))
		{
			//Debug.Log(string.Format("TextureDownloaded Received: Expected {0} . Received: {1}. Matches? {2}", Variation.thumbnail, textureKVP.Key, (textureKVP.Key == Variation.materials[0].texture)));
			if (Variation.Setmaterials[0].texture == textureKVP.Key)
			{
				if (Item.type == CatalogueItemType.WallPainting)
				{
					SceneManager.SceneManagerInstance.SetWallTexture(Item.name, Variation.Setmaterials[0].texture);
				}
				else if (Item.type == CatalogueItemType.FloorPainting)
				{
					SceneManager.SceneManagerInstance.SetFloorTexture(Item.name, Variation.Setmaterials[0].texture, Item.category);

				}
				isContentLoaded = true;
				HideLoading();
			}
		}
	}
	
	public void SpriteDownloaded(KeyValuePair<string, Sprite> spriteKVP) {
		
		if (IsThumbnailLoaded || Variation == null) return;
		//Debug.Log(string.Format("SpriteDownloaded Received: Expected {0} . Received: {1}. Matches? {2}", Variation.thumbnail, spriteKVP.Key, (spriteKVP.Key == Variation.thumbnail)));
		if (spriteKVP.Key == Variation.thumbnail) {
			LoadThumbail();
		}
	}
	
	protected void LoadTexture(string path) {
		if (path == Variation.thumbnail)
		{
			LoadThumbail();
		}
		else
		{
			if (Item.type == CatalogueItemType.WallPainting)
			{
				SceneManager.SceneManagerInstance.SetWallTexture(Item.name, Variation.Setmaterials[0].texture);
			}
			else if (Item.type == CatalogueItemType.FloorPainting)
			{
			//	Debug.Log("ssssssssssssssssssssssssssss "+Variation.Setmaterials[0].texture);
				SceneManager.SceneManagerInstance.SetFloorTexture(Item.name, Variation.Setmaterials[0].texture, Item.category);
			}
		}
		
	}
	
	public void LoadThumbail() {
		var thumbnail = SceneManager.SceneManagerInstance.GetSprite(Variation.thumbnail);
		//textureSprite.material = material;
		
		
		if (thumbnail == null) {
			IsThumbnailLoaded = false;
			ShowLoading();
		}
		else {
			SetThumbnail(thumbnail);
			IsThumbnailLoaded = true;
			HideLoading();
		}
	}
	
	protected virtual void SetTexture(Texture texture) {
		
		textureSprite = this.GetComponentInChildren<UITexture>();
		textureSprite.mainTexture = texture;
	}
	protected virtual void SetThumbnail(Sprite sprite) {
		UnityEngine.UI.Image image = this.GetComponentInChildren<UnityEngine.UI.Image>();
		image.sprite = sprite;
	}



	CatalogueItemVariation varN;
	public string WallName = "WallName";
	public string FloorName;
	public string FloorCategory;

	public void Clicked()
	{


		if (Item != null)
		{

			//WallPainting
			if (Item.type == CatalogueItemType.WallPainting)
			{
				SceneManager.SceneManagerInstance.SetWallTexture(Item.name, Variation.Setmaterials[0].texture);
				WallName = Item.name;

				//Debug.Log ("WallName:: " + Item.name);//Nombre Pared
			}

			//FloorPainting
			else if (Item.type == CatalogueItemType.FloorPainting)
			{

				SceneManager.SceneManagerInstance.SetFloorTexture(Item.name, Variation.Setmaterials[0].texture, Item.category);

				FloorName = Item.name;
				FloorCategory = Item.category;
				//Debug.Log ("FloorName:: " + Item.name);//Nombre Piso



			}
			else
			{

				///---------Aca Carga y posiciona el Objeto
				if (Item.modelObject == null)//	public CatalogueItem Item;
				{
					TryToLoadObject();
				}
				else if (Item.modelObject != null)//	public CatalogueItem Item;
				{
					PositionObject();//Posiciona el objeto
				}
			}
		}
	}
	
	void TryToLoadObject()
	{
		if (Item != null && Item.modelObject == null)
		{
			if (!loadingObject)
			{
				StartCoroutine(LoadModel());
			}
		}
	}


	public string idSetColores;
	private IEnumerator LoadModel()
	{
		
		item = Item as CatalogueItem;
		if (item != null && item.modelObject == null)
		{
			loadingObject = true;
			try
			{
				var url = item.model.IndexOf("http") > -1 ? item.model : Configuration.Endpoint + item.model;
				ObjReader.use.objPosition = item.offset;
				ObjReader.use.objRotation = item.rotation;
			//	objData = ObjReader.use.ConvertFileAsync(url, item.modelUseMtl, SceneManager.SceneManagerInstance.DefaultMaterial, SceneManager.SceneManagerInstance.TransparentMaterial);



				//Comprobacion de sets de colores



				///---***---///

				//Default

			objData = ObjReader.use.ConvertFileAsync(url/*url del modelo*/, item.modelUseMtl/*bool modelo*/, SceneManager.SceneManagerInstance.DefaultMaterial, SceneManager.SceneManagerInstance.TransparentMaterial);


			
			
			}
			catch (UnityException ex)
			{
				Debug.LogError(ex);
			}
			while (!objData.isDone)
			{
				ReportProgress(item, objData);
				yield return null;
			}
			loadingObject = false;
			if (objData == null || objData.gameObjects == null)
			{
				ReportError(item, objData);
				yield return false;
			}
			else
			{
				obj = objData.gameObjects.Length > 1 ? new GameObject() : null;
				for (int i = 0; i < objData.gameObjects.Length; i++)
				{
					if (objData.gameObjects.Length > 1)
					{
						objData.gameObjects[i].transform.parent = obj.transform;
					}
					else
					{
						obj = objData.gameObjects[i];
					}
				}
				//Debug.Log(item.scale);
				item.modelObject = obj;
				item.modelObject.SetActive(false);
				SceneManager.SceneManagerInstance.ApplyCatalogueToObject(Item, Item.modelObject);
				ReportLoad(item);
				PositionObject();
			}
		}
	}
	
	private void ReportLoad(CatalogueItem item)
	{
		
		//label.text = item.name;
	}
	
	private void ReportError(CatalogueItem item, ObjReader.ObjData objData)
	{
		//Debug.LogError("Item not loaded: " + item.id + " (" + item.model + ")");
		throw new System.NotImplementedException();
	}
	
	private void ReportProgress(CatalogueItem item, ObjReader.ObjData objData)
	{
		//label.text = string.Concat(item.name, " ", (objData.progress * 100).ToString("f0"), "%");
	}
	void PositionObject()
	{
		if (Item.modelObject != null)
		{
			SceneManager.SceneManagerInstance.TryToSpawnModel(Item, new Vector3(), Variation);
		}
	}
	
	void OnTooltip(bool show)
	{
		if (show)
		{
			SceneManager.SceneManagerInstance.UIManager.ShowInfo(Item, Variation);
		}
		else
		{
			SceneManager.SceneManagerInstance.UIManager.HideInfo();
		}
	}
	
}
