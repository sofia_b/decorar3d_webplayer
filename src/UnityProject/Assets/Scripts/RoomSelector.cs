﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RoomSelector : MonoBehaviour {
	
	
	public RoomUIBehavior[] RoomUI;
	public int DefaultIndex = 0;
	public int SelectIndex = -1;
	protected RoomUIBehavior SelectedRoom;
	protected int currentIndex=-1;
	protected SceneManager SceneManager;

	//public Text TExtoUrl;

	public GameObject Menu;
	// Use this for initialization
	public void Start()
	{
		SceneManager = SceneManager ?? GameObject.FindGameObjectWithTag("SceneManager").GetComponent<SceneManager>();
		Reset();
		//TExtoUrl.text = SceneManager.CatalogueURL;
	
	}
	
	public void Reset()
	{
		if (RoomUI.Length > DefaultIndex)
		{
			var index = DefaultIndex;
			SelectRoom(index);
//			Debug.Log("Reset");
		}
	}
	
	public void SelectRoom(int index)
	{
		for (int i = 0; i < RoomUI.Length; i++)
		{
			RoomUI[i].gameObject.SetActive(false);
		}
		SelectedRoom = RoomUI[index];
		currentIndex = index;
		SelectedRoom.gameObject.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		if (SelectIndex > -1)
		{
			if (SelectIndex < RoomUI.Length)
			{
				SelectRoom(SelectIndex);
			}
			else
			{
				Debug.LogWarning(string.Format("There is no room ui index {0}", SelectIndex));
			}
			SelectIndex = -1;
		}
	}
	
	public void Decorate()
	{
		for (int i = 0; i < RoomUI.Length; i++)
		{
			RoomUI[i].IsDecorating = false;
			RoomUI[i].gameObject.SetActive(false);
		}
		this.gameObject.SetActive(false);
		this.SceneManager.GetComponent<UIManager>().SetSelectedRoom(SelectedRoom);
		Menu.SetActive (true);

	}
	public void Reset2(){
		SelectIndex = -1 ;
		Debug.Log ("Reset2");
		for (int i = 0; i < RoomUI.Length; i++)
		{
			RoomUI[i].IsDecorating = true;
			RoomUI[i].gameObject.SetActive(true);

		}
		var index = DefaultIndex;
		SelectRoom(index);

		
		
	}
}
