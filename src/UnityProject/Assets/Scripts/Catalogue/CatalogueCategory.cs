﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Catalogue
{
	public class CatalogueCategory
	{
		public string id;
		public string name;

		public string UrlItems;//new enero2017
		//public string thumbnail;
		public CatalogueCategory() {
			UID = Guid.NewGuid();
		}
		public CatalogueCategory(Guid uid) {
			UID = uid;
		}
		
		private List<CatalogueCategory> _children = new List<CatalogueCategory>();
		
		public List<CatalogueCategory> children
		{
			get { return _children; }
			set { _children = value; }
		}
		
		public string parentId { get; set; }
		public CatalogueCategory parent { get; set; }
		
		public Guid UID { get; private set; }
		
		public int level { get; set; }
	}
}
