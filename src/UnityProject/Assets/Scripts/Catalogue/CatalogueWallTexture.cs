﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Catalogue
{
    [Serializable]
    public class CatalogueWallTexture : CatalogueMaterial
    {
        public Vector2 defaultScaling = new Vector2(1, 1);
        public Vector2 defaultOffset = new Vector2(0, 0);
        public string materialName;
    }
}