﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Catalogue
{
	public class CatalogueItem
	{
		//---Basico
		public string id;
		public string category;
		public string name;
		public string description;
		public string model;
		public bool modelUseMtl;
		public string height;
		public string width;
		public string deep;
		public Vector3 scale = new Vector3(1, 1, 1);
		public Vector3 offset = new Vector3(0, 0, 0);
		public Vector3 rotation = new Vector3(0, 0, 0);
		
		public string thumbnail;
		public List<CatalogueItemVariation> variations = new List<CatalogueItemVariation>();
		
		public float widthF;
		public float heightF;
		
		//attached to wall
		public string fijadoA;
		public int Altura;
		//--------
		//---
	
		public Material materialMueble;
		
		public CatalogueItemType type;

		
		public GameObject modelObject
		{
			get;
			set;
		}
		
		
		
		
		public bool Featured { get; set; }
	}
	
	public enum CatalogueItemType
	{
		RoomObject,
		FloorPainting,
		WallPainting
	}
}
