﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Assets.Scripts.Catalogue
{
    [Serializable]
    public class Catalogue
    {
        public decimal version;
        public DateTime modifiedDate;
        public List<CatalogueCategory> Categories = new List<CatalogueCategory>();
        public List<CatalogueItem> Items = new List<CatalogueItem>();
        public List<CatalogueWallTexture> WallTextures = new List<CatalogueWallTexture>();
		//public List<CatalogueUsuario> Usuario = new List<CatalogueUsuario>();

    }
}