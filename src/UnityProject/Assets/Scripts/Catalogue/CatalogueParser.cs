using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Catalogue
{
	public static class CatalogueParser
	{
		public static string FeaturedPropertyName = "Featured";
		public static Material DefaultMaterial;
		
		//-----------//public static Catalogue Parse(string catalogueData)
		public static Catalogue Parse(string catalogueData)
		{
			//Debug.Log("Catalogue length: " + catalogueData.Length.ToString());
			JSONNode catalogueJSON = JSONNode.Parse(catalogueData);
			Rootobject root = new Rootobject();
			
			JSONArray categories = catalogueJSON["categories"].AsArray;
			
			var catalogue = new Catalogue();
			//	Debug.Log(string.Concat("Categorias: ", categories, catalogueData));
			foreach (JSONNode category in categories)
			{
				var catCategory = ParseCategoryNode(category, 0);
				catalogue.Categories.Add(catCategory);
			}
			
			foreach (var cat in catalogue.Categories)
			{
				if (!string.IsNullOrEmpty(cat.parentId))
				{
					var parent = catalogue.Categories.FirstOrDefault(c=>c.id == cat.parentId);
					if (parent != null)
					{
						cat.level = parent.level+1;
						cat.parent = parent;
						parent.children.Add(cat);
					}
				}
			}
			
			JSONArray items = catalogueJSON["objects"].AsArray;
			foreach (JSONNode item in items)
			{
				//	Debug.Log(item.ToString());
				var catItem = ParseItem(item);
				catalogue.Items.Add(catItem);
			}
			return catalogue;
		}


		public static Catalogue Parse2(string catalogueData)
		{
			//Debug.Log("Catalogue length: " + catalogueData.Length.ToString());
			JSONNode catalogueJSON = JSONNode.Parse(catalogueData);
			Rootobject root = new Rootobject();
		
			JSONArray categories = catalogueJSON["categories"].AsArray;
			
			var catalogue = new Catalogue();
			//	Debug.Log(string.Concat("Categorias: ", categories, catalogueData));
			foreach (JSONNode category in categories)
			{
				var catCategory = ParseCategoryNode(category, 0);
				catalogue.Categories.Add(catCategory);
			}
			
			foreach (var cat in catalogue.Categories)
			{
				if (!string.IsNullOrEmpty(cat.parentId))
				{
					var parent = catalogue.Categories.FirstOrDefault(c=>c.id == cat.parentId);
					if (parent != null)
					{
						cat.level = parent.level+1;
						cat.parent = parent;
						parent.children.Add(cat);
					}
				}
			}
			
			JSONArray items = catalogueJSON["objects"].AsArray;
			foreach (JSONNode item in items)
			{
				//	Debug.Log(item.ToString());
				var catItem = ParseItem(item);
				catalogue.Items.Add(catItem);
			}
			return catalogue;
		}
		//Fin - public static Catalogue Parse(string catalogueData)
		
		
		//-----------////static CatalogueItem ParseItem(JSONNode item)
		static CatalogueItem ParseItem(JSONNode item)
		{
			CatalogueItem catItem = new CatalogueItem();
			catItem.category = item["category"];
			catItem.id = item["id"];
			catItem.name = item["name"];
			catItem.description = item["description"];
			
			
			
			
			catItem.type = CatalogueItemType.RoomObject;
			catItem.Featured = item[FeaturedPropertyName].AsBool;
			
			var type = "room_object";
			if (item["type"] != null)
			{
				switch (item["type"].Value)
				{
				case "room_object":
					catItem.type = CatalogueItemType.RoomObject;
					break;
				case "wall_painting":
					catItem.type = CatalogueItemType.WallPainting;
					break;
				case "floor_painting":
					catItem.type = CatalogueItemType.FloorPainting;
					break;
				default:
					//Debug.LogWarning("Type not found: " + item["type"].Value);
					break;
				}
			}
			
			if (catItem.type == CatalogueItemType.RoomObject)
			{
				ParseObject_RoomObject(item, catItem);
			}
			else if (catItem.type == CatalogueItemType.WallPainting)
			{
				ParseObject_WallPainting(item, catItem);
			}
			else if (catItem.type == CatalogueItemType.FloorPainting)
			{
				ParseObject_FloorPainting(item, catItem);
			}
			else
			{
				//Debug.LogWarning("Type not found: " + catItem.type);
				//	Debug.LogWarning(catItem.ToString());
			}
			return catItem;
		}
		
		
		//FIn- 
		//private static CatalogueWallTexture ParseCatalogueWallTextureNode(JSONNode nodeMaterial)
		//{
		//    CatalogueWallTexture catMaterial = new CatalogueWallTexture();
		//    if (nodeMaterial["name"] != null)
		//    {
		//        catMaterial.name = nodeMaterial["name"];
		//    }
		//    if (nodeMaterial["color"] != null)
		//    {
		//        catMaterial.color = GetColor(nodeMaterial["color"]);
		//    }
		//    if (nodeMaterial["texture"] != null)
		//    {
		//        catMaterial.texture = GetFullUrl(nodeMaterial["texture"]);
		//    }
		//    if (nodeMaterial["scaling"] != null)
		//    {
		//        catMaterial.defaultScaling = GetVector2(nodeMaterial["scaling"]);
		//    }
		//    if (nodeMaterial["offset"] != null)
		//    {
		//        catMaterial.defaultOffset = GetVector2(nodeMaterial["offset"]);
		//    }
		//    return catMaterial;
		//}
		
		
		//-----------//static CatalogueCategory ParseCategoryNode(JSONNode categoryNode, int level)
		static CatalogueCategory ParseCategoryNode(JSONNode categoryNode, int level)
		{
			CatalogueCategory catCategory = new CatalogueCategory();
			catCategory.id = categoryNode["id"];
			catCategory.name = categoryNode["name"];
			catCategory.parentId = categoryNode["parent"];
			catCategory.level = level;
			
			return catCategory;
		}
		
		
		//-----------//private static void ParseObject_FloorPainting(JSONNode item, CatalogueItem catItem)
		private static void ParseObject_FloorPainting(JSONNode item, CatalogueItem catItem)
		{
			CatalogueItemVariation variation = new CatalogueItemVariation();
			variation.name = catItem.name;
			variation.Item = catItem;
			//crear variable para tamaño de pisos
			variation.thumbnail = GetFullUrl(item["thumbnail"]);
			variation.Setmaterials.Add(new CatalogueMaterial()
			                           {
				texture = GetFullUrl(item["texture"])
			});
			
			catItem.variations.Add(variation);
		}
		
		//-----------//private static void ParseObject_WallPainting(JSONNode item, CatalogueItem catItem)
		private static void ParseObject_WallPainting(JSONNode item, CatalogueItem catItem)
		{
			CatalogueItemVariation variation = new CatalogueItemVariation();
			variation.name = catItem.name;
			variation.Item = catItem;
			variation.thumbnail = GetFullUrl(item["thumbnail"]);
			variation.Setmaterials.Add(new CatalogueMaterial()
			                           {
				texture = GetFullUrl(item["texture"]),
				textures = new List<string>() {GetFullUrl(item["texture"])}
			});
			
			catItem.variations.Add(variation);
		}
		
		
		//---------------------------------------------------------------
		//-----------//public static void ParseObject_RoomObject(JSONNode item, CatalogueItem catItem)
		public static void ParseObject_RoomObject(JSONNode item, CatalogueItem catItem)
		{
			
			//Model info
			//category
			//name
			//id
			catItem.model = GetFullUrl(item["model"]);
			catItem.modelUseMtl = item["modelUseMtl"].AsBool;
			catItem.scale = GetVector3(item["scale"], Vector3.one);
			catItem.offset = GetVector3(item["offset"], Vector3.zero);
			catItem.rotation = GetVector3(item["rotation"], Vector3.zero);
			catItem.deep = item["deep"];
			catItem.width = item ["width"];
			catItem.height = item ["height"];
			catItem.heightF = item ["largo"].AsFloat;//prueba pisos smb
			catItem.widthF = item ["ancho"].AsFloat;//prueba pisos smb
			catItem.fijadoA = item ["fijado_a"];//SMB: AttachedToWall: Asignacion de valores desde DB. Valores posibles: "pared" - "piso" - "techo" - Null.
			catItem.Altura = item ["en_altura"].AsInt;//SMB: AttachedToWall: Asignacion de valores desde DB. Altura del piso, dependiendo del objeto, seteado en DB.
			
			
			//Variation info
			
			CatalogueItemVariation catVariation = new CatalogueItemVariation();
			catVariation.name = catItem.name;
			catVariation.Item = catItem;
			
			catVariation.thumbnail = GetFullUrl(item["thumbnail"]);
			
			
			//Set Materiales
			foreach (JSONNode Setmaterials in item["materials"].AsArray)//Preset de colores
			{
				CatalogueMaterial catMaterial = new CatalogueMaterial();
				//----Dentro del set de materiales
				if(Setmaterials["roomObject_id"] !=null)//Set de materiales nro
				{
					catMaterial.roomObject_id = Setmaterials["roomObject_id"].AsInt;
				}
				if(Setmaterials["nombre"] !=null)//Nombre de set de material nro
				{
					catMaterial.nombreSets = Setmaterials["nombre"];
				}
				foreach (JSONNode materialAUX in Setmaterials["materiales"].AsArray)
				{
					CatalogueMaterialesSet catMaterialSet = new CatalogueMaterialesSet();
					if(materialAUX["material_set_id"] !=null)
					{
						catMaterialSet.material_set_id = materialAUX["material_set_id"];
					}
					if(materialAUX["material"] !=null)
					{
						catMaterialSet.materialSet = materialAUX["material"];
					}
					if(materialAUX["tipo"] !=null)
					{
						catMaterialSet.tipoSet = materialAUX["tipo"];
					}
					if (materialAUX["color"] != null)
					{
						if(materialAUX["color"] == ""){
							catMaterialSet.colorSet = null;
						}else{
							catMaterialSet.colorSet = GetColor(Setmaterials["color"]);
						}
					}
					if(materialAUX["texture"] !=null)
					{
						string texturestring = materialAUX["texture"].AsArray.ToString();
						texturestring = texturestring.Replace("[ ","");
						texturestring = texturestring.Replace(" ]","");
						texturestring = texturestring.Replace("\"","");
						catMaterialSet.textureSet = GetFullUrl(texturestring);
						
					}
					catMaterial.materialesInternos.Add(catMaterialSet);
					
				}
				catVariation.Setmaterials.Add(catMaterial);
			}
			
			JSONArray plugins = item["plugins"].AsArray;
			foreach (JSONNode plugin in plugins)
			{
				var builder = Plugins.PluginResolver.GetBuilder(plugin["name"]);
				if (builder != null)
				{
					catVariation.plugins.Add(builder.GetPluginMetadata(plugin));
				}
			}
			catItem.variations.Add(catVariation);
			
			catItem.modelObject = null;
		}
		
		//---------------------------------------------------------------
		//---------------------------------------------------------------
		//---------------------------------------------------------------
		
		//-----------//static CatalogueItemVariation ParseItemVariationNode(JSONNode variation, CatalogueItem item)
		static CatalogueItemVariation ParseItemVariationNode(JSONNode variation, CatalogueItem item)
		{
			CatalogueItemVariation catVariation = new CatalogueItemVariation();
			catVariation.Item = item;
			catVariation.name = variation["name"];
			catVariation.thumbnail = GetFullUrl(variation["thumbnail"]);
			
			foreach (JSONNode nodeMaterial in variation["materials"].AsArray)//sets de materiales
			{
				
				CatalogueMaterial catMaterial = new CatalogueMaterial();
				//----aca van los sets(?)
				if(nodeMaterial["roomObject_id"] !=null)
				{
					catMaterial.roomObject_id = nodeMaterial["roomObject_id"].AsInt;
				}
				if(nodeMaterial["nombre"] !=null)
				{
					catMaterial.nombreSets = nodeMaterial["nombre"];
				}
				foreach (JSONNode materialAUX in nodeMaterial["materiales"].AsArray)
				{
					CatalogueMaterialesSet catMaterialSet = new CatalogueMaterialesSet();
					if(materialAUX["material_set_id"] !=null)
					{
						catMaterialSet.material_set_id = GetFullUrl(materialAUX["material_set_id"]);
					}
					if(materialAUX["material"] !=null)
					{
						catMaterialSet.materialSet = materialAUX["material"];
					}
					if(materialAUX["tipo"] !=null)
					{
						catMaterialSet.tipoSet = materialAUX["tipo"];
					}
					if (materialAUX["color"] != null)
					{
						catMaterialSet.colorSet = GetColor(materialAUX["color"]);
					}
					if(materialAUX["texture"] !=null)
					{
						string texturestring = materialAUX["texture"].AsArray.ToString();
						texturestring = texturestring.Replace("[ ","");
						texturestring = texturestring.Replace(" ]","");
						texturestring = texturestring.Replace("\"","");
						catMaterialSet.textureSet = GetFullUrl(texturestring);
					}
					catMaterial.materialesInternos.Add(catMaterialSet);
				}
				catVariation.Setmaterials.Add(catMaterial);
			}
			
			
			JSONArray plugins = variation["plugins"].AsArray;
			foreach (JSONNode plugin in plugins)
			{
				var builder = Plugins.PluginResolver.GetBuilder(plugin["name"]);
				if (builder != null)
				{
					catVariation.plugins.Add(builder.GetPluginMetadata(plugin["metadata"]));
				}
			}
			return catVariation;
		}
		
		//-----------//private static string GetFullUrl(string value)
		private static string GetFullUrl(string value)
		{
			if (string.IsNullOrEmpty(value)) return value;
			string finalUrl = value.IndexOf("http") > -1 ? value : Configuration.Endpoint + value;
			
			return finalUrl;
		}
		
		//-----------//public static Vector3 GetVector3(JSONNode node, Vector3 defaultVector)
		public static Vector3 GetVector3(JSONNode node, Vector3 defaultVector)
		{
			string[] terms = node.Value
				.Replace("[","")
					.Replace("]","")
					.Split(',');
			if (terms.Length == 3)
			{
				return new Vector3(float.Parse(terms[0]), float.Parse(terms[1]), float.Parse(terms[2]));
			}
			else
			{
				return defaultVector;
			}
		}
		
		//-----------//private static Vector2 GetVector2(JSONNode node)
		private static Vector2 GetVector2(JSONNode node)
		{
			JSONArray array = node.AsArray;
			return new Vector2(array[0].AsFloat, array[1].AsFloat);
		}
		
		//-----------//static List<type> GetList<type>(JSONNode node) where type : class
		static List<type> GetList<type>(JSONNode node) where type : class
		{
			List<type> list = new List<type>();
			foreach (JSONNode child in node.AsArray)
			{
				list.Add(child as type);
			}
			
			return list;
		}
		
		//-----------//public static Color GetColor(JSONNode node)
		public static Color GetColor(JSONNode node)
		{
			Vector3 colorVector = GetVector3(node, new Vector3(0, 0, 0));
			float alpha = 1f;
			if (node.Count > 3)
			{
				alpha = node.AsArray[3].AsFloat;
			}
			Color color = new Color(colorVector.x / 255, colorVector.y / 255, colorVector.z / 255, alpha);
			return color;
		}
		
		//-----------//public class Rootobject
		public class Rootobject
		{
			public JsonCategory[] categories { get; set; }
			public JsonCatalogueObject[] objects { get; set; }
		}
		//-----------//public class JsonCategory
		public class JsonCategory
		{
			public string id { get; set; }
			public string name { get; set; }
			public string parent { get; set; }
		}
		
		//-----------//public class JsonCatalogueObject
		public class JsonCatalogueObject
		{
			public string category { get; set; }
			public string id { get; set; }
			public string name { get; set; }
			public string description { get; set; }
			public string type { get; set; }
			public string texture { get; set; }
			public string thumbnail { get; set; }
			public string height { get; set; }
			public string width { get; set; }
			public string deep { get; set; }
			public string model { get; set; }
			public bool modelUseMtl { get; set; }
			public float[] scale { get; set; }
			public int[] rotation { get; set; }
			public JsonMaterial[] materials { get; set; }//**************
			public JsonPlugin[] plugins { get; set; }
		}
		
		//-----------//public class JsonMaterial
		public class JsonMaterial
		{
			/*	public string type { get; set; }
			public string[] textures { get; set; }
			*/
			
			
			
			public string roomObject_id{ get; set; }
			public string nombreSets{ get; set; }
			public JsonMaterial2[] materiales{ get; set; }
			
		}
		
		
		//----Clase para Materilaes nuevos
		
		public class JsonMaterial2
		{
			public string material_set_id{ get; set; }
			public string material{ get; set; }
			public string tipoSet{ get; set; }
			public string colorSet{ get; set; }
			public Texture[] textures{ get; set; }
			
		}
		//-----------//public class JsonPlugin
		public class JsonPlugin
		{
			public string name { get; set; }
		}
		
	}
}
