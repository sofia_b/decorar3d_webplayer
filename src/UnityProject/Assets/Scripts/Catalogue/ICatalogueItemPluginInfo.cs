﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Catalogue
{
    public interface ICatalogueItemPluginInfo
    {
        string PluginName { get; }
    }
}
