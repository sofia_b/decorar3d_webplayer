﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Catalogue
{
    [Serializable]
    public class CatalogueFloorTexture : CatalogueMaterial
    {
        public string texture;
        public string name;
        public Vector2 defaultScaling = new Vector2(1, 1);
        public Vector2 defaultOffset = new Vector2(0, 0);
        public string materialName;
    }
}