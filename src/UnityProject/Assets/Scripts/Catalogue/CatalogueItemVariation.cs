﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Catalogue
{
	//[Serializable]
    public class CatalogueItemVariation {
        public CatalogueItemVariation() {
            UID = Guid.NewGuid();
        }
        public CatalogueItemVariation(Guid uid) {
            UID = uid;
        }
        public string name;

       public List<CatalogueMaterial> Setmaterials = new List<CatalogueMaterial>();//Son los sets de materiales
        public string thumbnail;

        public GameObject MenuItem { get; set; }
        public List<ICatalogueItemPluginInfo> plugins = new List<ICatalogueItemPluginInfo>();

        public Guid UID;

        public CatalogueItem Item { get; set; }
    }
}
