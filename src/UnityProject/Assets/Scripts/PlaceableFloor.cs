﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Crea pisos y paredes, pero no interaccion con los modelos/objetos/muebles
public class PlaceableFloor : MonoBehaviour
{
    public LayerMask CollidingMask;
    public List<System.Guid> Owners = new List<System.Guid>();
    public System.Guid ID { get; private set; }

    public Dictionary<Sides, PlaceableFloor> Neighbours = new Dictionary<Sides, PlaceableFloor>() {
            {Sides.North, null},
            {Sides.South, null},
            {Sides.West, null},
            {Sides.East, null},
        };
    public Dictionary<Sides, Vector3> Bounds = new Dictionary<Sides, Vector3>();
    public Dictionary<Sides, GameObject> Walls = new Dictionary<Sides, GameObject>()
        {
            {Sides.North, null},
            {Sides.South, null},
            {Sides.West, null},
            {Sides.East, null},
        };


    public PlaceableFloor()
    {
        ID = System.Guid.NewGuid();

    }
    void Awake()
    {
        Register();
    }


    private void Register()
    {
        if (BuildingManager != null)
        {
            BuildingManager.Register(ID, this);
        }
    }
    protected BuildingManager BuildingManager
    {
        get;
        private set;
    }
    public static Sides InverseSide(Sides side)
    {
        switch (side)
        {
            case Sides.North:
                return Sides.South;
            case Sides.South:
                return Sides.North;
            case Sides.West:
                return Sides.East;
            case Sides.East:
                return Sides.West;
            default:
                return side;
        }
    }
    // Use this for initialization
    void Start()
    {
        Register();
        if (!Neighbours.ContainsKey(Sides.North))
            Neighbours.Add(Sides.North, null);
        if (!Neighbours.ContainsKey(Sides.South))
            Neighbours.Add(Sides.South, null);
        if (!Neighbours.ContainsKey(Sides.West))
            Neighbours.Add(Sides.West, null);
        if (!Neighbours.ContainsKey(Sides.East))
            Neighbours.Add(Sides.East, null);


        if (!Walls.ContainsKey(Sides.North))
            Walls.Add(Sides.North, null);
        if (!Walls.ContainsKey(Sides.South))
            Walls.Add(Sides.South, null);
        if (!Walls.ContainsKey(Sides.West))
            Walls.Add(Sides.West, null);
        if (!Walls.ContainsKey(Sides.East))
            Walls.Add(Sides.East, null);

        UpdateNeighbours();


    }

    // Update is called once per frame
    Vector3 lastScale = default(Vector3);
    Vector3 lastPosition = default(Vector3);
    void Update()
    {
        SetBounds();
        bool scaleChanged = lastScale != transform.localScale;
        bool positionChanged = lastPosition != transform.localPosition;
       
		if (scaleChanged | positionChanged)
        {
            UpdateWalls(false);

            lastScale = transform.localScale;
            lastPosition = transform.localPosition;
        }


    }

    private void SetBounds()
    {
        if (Bounds.Count == 0)
        {
            Bounds.Add(Sides.North, GetOriginFromSide(Sides.North));
            Bounds.Add(Sides.South, GetOriginFromSide(Sides.South));
            Bounds.Add(Sides.West, GetOriginFromSide(Sides.West));
            Bounds.Add(Sides.East, GetOriginFromSide(Sides.East));

        }
    }


    internal void RemoveSelf()
    {
        RemoveWallFromSide(Sides.North);
        RemoveWallFromSide(Sides.South);
        RemoveWallFromSide(Sides.East);
        RemoveWallFromSide(Sides.West);
        UpdateNeighbours(boundSet: false);
    }
    public void UpdateNeighbours(bool notify = true, bool boundSet = true)
    {
        CheckSideNeighbour(Sides.North, notify, boundSet);
        CheckSideNeighbour(Sides.South, notify, boundSet);
        CheckSideNeighbour(Sides.East, notify, boundSet);
        CheckSideNeighbour(Sides.West, notify, boundSet);
    }

    private void CheckSideNeighbour(Sides side, bool notify, bool boundSet)
    {
        Vector3 origin = new Vector3();
        Vector3 direction = GetDirectionForSide(side, out origin);
        Vector3 targetPosition = transform.position + direction;
        RaycastHit hitInfo;
        Debug.DrawLine(transform.position, transform.position + (origin), Color.yellow, 5);
        float distance = Vector3.Distance(transform.position, targetPosition);
		//Debug.Log("RayCast - PlaceableFloor (7)");
		if (!Physics.Raycast(transform.position, direction, out hitInfo, distance, CollidingMask))
        {
            Neighbours[side] = null;
            SetBounds();
        }
        else
        {
            UpdateHasNeighbour(side, hitInfo.collider.GetComponent<PlaceableFloor>());
            if (notify)
            {
                hitInfo.collider.GetComponent<PlaceableFloor>().UpdateBoundNeighbour(InverseSide(side), boundSet ? this.gameObject : null);
            }
        }
    }

    private void UpdateBoundNeighbour(Sides side, GameObject colliding)
    {
        bool hasNeighbour = colliding != null;
        UpdateHasNeighbour(side, hasNeighbour ? colliding.GetComponent<PlaceableFloor>() : null);
    }

    private void UpdateHasNeighbour(Sides side, PlaceableFloor neighbour)
    {
        Neighbours[side] = neighbour;
        if (neighbour != null)
            RemoveWallFromSide(side);
		//Debug.Log("Se borran paredes innecesarias_");//SMB
    }

    internal PlaceableFloor GetNeighbour(Sides side)
    {
        return Neighbours[side];
    }

    public bool CreateNeighbour(Sides side)
    {
        Vector3 origin = new Vector3();
        Vector3 direction = GetDirectionForSide(side, out origin);
        Vector3 targetPosition = transform.position + direction;
        RaycastHit hitInfo;
       // Debug.DrawLine(transform.position, transform.position + (origin), Color.yellow, 5);
        float distance = Vector3.Distance(transform.position, targetPosition);
		//Debug.Log("RayCast - PlaceableFloor (8)");
		if (!Physics.Raycast(transform.position, direction, out hitInfo, distance, CollidingMask))
        {
            BuildingManager buildingManager = GetBuildingManager();
            RemoveWallFromSide(side);
            if (buildingManager != null)
            {
                buildingManager.CreateFloor(targetPosition);
            }
            UpdateNeighbours();

            UpdateMeasurer();

            return true;
        }
        else
        {
            Debug.DrawLine(transform.position, hitInfo.point, Color.red, 3);
            Debug.DrawLine(transform.position, targetPosition, Color.green, 3);
            Debug.DrawLine(hitInfo.point, hitInfo.point + new Vector3(0, 10, 0), Color.blue, 2);
            return false;
        }
    }

    private void UpdateMeasurer()
    {
        DimBoxes_DimBox box = transform.parent.GetComponent<DimBoxes_DimBox>();
        if (box != null)
        {
            box.DoStartup(true);
        }
    }

    private BuildingManager GetBuildingManager()
    {

        return BuildingManager ?? GameObject.FindGameObjectWithTag("SceneManager").GetComponent<BuildingManager>(); ;
    }

    public void RemoveWallFromSide(Sides side)
    {
        if (Walls.ContainsKey(side))
        {
            GameObject wall = Walls[side];
            if (wall != null)
                GameObject.Destroy(wall);
            Walls[side] = null;
        }
    }

    private Vector3 GetDirectionForSide(Sides side, out Vector3 origin)
    {
        Vector3 neworigin = GetOriginFromSide(side);
        Vector3 direction = (neworigin * transform.lossyScale.x);
        origin = neworigin;
        return direction;
    }

    private Vector3 GetOriginFromSide(Sides side)
    {
        Vector3 origin;
        origin = new Vector3();
        switch (side)
        {
            case Sides.North:
                origin = transform.forward;
                break;
            case Sides.South:
                origin = transform.forward * -1;
                break;
            case Sides.West:
                origin = transform.right * -1;
                break;
            case Sides.East:
                origin = transform.right;
                break;
            default:
                break;
        }
        return origin;
    }

    internal void AddWallToSide(Sides side, GameObject wall)
    {
        Walls[side] = wall;
    }

    internal void UpdateWalls(bool create)
    {
        if (create || Walls[Sides.North] != null)
            UpdateWall(Sides.North);
        if (create || Walls[Sides.South] != null)
            UpdateWall(Sides.South);
        if (create || Walls[Sides.East] != null)
            UpdateWall(Sides.East);
        if (create || Walls[Sides.West] != null)
            UpdateWall(Sides.West);
    }

    private void UpdateWall(Sides side)
    {
        if (!Neighbours[side])
        {
            if (Walls[side] == null)
            {
                var buildingManager = GetBuildingManager();
                if (buildingManager != null)
                    buildingManager.CreateWall(this, side);
            }
            else
            {
                Walls[side].GetComponent<Wall>().AlignToFloor(this);
                Walls[side].transform.position = CalculateWallPosition(side);
            }
        }
        else
        {
            RemoveWallFromSide(side);
        }
    }

    void OnDisable()
    {
        foreach (var wall in Walls)
        {
            if (wall.Value != null) wall.Value.gameObject.SetActive(false);
        }
    }

    void OnEnable()
    {
        foreach (var wall in Walls)
        {
            if (wall.Value != null) wall.Value.gameObject.SetActive(true);
        }
    }



    internal Vector3 CalculateWallPosition(Sides side)
    {
        SetBounds();
        Vector3 bound = Bounds[side];
        Vector3 scale = new Vector3(transform.lossyScale.x / 2 * bound.x, 0 * bound.y / 2, transform.lossyScale.z / 2 * bound.z);
        Vector3 targetPosition = transform.position + scale;
        //Debug.Log(string.Format("{0}: {1} - {2} = {3}", side, bound, transform.lossyScale, targetPosition));
        Debug.DrawLine(targetPosition, targetPosition + new Vector3(0, 10, 0), Color.magenta, 4);
        return targetPosition;
    }
}