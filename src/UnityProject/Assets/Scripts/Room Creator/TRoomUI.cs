﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class TRoomUI : RoomUI<TRoom> {
    public UnityEngine.UI.InputField TopHallInput;
    public UnityEngine.UI.InputField DownHallInput;
    public UnityEngine.UI.InputField WallHeightInput;
    public UnityEngine.UI.InputField LeftCorridorInput;
    public UnityEngine.UI.InputField RightCorridorInput;
    public UnityEngine.UI.InputField DownCorridorInput;
    // Use this for initialization
    protected override void Start() {
        TopHallInput.onValidateInput += ValidateInput;
        DownHallInput.onValidateInput += ValidateInput;
        WallHeightInput.onValidateInput += ValidateInput;
        LeftCorridorInput.onValidateInput += ValidateInput;
        RightCorridorInput.onValidateInput += ValidateInput;
        DownCorridorInput.onValidateInput += ValidateInput;


        TopHallInput.onValueChange.AddListener((value) => UpdateTopHallInput(value));
        DownHallInput.onValueChange.AddListener((value) => UpdateDownHallInput(value));
        WallHeightInput.onValueChange.AddListener((value) => UpdateWallHeight(value));
        LeftCorridorInput.onValueChange.AddListener((value) => UpdateTopHallInput(value));
        RightCorridorInput.onValueChange.AddListener((value) => UpdateDownHallInput(value));
        DownCorridorInput.onValueChange.AddListener((value) => UpdateWallHeight(value));


        TopHallInput.text = ((int)RoomObject.TopHallLength * 100).ToString();
        DownHallInput.text = ((int)RoomObject.DownHallLength * 100).ToString();
        WallHeightInput.text = ((int)RoomObject.WallHeight * 100).ToString();
        RightCorridorInput.text = ((int)RoomObject.RightLength * 100).ToString();
        LeftCorridorInput.text = ((int)RoomObject.LeftLength * 100).ToString();
        DownCorridorInput.text = ((int)RoomObject.DownLength * 100).ToString();
    }

    protected char ValidateInput(string text, int charIndex, char addedChar) {
        return (char.IsDigit(addedChar) || addedChar == '.') ? addedChar : '\0';
    }

    private void UpdateWallHeight(string value) {
        int length = (int)(RoomObject.WallHeight * 100);
        Int32.TryParse(value, out length);
        RoomObject.WallHeight = (float)length / 100;
    }

    private void UpdateTopHallInput(string value) {
        int length = (int)(RoomObject.TopHallLength * 100);
        Int32.TryParse(value, out length);

        RoomObject.TopHallLength = (float)length / 100F;
    }
    private void UpdateDownHallInput(string value) {
        int length = (int)(RoomObject.DownHallLength * 100);
        Int32.TryParse(value, out length);
        RoomObject.DownHallLength = (float)length / 100F;
    }
    private void UpdateLeftCorridorInput(string value) {
        int length = (int)(RoomObject.LeftLength * 100);
        Int32.TryParse(value, out length);

        RoomObject.LeftLength = (float)length / 100F;
    }
    private void UpdateRightCorridorInput(string value) {
        int length = (int)(RoomObject.RightLength * 100);
        Int32.TryParse(value, out length);

        RoomObject.RightLength = (float)length / 100F;
    }
    private void UpdateDownCorridorInput(string value) {
        int length = (int)(RoomObject.DownLength * 100);
        Int32.TryParse(value, out length);
        RoomObject.DownLength = (float)length / 100F;
    }
	public void ResetInputs()
	{
	
		TopHallInput.text = (400).ToString();
		DownHallInput.text= (400).ToString();
		WallHeightInput.text = (300).ToString();
		LeftCorridorInput.text = (200).ToString();
		RightCorridorInput.text = (200).ToString();
		DownCorridorInput.text = (300).ToString();
		
	}
}

