﻿using UnityEngine;
using System.Collections;

public class BoxRoom : RoomBehavior
{
    public float MinLength = 0.0001f;
	//public float MaxLength = 1f;

    public float WallHeight = 2f;
    public float TopHallLength = 1f;
    public float DownHallLength = 1f;
    public bool PutWallsOnStart = true;
    public PlaceableFloor CenterObject;

    // Use this for initialization
    protected virtual void Start()
    {
        if (PutWallsOnStart)
        {
            CenterObject.UpdateWalls(true);


        }
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        UpdateInputLengths();

        UpdateScale();

        UpdatePositions();
    }

    protected virtual void UpdatePositions()
    {
    }

    protected virtual void UpdateScale()
    {
        UpdateCenterScale();
    }

    protected virtual void UpdateInputLengths()
    {


						TopHallLength = Mathf.Max (1, TopHallLength);
						DownHallLength = Mathf.Max (1, DownHallLength);
						WallHeight = Mathf.Max (1, WallHeight);
				
			
		

    }



    protected virtual void UpdateCenterScale()
    {
        var oldScale = CenterObject.transform.localScale;
        CenterObject.transform.localScale = new Vector3(DownHallLength, CenterObject.transform.localScale.y, TopHallLength);
        UpdateWallHeights();
        if (oldScale != CenterObject.transform.localScale)
            UpdateMeasures(CenterObject);
    }

    protected virtual void UpdateWallHeights()
    {
        SetWallHeight(CenterObject);
    }

    protected virtual void SetWallHeight(PlaceableFloor floor)
    {
        foreach (var side in floor.Walls.Keys)
        {
            if (floor.Walls[side] != null)
            {
                var wall = floor.Walls[side].transform;
                wall.localScale = new Vector3(wall.localScale.x, WallHeight, wall.localScale.z);
            }
        }
    }

    protected virtual void UpdateMeasures(PlaceableFloor obj)
    {
        var measurer = obj.GetComponent<DimBoxes_DimBox>();
        if (measurer != null && measurer.enabled)
        {
            measurer.DoStartup(true);
        }
    }

    protected virtual Vector3 GetRelativePosition(Transform transform, float xMultiplier, float zMultiplier)
    {
        float targetX = (CenterObject.transform.position.x + CenterObject.transform.localScale.x / 2) + transform.localScale.x / 2;
        float targetZ = (CenterObject.transform.position.z + CenterObject.transform.localScale.z / 2) + transform.localScale.z / 2;

        return new Vector3(targetX * xMultiplier, transform.position.y, targetZ * zMultiplier);
    }

}
