﻿using UnityEngine;
using System.Collections;
using System;

public class BoxRoomUI : RoomUI<BoxRoom>
{
    public UnityEngine.UI.InputField TopHallInput;
    public UnityEngine.UI.InputField DownHallInput;
    public UnityEngine.UI.InputField WallHeightInput;
    // Use this for initialization
    protected override void Start()
    {
        TopHallInput.onValidateInput += ValidateInput;
        DownHallInput.onValidateInput += ValidateInput;
        WallHeightInput.onValidateInput += ValidateInput;

        TopHallInput.onValueChange.AddListener((value) => UpdateTopHallInput(value));
        DownHallInput.onValueChange.AddListener((value) => UpdateDownHallInput(value));
        WallHeightInput.onValueChange.AddListener((value) => UpdateWallHeight(value));

        TopHallInput.text = ((int)RoomObject.TopHallLength * 100).ToString();
        DownHallInput.text = ((int)RoomObject.DownHallLength * 100).ToString();
        WallHeightInput.text = ((int)RoomObject.WallHeight * 100).ToString();
    }

    private void UpdateWallHeight(string value)
    {
        int length = (int)(RoomObject.WallHeight * 100);
        Int32.TryParse(value, out length);
        RoomObject.WallHeight = (float)length / 100;
    }

    private void UpdateTopHallInput(string value)
    {
        int length = (int)(RoomObject.TopHallLength * 100);
        Int32.TryParse(value, out length);

        RoomObject.TopHallLength = (float)length / 100F;
    }
    private void UpdateDownHallInput(string value)
    {
        int length = (int)(RoomObject.DownHallLength * 100);
        Int32.TryParse(value, out length);
        RoomObject.DownHallLength = (float)length / 100F;
    }

    protected char ValidateInput(string text, int charIndex, char addedChar)
    {
        return (char.IsDigit(addedChar) || addedChar=='.') ? addedChar : '\0';
    }

    // Update is called once per frame
   public void ResetInputs()
    {
		TopHallInput.text = (400).ToString();
		DownHallInput.text= (400).ToString();
		WallHeightInput.text = (300).ToString();

    }
}
