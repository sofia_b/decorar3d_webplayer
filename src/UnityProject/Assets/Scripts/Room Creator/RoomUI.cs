﻿using UnityEngine;
using System.Collections;
using System;

public abstract class RoomUI<T> : RoomUIBehavior where T : RoomBehavior
{
    public T RoomObject;
    protected abstract void Start();

    void OnDisable()
    {
        if (IsDecorating)
        {
            RoomObject.gameObject.SetActive(false);
        }
    }

    void OnEnable()
    {
        if (IsDecorating)
        {
            RoomObject.gameObject.SetActive(true);
        }
    }



    public override string GetRoomName()
    {
        return RoomObject.name;
    }
}
