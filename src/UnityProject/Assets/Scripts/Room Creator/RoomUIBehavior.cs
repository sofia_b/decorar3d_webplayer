﻿using UnityEngine;
using System.Collections;
using System;

public abstract class RoomUIBehavior : MonoBehaviour
{
    public bool IsDecorating = true;

    public abstract string GetRoomName();
}
