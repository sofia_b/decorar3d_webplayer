﻿using UnityEngine;
using System.Collections;

public class LRoom : BoxRoom {

    public float RightLength = 1f;
    public float DownLength = 1;

    public PlaceableFloor DownObject;
    public PlaceableFloor RightObject;

    protected override void Start()
    {
        base.Start();
        if (PutWallsOnStart)
        {
            DownObject.UpdateWalls(true);
            RightObject.UpdateWalls(true);
        }
    }
    protected override void UpdateWallHeights()
    {
        base.UpdateWallHeights();
        SetWallHeight(RightObject);
        SetWallHeight(DownObject);
    }
    protected override void UpdatePositions()
    {
        base.UpdatePositions();
        RightObject.transform.position = GetRelativePosition(RightObject.transform, 1, 0);
        DownObject.transform.position = GetRelativePosition(DownObject.transform, 0, -1);
    }

    protected override void UpdateScale()
    {
        base.UpdateScale();
        UpdateRightScale();
        UpdateDownScale();
    }

    protected override void UpdateInputLengths()
    {
        base.UpdateInputLengths();
        RightLength = Mathf.Max(MinLength, RightLength);
        DownLength = Mathf.Max(MinLength, DownLength);
    }

    protected virtual void UpdateDownScale()
    {
        var oldScale = DownObject.transform.localScale;
        DownObject.transform.localScale = new Vector3(DownHallLength, DownObject.transform.localScale.y, DownLength);
        if (oldScale != DownObject.transform.localScale)
        {
            UpdateMeasures(DownObject);
        }
    }

    protected virtual void UpdateRightScale()
    {
        var oldScale = RightObject.transform.localScale;
        RightObject.transform.localScale = new Vector3(RightLength, RightObject.transform.localScale.y, TopHallLength);
        if (oldScale != RightObject.transform.localScale)
            UpdateMeasures(RightObject);
    }

}
