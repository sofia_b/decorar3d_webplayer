﻿using UnityEngine;
using System.Collections;

public class TRoom : LRoom {

    public float LeftLength = 1f;

    public PlaceableFloor LeftObject;
    protected override void Start()
    {
        base.Start();
        if (PutWallsOnStart)
        {
            LeftObject.UpdateWalls(true);
        }
    }
    protected override void UpdateWallHeights()
    {
        base.UpdateWallHeights();
        SetWallHeight(LeftObject);
    }
    protected override void UpdatePositions()
    {
        base.UpdatePositions();
        LeftObject.transform.position = GetRelativePosition(LeftObject.transform, -1, 0);
    }
    protected override void UpdateScale()
    {
        base.UpdateScale();

        UpdateLeftScale();
    }
    protected virtual void UpdateLeftScale()
    {
        var oldScale = LeftObject.transform.localScale;
        LeftObject.transform.localScale = new Vector3(LeftLength, LeftObject.transform.localScale.y, TopHallLength);
        if (oldScale != LeftObject.transform.localScale)
            UpdateMeasures(LeftObject);
    }
}
