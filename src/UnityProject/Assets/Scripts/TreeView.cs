using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;

public class TreeView : MonoBehaviour
{
		public GameObject menuItem = null;
		public GameObject labelFont = null;
		public bool requiresReposition;
	public Material ItemMaterial;
		
		protected SceneManager SceneManager;
        protected bool TreeBuilt;
		UITable table;
		UIScrollView parentScroll;
		// Start is called just before any of the
		// Update methods is called the first time.
		void Start ()
		{
		}
	    void OnEnable()
        {
            if (!TreeBuilt && this.SceneManager.Catalogue != null)
            {
                BuildTree();
            }
	    }
	
	
		void Awake ()
		{
            this.SceneManager = this.SceneManager ?? GameObject.FindWithTag("SceneManager").GetComponent<SceneManager>();
                this.SceneManager.CatalogueUpdated += BuildTree;
				parentScroll = this.transform.parent.GetComponent<UIScrollView> ();
				
				table = GetComponent<UITable> ();
				table.onReposition = TableRepositioned;
		}
		void TableRepositioned ()
		{
				parentScroll.ResetPosition ();
		}
	
		// Update is called every frame, if the
		// MonoBehaviour is enabled.
		void Update ()
		{
				if (requiresReposition) {
						table.repositionNow = true;
				}
		}
		int itemsCount;
		void BuildTree ()
		{
				foreach (CatalogueCategory category in SceneManager.Catalogue.Categories) {
						TreeItem parent = CreateCategoryItem (category);
						
						foreach (CatalogueItem item in SceneManager.Catalogue.Items.Where(i=>i.category==category.id)) {
								TreeItem childItem = CreateItem (item, parent);
								parent.addChild (childItem);
						}
				}
                TreeBuilt = true;
		}
	
		TreeItem CreateCategoryItem (CatalogueCategory category)
		{
				GameObject obj = GameObject.Instantiate (menuItem) as GameObject;
				itemsCount++;
				obj.transform.parent = transform;
				obj.transform.localScale = new Vector3 (1, 1, 1);
				obj.transform.localPosition = new Vector3 (0, itemsCount, 0);
				TreeItem item = obj.GetComponent<TreeItem> ();
				item.treeview = this;
				item.title = category.name;
				item.innerObject = category;
				item.spriteName = null;
				item.expanded = true;
				item.indentation = 0;
				item.updateIndentation ();
				return item;
		}
	
		TreeItem CreateItem (CatalogueItem catalogueItem, TreeItem parent)
		{
				GameObject obj = GameObject.Instantiate (menuItem) as GameObject;
				itemsCount++;
				obj.transform.parent = transform;
				obj.transform.localScale = new Vector3 (1, 1, 1);
				obj.transform.localPosition = new Vector3 (0, itemsCount, 0);
				TreeItem item = obj.GetComponent<TreeItem> ();
				item.AllowDrag = true;
				item.treeview = this;
				item.title = catalogueItem.name;
				item.innerObject = catalogueItem;
				item.itemMaterial = ItemMaterial;
				item.spriteName = null;
				item.expanded = true;
				if (parent != null) {
						item.parent = parent;
						item.indentation = parent.indentation + 1;
				} else {
						item.indentation = 0;
				}
				item.updateIndentation ();
				return item;
		}
}
