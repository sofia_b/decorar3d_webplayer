using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;

public class CategoryButton : MonoBehaviour
{
	
	public CatalogueCategory Category;
	public bool IsLoaded = false;
	private bool IsActive = false;
	// Update is called every frame, if the
	// MonoBehaviour is enabled.
	void Update()
	{
		
	}
	
	public void Load()
	{
	}
	
	public void Clicked()
	{
		CatalogueView.Instance.LoadCategory(Category);
	}
	
	public void Activate() {
	}
	
	public void LoadData(GridText.IGridTextInfo info) {
		
		Category = info.Data as Assets.Scripts.Catalogue.CatalogueCategory;
	}
	
}
