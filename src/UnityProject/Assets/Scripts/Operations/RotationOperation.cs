﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

using System.Collections;
using UnityEngine.EventSystems;
using Assets.Scripts.Catalogue;



public class RotationOperation : IOperation<Quaternion>
{
	public virtual bool IsValid { get; set; }
	
	protected SceneObject Owner;
	protected Quaternion InitialRotation;
	protected Quaternion lastValidRotation;
	public RotationOperation(SceneObject owner)
	{
		Owner = owner;
	}
	public void Start()
	{
	
	this.InitialRotation = Owner.transform.rotation;
	this.lastValidRotation = this.InitialRotation;
	this.IsActive = true;
	
				
	}
	
	public void Cancel() {
		//Debug.Log("Canceling rotation" + this.IsActive.ToString());
		if (this.IsActive)
		{
			Owner.transform.rotation = this.InitialRotation;
			Owner.IsSafe();
			this.IsActive = false;
			RefreshOwner();
		}
	}
	
	public void End()
	{
		//Debug.Log("Ending rotation");
		Owner.IsSafe();
		this.IsActive = false;
		RefreshOwner();
	}
	public void Finish()
	{
		if (Validate())
		{
			End();
		}
		else
		{
			Cancel();
		}
	}
	
	public Vector3 resvaltransf;
	public bool Validate()
	{
		resvaltransf = Owner.ValidateTransform ();
		if (resvaltransf == new Vector3(0,-100,0)) {
			IsValid = true;
		} else {
			IsValid = false;
			Owner.transform.position = resvaltransf;
		}
		
		//IsValid = Owner.ValidateTransform();
		if (IsValid)
		{
			SetCurrentAsValid();
		}
		return IsValid;
	}
	
	public bool IsActive
	{
		get;
		protected set;
	}
	
	public void FixedUpdate()
	{
		if (Owner.transform.rotation != lastValidRotation && Owner.IsNotColliding)
		{
			//Debug.Log(string.Format("Setting rotation from: {0} to valid {1}", lastValidRotation, Owner.transform.rotation));
			Owner.transform.rotation = lastValidRotation;
			SetCurrentAsValid();
			RefreshOwner();
		}
	}
	
	private void RefreshOwner()
	{
		Owner.UpdateBounds();
	}
	
	
	
	
	public void SetCurrentAsValid() {
		//Debug.Log(string.Format("Setting current rotation as valid: {0} to {1}", lastValidRotation, Owner.transform.rotation));
		lastValidRotation = Owner.transform.rotation;
		Owner.IsSafe();
	}
	
	public void StartAndSet(Quaternion unit)
	{ 	if (!EventSystem.current.IsPointerOverGameObject ()) {
			//Debug.Log(string.Format("Setting rotation from: {0} to {1}" , Owner.transform.rotation, unit));
			Owner.transform.rotation = unit;
			//Debug.Log(string.Format("Rotation set to {0}", Owner.transform.rotation, unit));
			RefreshOwner ();
			//Debug.Log("Starting rotation");
				}
	}
}