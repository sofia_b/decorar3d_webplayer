﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TranslationOperation : IOperation<Vector3>
{
	protected SceneObject Owner;
	protected Vector3 InitialPosition;//(0,0,10)
	protected Vector3 lastValidPosition;
	protected int RealLayer;
	private Guid UID;
	
	
	
	public Vector3 InitPositionToMove;//SMB: Posicion donde el Ray apunta al piso a modo inicial para poder setear el Diff de Vector3. ****
	public Vector3 PositionToMove;//SMB: Posicion que debera desplazarce el objeto. ****
	public bool IsFristPositionMouse = true;//SMB: Flag para poder obtener solo una posicion inicial del mouse proyectado por el Ray. ****
	public bool IsOnStarted = true;//SMB: Flag para ver si es cuando apenaz se crea el objeto. ****
	
	
	
	public TranslationOperation(SceneObject owner)
	{
		UID = Guid.NewGuid();
		Owner = owner;
		RealLayer = Owner.gameObject.layer;
	}
	public void Start()
	{
		this.RealLayer = Owner.gameObject.layer;//
		Owner.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
		if (Owner.GetComponent<Rigidbody>() != null) Owner.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;//si el objeto no tiene rigidBody se lo asigna -smb
		
		
		
		this.InitialPosition = Owner.transform.position;
		this.lastValidPosition = this.InitialPosition;
		IsActive = true;
		//Debug.Log("Starting Translation. Initial Position: ("+this.InitialPosition.x+";"+this.InitialPosition.y+";"+this.InitialPosition.z+")");
	}
	
	public void Cancel()
	{
		if (this.IsActive)
		{
			Owner.gameObject.layer = this.RealLayer;
			if (Owner.GetComponent<Rigidbody>() != null) Owner.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Discrete;
			//Debug.Log(string.Format("[{0}] Canceling Translation", UID));
			Owner.transform.position = InitialPosition;
			this.IsActive = false;
			Owner.IsSafe();
			//Debug.Log("Cancel Translation. Initial Position: ("+this.InitialPosition.x+";"+this.InitialPosition.y+";"+this.InitialPosition.z+")");
			RefreshOwner();
		}
	}
	
	public void End()
	{
		Owner.gameObject.layer = this.RealLayer;
		if (Owner.GetComponent<Rigidbody>() != null) Owner.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Discrete;
		//Debug.Log(string.Format("[{0}] Ending Translation",UID));
		this.IsActive = false;
		IsFristPositionMouse = true;//SMB: Esto setea a que tome desde cero la posicion inicial. ****
		RefreshOwner();
	}
	
	private void RefreshOwner()
	{
		Owner.UpdateBounds();
	}
	
	public void Finish()
	{
		if (Validate())
		{
			End();
		}
		else
		{
			Cancel();
		}
	}
	
	public Vector3 resvaltransf;
	public bool Validate()
	{
		resvaltransf = Owner.ValidateTransform ();
		//Debug.LogWarning ("TO - REsvaltransf = "+ resvaltransf );
		if (resvaltransf == new Vector3(0,-100,0)) {
			IsValid = true;
		} else {
			IsValid = false;
			//Owner.transform.position = resvaltransf;//no afecta al attached to wall
		}
		
		//IsValid = Owner.ValidateTransform();
		if (IsValid)
		{
			SetCurrentAsValid();
		}
		return IsValid;
	}
	
	public bool IsActive
	{
		get;
		protected set;
	}
	
	public bool IsValid
	{
		get;
		protected set;
	}
	
	public void FixedUpdate()
	{
		if (Owner.transform.position != lastValidPosition  && Owner.IsNotColliding)//
		{
			//Debug.Log(string.Format("[{0}] Setting position from {1} to: {2}.Not colliding: {3}", UID, Owner.transform.position, lastValidPosition, Owner.IsNotColliding));
			//  if()
			Owner.transform.position = lastValidPosition; // Devuelve el modelo a la ultima posicion valida
			//Debug.LogWarning ("TO - position = lastValidPosition = "+ lastValidPosition);
			//SetCurrentAsValid();//desactivado smb
			//  RefreshOwner();//desactivado smb
		}
	}
	
	public void SetCurrentAsValid()
	{
		//Debug.Log(string.Format("[{0}] Setting valid position from {1} to: {2}. IsNotColliding: {3}", UID, lastValidPosition, Owner.transform.position, Owner.IsNotColliding));
		lastValidPosition = Owner.transform.position;
		//Debug.LogWarning ("TO - lastValidPosition = position = "+ lastValidPosition);

		//lastValidPosition = ;//Posicion valida es igual a la ultima posicion antes de colisionar.//smb
		Owner.IsSafe();
	}
	
	
	public void StartAndSet(Vector3 unit)
	{
		//Debug.Log(string.Format("[{0}] Setting position from {1} to: {2}. IsNotColliding: {3}", UID, Owner.transform.position, unit, Owner.IsNotColliding));
		
		/*
		//------------------------------------- ****
		//SMB: Setea una vez la posicion inicial del mouse para
		if(IsFristMousePositionMouse){
			MouseInitPositionToMove = sceneManager.ClampPosition(hitinfo.point);
			IsFristMousePositionMouse = false;
		}
		//sceneManager.ta
		PositionToMove = sceneManager.ClampPosition(hitinfo.point) - MouseInitPositionToMove;//SMB: Esto termina de setear el valor que se pasara a mover
		PositionToMove = PositionToMove + sceneObject.transform.position;//SMB: Suma la distancia con la posicion inical del objeto.
		sceneObject.TranslationOperation.StartAndSet(PositionToMove);//SMB: Aca se cambia la posicion del objeto cuando se posiciona el objeto o se permite el movimiento del mismo con el boton mover. ****
		InitPositionToMove = sceneManager.ClampPosition(hitinfo.point);
		//------------------------------------- ****
		*/
		
		//------------------------------------- ****
		if (Owner.Item.fijadoA == "pared") {//SMB: AttachedToWall:
						
			
						Owner.transform.position = unit;
			
				} else {
						if (IsFristPositionMouse) {
							
								if (IsOnStarted) {
										IsOnStarted = false;
										InitPositionToMove = Owner.transform.position;
								} else {
										InitPositionToMove = unit;
								}
								IsFristPositionMouse = false;
				
						} else {
			
							/*	if (Owner.pepe == true) {//Agregado por SMB Esto no afecta al attached to wall

										//unit.x = Owner.Position2.x + 0.5f;
										//unit.z = Owner.Position2.z + 0.5f;
										unit = Owner.SafePlace;
										Owner.transform.position = unit;//Agregado por SMB
										Debug.Log ("///ValidaMueble = " + Owner.transform.position);//Agregado por SMB
										Owner.pepe = false;//Agregado por SMB

								} else{*/
			
			

							
				
								/*
			Vector3 unit2 = (unit - InitPositionToMove) + Owner.transform.position; //SMB: Esto termina de setear el valor que se pasara a mover
			Owner.transform.position = unit2;//SMB: Aca cambia el valor de la posicion del objeto respoecto de la poosicion del mouse que tiene que ver con el Ray que se obtiene del Camera.
			//InitPositionToMove = unit;//SMB: Aca se vuelve a tomar como cero el ultimo valor de mause como el inicial. ****
*/
				if(Owner.tag=="Muebles"&&unit.y!=0.1125F){
					unit.y=0.1125F;
				}
								//Vector3 unit2 = (unit - InitPositionToMove) + Owner.transform.position; //SMB: Esto termina de setear el valor que se pasara a mover
								Owner.transform.position = unit;//SMB: Aca cambia el valor de la posicion del objeto respecto de la posicion del mouse que tiene que ver con el Ray que se obtiene del Camera.
								//InitPositionToMove = unit;//SMB: Aca se vuelve a tomar como cero el ultimo valor de mause como el inicial. ****
								InitPositionToMove = unit;
								//------------------------------------- ****
								//Owner.transform.position = unit;//SMB: Aca cambia el valor de la posicion del objeto respoecto de la poosicion del mouse que tiene que ver con el Ray que se obtiene del Camera. ****
							//}

				}
		}
		RefreshOwner();
	}
	//public void SetCollision(Vector3 unit)//Agregado por SMB
	//{
	//	Owner.transform.position = Owner.SafePlace;//Agregado por SMB
	//	Debug.Log("///ValidaMueble = "+  Owner.transform.position);//Agregado por SMB
	//}
}