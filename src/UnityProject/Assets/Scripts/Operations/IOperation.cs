﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IOperation<TUnit>
{
    void Start();
    void Cancel();
    void End();
    void Finish();
    bool Validate();
    bool IsActive { get; }
    bool IsValid { get; }
    void FixedUpdate();
    void SetCurrentAsValid();
    void StartAndSet(TUnit unit);
}