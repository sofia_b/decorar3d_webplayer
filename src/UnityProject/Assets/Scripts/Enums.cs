﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum Cursors
{
    None,
    Move,
    Rotate,
    Pan,
    Add,
    Remove,
    Painting,

}
public enum ActionMode
{
    None,
    Moving,
    Rotating,
    Painting,
    Adding

}