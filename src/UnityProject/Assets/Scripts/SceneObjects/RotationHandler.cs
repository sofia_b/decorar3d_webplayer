﻿using UnityEngine;
using System.Collections;


public class RotationHandler : MonoBehaviour {
	
	public bool RotationEnabled = false;
	SceneObject sceneObject;
	SceneManager sceneManager;
	protected bool IsDragging;
	protected bool WasDragging;
	// Use this for initialization
	void Start() {

						sceneObject = GetComponent<SceneObject> ();
						sceneManager = GameObject.FindGameObjectWithTag (SceneManager.DefaultTag).GetComponent<SceneManager> ();
						if (sceneObject == null) {
								RotationEnabled = false;
								//Debug.LogWarning (string.Format ("El objeto seleccionado no es un scene object: {0}", gameObject.name));
								return;
						}
				
	}
	
	//// Update is called once per frame
	//void Update() {
	//    //No estaba haciendo dragging en el frame anterior, y ahora si... Inicializo
	//    if (!WasDragging && IsDragging) {
	
	//        this.sceneManager.SetMode(ActionMode.Rotating);
	//        sceneObject.RotationOperation.Start();
	//        WasDragging = true;
	//    }
	//    if (RotationEnabled) 
	//    {
	//        if (sceneObject.TranslationOperation.IsActive) {
	//            sceneObject.TranslationOperation.Cancel();
	//        }
	
	//        if (Input.GetMouseButton(1)) {
	//            Debug.Log("Cancelando rotacion");
	//            sceneObject.RotationOperation.Cancel();
	//            sceneManager.SetSelected(null);
	//            RotationEnabled = false;
	//            sceneManager.SetMode(ActionMode.None);
	//            sceneManager.SendMessage("UpdateView");
	//        }
	//        else if (IsDragging & !sceneObject.RotationOperation.IsActive) {
	//            Debug.Log("Iniciando rotacion");
	//            sceneObject.RotationOperation.Start();
	//        }
	//        else if (Input.GetMouseButtonUp(0) & sceneObject.RotationOperation.IsActive) {
	//            Debug.Log("Terminando rotacion");
	//            sceneObject.RotationOperation.Finish();
	//            RotationEnabled = false;
	//            sceneManager.SetMode(ActionMode.None);
	//            sceneManager.SendMessage("UpdateView");
	//        }   
	//    }
	
	//    if (WasDragging && !IsDragging) {
	
	//        sceneObject.RotationOperation.Finish();
	//        this.sceneManager.SetMode(ActionMode.None);
	//        WasDragging = false;
	//    }
	//    WasDragging = IsDragging;
	//    IsDragging = false;
	//}
	//public void DoDrag(Vector2 delta) {
	//    if (RotationEnabled) {
	//        UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
	
	//        sceneManager.Mode = ActionMode.Rotating;
	//        if (sceneObject.TranslationOperation.IsActive) sceneObject.TranslationOperation.Cancel();
	//        sceneObject.RotationOperation.StartAndSet(Quaternion.Euler(0f, -0.5f * delta.x * sceneManager.RotationSpeed, 0f) * transform.localRotation);
	//        sceneObject.UpdateBounds();
	//        WasDragging = true;
	//    }
	//}
	
}
