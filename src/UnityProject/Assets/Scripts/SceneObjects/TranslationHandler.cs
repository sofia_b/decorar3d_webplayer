using UnityEngine;
using System.Collections;
using Assets.Scripts.Catalogue;
using UnityEngine.EventSystems;
public class TranslationHandler : MonoBehaviour
{
	
	public bool TranslationEnabled = false;
	SceneObject sceneObject;
	SceneManager sceneManager;
	
	LayerMask targetLayer;
	
	float xAbs;
	float zAbs;
	bool zIsValid;
	bool xIsValid;
	Vector3 facingDirection;
	float angle;
	RaycastHit hitinfo;
	float yAbs;
	public SceneObject item;
	Vector3 posicionFinalParedV;
	
	// Use this for initialization
	void Start()
	{
		sceneObject = GetComponent<SceneObject>();
		sceneManager = GameObject.FindGameObjectWithTag(SceneManager.DefaultTag).GetComponent<SceneManager>();
		if (sceneObject == null)
		{
			TranslationEnabled = false;
			//Debug.LogWarning(string.Format("El objeto seleccionado no es un scene object: {0}", gameObject.name));
			return;
		}
		
	}
	public Collider NombrePared;
	public Vector3 PosicionPared{ get; set; }
	// Update is called once per frame
	void Update()
	{
		//Activacion y desactivacion de traslacion - smb
		if (TranslationEnabled)
		{
			
			//si la translacion esta activa
			if (!sceneObject.TranslationOperation.IsActive)
			{
				if(!EventSystem.current.IsPointerOverGameObject()){
				Debug.Log("Starting translating operation");
				sceneObject.TranslationOperation.Start();
				sceneObject.ForceColliderMuebles = false;
				sceneObject.TCOL = 0;
				}
				
			}
			// si presiona el boton derecho
			if (Input.GetMouseButton(1))
			{
			//	Debug.LogWarning("Canceling translating operation");
				sceneObject.TranslationOperation.Cancel();
				TranslationEnabled = false;
				sceneManager.SetMode(ActionMode.None);
				sceneManager.SendMessage("UpdateView");
			}
			//si levanta el boton izquierdo
			else if (Input.GetMouseButtonUp(0))
			{
				sceneObject.TranslationOperation.Finish();
				TranslationEnabled = false;
				sceneManager.SetMode(ActionMode.None);
				sceneManager.SendMessage("UpdateView");
			}
			
			//-----------------///Aca funciona la posicion del mouse y la camara- smb
			//si presiona el boton izquierdo y la traslacion esta activa
			if (Input.GetMouseButton(0) && sceneObject.TranslationOperation.IsActive)
			{
				sceneObject.TCOL = 0;
				var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				//mouseRay.
				
				//= sceneObject.Item.attachedToWall ? sceneManager.WallLayer : sceneManager.FloorLayer;
				
				// Chequeo en Update en caso de Translation Handler al tener que mover el objeto.
				if (sceneObject.Item.fijadoA == "pared")//SMB: AttachedToWall: Chequeo de si el objeto es a "pared"
				{
					
					targetLayer = sceneManager.WallLayer;//SMB: AttachedToWall: Asignacion de objeto que sea Target a Wall Layer
					
				}else{
					
					
					targetLayer = sceneManager.FloorLayer;
					
					
				}
				
				//				Debug.Log("RayCast - TranslationHandler (13)");
				RaycastHit[] hitsinfo = Physics.RaycastAll(mouseRay, 10, targetLayer);// - smb
				Vector3 lastPoint = Camera.main.transform.position;
				Color color1 = Color.cyan;
				Color color2 = Color.magenta;
				Color normalColor = Color.yellow;
				bool altColor = false;
				bool located = false;
				
				
				
				
				
				
				
				if (hitsinfo.Length > 0)// Mantiene la data del HitInfo
				{
					
					//------------  Objetos de pared - smb 
					//SMB: ------>>>>> AttachedToWall: Comportamiento de Translation Handler en caso de que sea "Pared" y que elhits info sea mayor que 0.
					if (sceneObject.Item.fijadoA == "pared")//SMB: AttachedToWall: Valida si el objeto es posicionado en la pared- smb
					{
						//Debug.Log("-Begin----------------------------------------------");
						Vector3 PosicionFinalAPared=sceneObject.transform.position;
						//Debug.Log("AA) sceneObject.transform.position: "+sceneObject.transform.position.ToString("F4"));
						
						
						///------------
						if( hitsinfo.Length > 1)
						{
							
							//Aca se elimina 1 y se toma solo el 2do collider
							//Debug.Log ("hitsinfo.Length: " + hitsinfo.Length);
							
							
							//Debug.Log ("hitsinfo.[1]: " + hitsinfo[1].collider.name);
							NombrePared = hitsinfo[1].collider;
							
							
							
							
						}else{
							
							
							NombrePared = hitsinfo[0].collider;
							
						}
						
						
						///---------------
						
						//foreach (RaycastHit hitinfo in hitsinfo)//SMB:indico al raycast que se almacena en variable hitsinfo
					//	Debug.Log("---FOREACH------------------");
						int i=0;
						angle= 0;
						foreach (RaycastHit hitinfo in hitsinfo)//SMB:indico al raycast que se almacena en variable hitsinfo
						{
							i=i+1;
						//	Debug.Log("---Begin FOREACH nro "+i+"------------------");
							if(hitsinfo.Length == 1 ){
							//	Debug.Log ("a) hitsinfo.lenght: " + hitsinfo.Length);
							//	Debug.Log ("a) hitinfo.point[0]: " + hitinfo.point.ToString("F4"));
								
								PosicionPared = hitinfo.point;
								PosicionFinalAPared=hitinfo.point;
								
								
								
							}else{
								if(hitsinfo.Length>=1)
								{
									Debug.DrawLine(lastPoint, hitinfo.point, altColor ? color2 : color1);//SMB: Dibujo una linea de debug 
									lastPoint = hitinfo.point;//SMB: Almacena el ultimo valor verificado en donde se posiciono el punto.
									altColor = !altColor;//SMB: AttachedToWall: Verificar?????
									Debug.DrawRay(hitinfo.point, hitinfo.normal, Color.green);//SMB: 
									
									
									
									//Debug.Log ("b) hitsinfo.lenght: " + hitsinfo.Length);
									//Debug.Log ("b) hitinfo.point[1]: " + hitinfo.point.ToString("F4"));
									PosicionFinalAPared = hitinfo.point;
									for(i= hitsinfo.Length; i>0; i++)
									{
										Debug.Log("Colision Name = " + hitsinfo[i].collider.name);
									}							
									if (i >= 2) {
											PosicionFinalAPared = hitinfo.point;
										if(i >=3){
											PosicionFinalAPared = hitinfo.point;
										//	Debug.Log("21323123123123" + hitinfo.collider.name);

										}else{
											
										}
									} 
															
								}else{
									
								}
								
							}
							
							
							
							
							//Debug.Log ("LOCATED: " + located);
							//if (located) continue;//SMB: AttachedToWall: Verificar?????
							
							if (sceneObject.RotationOperation.IsActive) sceneObject.RotationOperation.Cancel();//SMB: se desactiva la rotacion
							
							Debug.DrawRay(sceneObject.transform.position, sceneObject.transform.forward);//SMB: se verifica la posicion del modelo y la de transformacion.
							
							
							
							//Debug.Log ("c) PosicionParedFinal:" + PosicionPared.ToString("F4"));
							
							//Verificacion de valores captados por hitinfo. 
							//De esta manera devuele el valor de todas las paredes para calcular el angulo correspondiendo a 
							//la pared a la que se esté apuntando.
							
							//Identifico el nombre del objeto con
							//	Debug.Log("hitinfo.collider.name= " + hitinfo.collider.name);
							
							
							
							//-------------------------------------//
							bool PuertaOVentana = false;
							//El ángulo de rotacion de Y. attached to wall
							if(sceneObject.CategoriaM =="612" || sceneObject.CategoriaM =="611" )
							{
								PuertaOVentana = true;
							}
							
							if(i==1||i==2){
								switch(hitinfo.collider.name)
								{
								case "Centro North Wall": 
									angle = 180;
									//posicionFinalParedV.z = 0.5f;
									if(PuertaOVentana == true)
									{
										PosicionFinalAPared.z = PosicionFinalAPared.z -0.05f;
									}

									break;
								case "Abajo North Wall": 
									angle = 180;
									if(PuertaOVentana == true)
									{

									PosicionFinalAPared.z = PosicionFinalAPared.z -0.05f;
									}
										break;
								case "Derecha North Wall": 
									angle = 180;
									if(PuertaOVentana == true)
									{

										PosicionFinalAPared.z = PosicionFinalAPared.z -0.05f;
									}
									break;
								case "Izquierda North Wall": 
									angle = 180;
									if(PuertaOVentana == true)
									{

										PosicionFinalAPared.z = PosicionFinalAPared.z -0.05f;
									}
									break;
									
								case "Centro East Wall":
									angle = -90;

									
									if(PuertaOVentana == true)
									{
										
										PosicionFinalAPared.x = PosicionFinalAPared.x - 0.05f;
									}
									break;
								case "Abajo East Wall": 
									angle = -90;
									if(PuertaOVentana == true)
									{
										
										PosicionFinalAPared.x = PosicionFinalAPared.x - 0.05f;
									}
									break;
								case "Derecha East Wall": 
									angle = -90;
									if(PuertaOVentana == true)
									{

										PosicionFinalAPared.x = PosicionFinalAPared.x -0.05f;
									}
									break;
								case "Izquierda East Wall": 
									angle = -90;
									if(PuertaOVentana == true)
									{
										
										PosicionFinalAPared.x = PosicionFinalAPared.x - 0.05f;
									}
									break;
									
								case "Centro West Wall":
									angle = 90;
									if(PuertaOVentana == true)
									{

										PosicionFinalAPared.x = PosicionFinalAPared.x + 0.05f;
									}
									break;
								

								case "Abajo West Wall": 
									angle = 90;
									if(PuertaOVentana == true)
									{
										
										PosicionFinalAPared.x = PosicionFinalAPared.x + 0.05f;
									}
									break;
								case "Derecha West Wall": 
									angle = 90;
									if(PuertaOVentana == true)
									{
										
										PosicionFinalAPared.x = PosicionFinalAPared.x + 0.05f;
									}
									break;
								case "Izquierda West Wall": 
									angle = 90;
									if(PuertaOVentana == true)
									{
										
										PosicionFinalAPared.x = PosicionFinalAPared.x + 0.05f;
									}
									break;
								case "Centro South Wall": 
									angle = 0;

									if(PuertaOVentana == true)
									{
										PosicionFinalAPared.z = PosicionFinalAPared.z +0.05f;
									}

									break;

								


								default:
									switch(hitinfo.collider.tag)
									{
									case "TV": 
										int L = hitsinfo.Length;
									//	Debug.Log ("LLLLLLLLL = "  +L);
										float rotationY = hitinfo.collider.transform.eulerAngles.y;
										//Debug.Log ("LLLRotation Y = "  + rotationY);
										string rotationName = hitinfo.collider.name;
									//	Debug.Log ("LLLRotation NAME = "  + rotationName);
										angle = rotationY;
										float rotTH = hitinfo.collider.GetComponent<SceneObject>().transform.rotation.y;
									//	Debug.Log("Rotation Y!!!!!!!" + rotTH);
										//angle = -180;
										//float RM = this.gameObject.GetComponent<Transform>().rotation.y;
										//RM = 180;
									//	Debug.Log("TV!!!!");
										PosicionFinalAPared.z = PosicionFinalAPared.z +0.05f;						
										break;

									default:
										angle = 0;
										if(PuertaOVentana == true)
										{
											PosicionFinalAPared.z = PosicionFinalAPared.z +0.05f;
										}
										
										break;
									}

									break;
								}
							}else{
								if(i>=3){
									switch(hitinfo.collider.tag)
									{
									case "TV": 
										int L = hitsinfo.Length;
										//Debug.Log ("LLLLLLLLL = "  +L);
										float rotationY = hitinfo.collider.transform.eulerAngles.y;
									//	Debug.Log ("LLLRotation Y = "  + rotationY);
										string rotationName = hitinfo.collider.name;
									//	Debug.Log ("LLLRotation NAME = "  + rotationName);
										angle = rotationY;
										float rotTH = hitinfo.collider.GetComponent<SceneObject>().transform.rotation.y;
										//Debug.Log("Rotation Y!!!!!!!" + rotTH);
										//angle = -180;
										//float RM = this.gameObject.GetComponent<Transform>().rotation.y;
										//RM = 180;
										//Debug.Log("TV!!!!");
										PosicionFinalAPared.z = PosicionFinalAPared.z +0.05f;						
										break;
									}
								}

							}
							
							//Debug.Log("ROTACION B (hitinfo.collider.name="+hitinfo.collider.name+"): angle = "+angle);
							
							
							//Fin de Rotacion de attached to Wall
							
							//-------------------------------------//
							
							//aca cambiar el hitI
							
							//Debug.Log ("d) PosicionParedFinal2:" + PosicionPared.ToString("F4"));
						//	Debug.Log ("d) hitinfo.point:" + hitinfo.point.ToString("F4"));
							//sceneObject.TranslationOperation.StartAndSet(sceneManager.ClampPosition(hitinfo.point, sceneObject.AlturaModelo));//SMB: AttachedToWall: Setea el cambio de psoicion del objeto cuando... pero no se que afecte al resto del programa.... ****
							//sceneObject.TranslationOperation.StartAndSet(sceneManager.ClampPosition( PosicionPared , sceneObject.AlturaModelo));//SMB: AttachedToWall: Setea el cambio de psoicion del objeto cuando... pero no se que afecte al resto del programa.... ****
							
							
							
							
							//located = true;//SMB: AttachedToWall: Verificar?????
							//continue;//SMB: AttachedToWall: Verificar?????
							
							
						}


				//		Debug.Log("---END FOREACH------------------");
						if (sceneObject.RotationOperation.IsActive) sceneObject.RotationOperation.Cancel();//SMB: AttachedToWall: Verificar?????
						sceneObject.RotationOperation.StartAndSet(Quaternion.Euler(new Vector3(0,angle,0)));//SMB: AttachedToWall: Verificar?????
						sceneObject.TranslationOperation.StartAndSet(sceneManager.ClampPosition(PosicionFinalAPared, sceneObject.AlturaModelo));//SMB: AttachedToWall: Setea el cambio de psoicion del objeto cuando... pero no se que afecte al resto del programa.... ****
				//		Debug.Log("Aca voy yo, PosicionFinalAPared="+PosicionFinalAPared.ToString("F4"));
				//		Debug.Log("-End----------------------------------------------");
						
					}else{
						//SMB: ------>>>>> not  AttachedToWall: End Behavior Translation Handler 
						
						for (int i = hitsinfo.Length - 1; i >= 0 ; i--)
						{
							
							hitinfo = hitsinfo[i];
							yAbs = Mathf.Abs(hitinfo.normal.y);
							if (yAbs>=1)
							{
								//Debug.DrawRay(hitinfo.point, hitinfo.normal, Color.green);
								if (sceneObject.RotationOperation.IsActive) sceneObject.RotationOperation.Cancel();
								sceneObject.TranslationOperation.StartAndSet(sceneManager.ClampPosition(hitinfo.point, sceneObject.AlturaModelo));//SMB: Original Script. ****
								break;
								
								/*Debug.Log ("Translation Handler No Pared");
								
								Debug.Log ("No Pared /// hitsinfo.lenght: " + hitsinfo.Length);
								Debug.Log ("No Pared /// hitinfo.point[1]: " + hitinfo.point.ToString("F4"));

								for(i= hitsinfo.Length; i>0; i++)
								{
									Debug.Log("Colision Name = " + hitsinfo[i].collider.name);
								}		*/					
							}
						}
						
						
					}
					
					
				}
			}
		}
	}
}
