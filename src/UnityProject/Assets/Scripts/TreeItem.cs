using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;

public class TreeItem : MonoBehaviour
{
    public TreeView treeview;
    public string title;
    private string lastTitle;
    public string spriteName;
    private string lastSpriteName;
    public bool expanded;
    private bool lastExpanded;

    public object innerObject
    {
        get;
        set;
    }

    UILabel label;
    UISprite sprite;
    UIButton button;
    UIWidget widget;
    public int indentation;
    private int lastIndentation;
    public Material itemMaterial;
    private ObjReader.ObjData objData;
    private List<TreeItem> children = new List<TreeItem>();
    bool loadingObject;
    public bool AllowDrag;
    bool SpawnRequested;
    Transform futureSpawnObject;
    Vector3 futureSpawnPoint;

    SceneManager SceneManager;

    void Awake()
    {
        label = GetComponentInChildren<UILabel>();
        sprite = GetComponentInChildren<UISprite>();
        widget = GetComponent<UIWidget>();
        button = GetComponent<UIButton>();
        button.onClick.Add(new EventDelegate(this, "Clicked"));
        SceneManager = GameObject.FindGameObjectWithTag(SceneManager.DefaultTag).GetComponent<SceneManager>();
    }
    // Start is called just before any of the
    // Update methods is called the first time.
    void Start()
    {
        label.text = title;
        sprite.spriteName = spriteName;
        label.overflowMethod = UILabel.Overflow.ResizeHeight;
    }

    // Update is called every frame, if the
    // MonoBehaviour is enabled.
    void Update()
    {
        if (lastTitle != title)
        {
            label.text = title;
            lastTitle = title;
        }
        if (lastSpriteName != spriteName)
        {
            sprite.spriteName = spriteName;
            lastSpriteName = spriteName;
        }
        if (indentation != lastIndentation)
        {
            updateIndentation();
        }
        if (lastExpanded != expanded)
        {
            lastExpanded = expanded;
            toggleExpansion();
        }
    }

    void Clicked()
    {

        bool old = expanded;
        if (children.Count > 0)
        {
            expanded = !expanded;

            Debug.Log(string.Format("Clicked. Bfr: {0}. Now: {1}. Last: {2}", old, expanded, lastExpanded));
        }
        else
        {

            CatalogueItem item = innerObject as CatalogueItem;
            if (item != null && item.modelObject == null)
            {
                TryToLoadObject();
            }
            else if (item != null && item.modelObject != null)
            {
                PositionObject();
            }

        }
    }

    void TryToLoadObject()
    {
        CatalogueItem item = innerObject as CatalogueItem;
        if (item != null && item.modelObject == null)
        {
            if (!loadingObject)
            {
                StartCoroutine(LoadModel());
            }
        }
    }

    private IEnumerator LoadModel()
    {

        CatalogueItem item = innerObject as CatalogueItem;
        if (item != null && item.modelObject == null)
        {
            loadingObject = true;
            try
            {
                var url = item.model.IndexOf("http") > -1 ? item.model : Configuration.Endpoint + item.model;
                objData = ObjReader.use.ConvertFileAsync(url, item.modelUseMtl, itemMaterial);
            }
            catch (UnityException ex)
            {
                Debug.LogError(ex);
            }
            while (!objData.isDone)
            {
                label.text = string.Concat(item.name, " ", (objData.progress * 100).ToString("f0"), "%");
                yield return null;
            }
            loadingObject = false;
            if (objData == null || objData.gameObjects == null)
            {
                label.text = string.Concat(item.name, " Error");
                yield return false;
            }
            else
            {
                label.text = item.name;
                item.modelObject = objData.gameObjects[0];
                item.modelObject.SetActive(false);
                SceneManager.ApplyCatalogueToObject(item, item.modelObject);

                PositionObject();
            }
        }
    }

    public TreeItem parent
    {
        get;
        set;
    }

    public void addChild(TreeItem childItem)
    {
        children.Add(childItem);
    }

    public void removeChild(TreeItem childItem)
    {
        children.Remove(childItem);
    }

    public void updateIndentation()
    {
        float indentOffset = (indentation * 20);
        float offset = widget.width / 2 * -1 + indentOffset;
        sprite.transform.localPosition = new Vector3(offset + (sprite.width / 2), sprite.transform.localPosition.y, sprite.transform.localPosition.z);
        label.transform.localPosition = new Vector3(offset + (sprite.width * 1.5f) + 4, label.transform.localPosition.y, label.transform.localPosition.z);
        label.width = widget.width + (int)offset;
        lastIndentation = indentation;
        foreach (TreeItem child in children)
        {
            child.indentation = indentation + 1;
        }
    }
    void PositionObject()
    {
        CatalogueItem item = (this.innerObject as CatalogueItem);
        if (item.modelObject != null)
        {
            SceneManager.TryToSpawnModel(item, futureSpawnPoint);
            SpawnRequested = false;
        }
    }


    public void toggleExpansion()
    {

        if (children.Count > 0)
        {
            if (!sprite.enabled)
            {

                sprite.enabled = true;
            }
            if (expanded)
            {
                spriteName = "Down";
            }
            else
            {
                spriteName = "Up";
            }
            foreach (TreeItem item in children)
            {
                item.expanded = expanded;
                item.gameObject.SetActive(expanded);
            }
            treeview.requiresReposition = true;
        }
        else
        {
            spriteName = null;
            sprite.enabled = false;
        }
    }
}
