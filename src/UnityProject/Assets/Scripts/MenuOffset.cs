﻿using UnityEngine;

public class MenuOffset : MonoBehaviour
{
    public GameObject panelMovCamara;
    public GameObject panelLeng;
    public GameObject panelHerramientas;

    public float offsetLenguage;
    public float offsetCamera;
    public float offsetHerramienta;

    public void OffsetOpenPanel(string panelOpen)
    {
        switch (panelOpen)
        {
            case "lenguage":
                panelMovCamara.transform.Translate(Vector3.down*offsetCamera);

			Debug.Log("fdslnfsdfnsdkfnslfksnflsdkfsd");
                break;
            case "camera":
                panelMovCamara.transform.Translate(Vector3.down * offsetCamera);
                panelHerramientas.transform.Translate(Vector3.down * offsetHerramienta * 4);
			Debug.Log("fdslnfsdfnsdkfnslfksnflsdkfsd");
                break;
            //case "herramienta":
            //    panelHerramientas.transform.Translate(Vector3.down * offsetHerramienta * 4);
            //    break;
        }
    }

    public void OffsetClosePanel(string panelClose)
    {
        switch (panelClose)
        {
            case "lenguage":
                panelMovCamara.transform.Translate(Vector3.up*offsetCamera);
                break;
            case "camera":
                panelMovCamara.transform.Translate(Vector3.up * offsetCamera);
                panelHerramientas.transform.Translate(Vector3.up * offsetHerramienta * 4);
                break;
            //case "herramienta":
            //    panelHerramientas.transform.Translate(Vector3.up * offsetHerramienta * 4);
            //    break;
        }
    }
}
