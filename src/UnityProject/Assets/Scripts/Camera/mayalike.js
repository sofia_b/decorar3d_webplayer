static var target : Transform;
static var distance : float;
 
var panSpeed = 5;
var smooth = 5;
var xSpeed = 250.0;
var ySpeed = 120.0;
 
var aimer : Transform;
 
var yMinLimit = -20;
var yMaxLimit = 80;
 
private var x = 0.0;
private var y = 0.0;
private var z = 0.0;
 
@script AddComponentMenu("Camera-Control/Maya Like")
 
function Update () {
        //Orbit = ALT + LEFT MOUSE BUTTON
        if (Input.GetKey (KeyCode.LeftAlt) &&  Input.GetMouseButton(0) ) {
        x += Input.GetAxis("Mouse X") * xSpeed * 0.02;
        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02;
       
        y = ClampAngle(y, yMinLimit, yMaxLimit);
       
        var rotation2 = Quaternion.Euler(y, x, z);
        transform.rotation = rotation2;
        }
        if (target) {
        var rotation3 = Quaternion.Euler(y, x, z);
        var position3 = rotation3 * Vector3(0.0, 0.0, -distance) + target.position;
        transform.rotation = rotation3;
        transform.position = Vector3.Lerp(transform.position, position3, Time.deltaTime * smooth);     
        }  
        if (Input.GetKey (KeyCode.LeftAlt)  && Input.GetMouseButton(0) ) {
        x += Input.GetAxis("Mouse X") * xSpeed * 0.02;
        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02;
       
        y = ClampAngle(y, yMinLimit, yMaxLimit);
       
        var rotation = Quaternion.Euler(y, x, z);
        var position = rotation * Vector3(0.0, 0.0, -distance) + target.position;
        transform.rotation = rotation;
        transform.position = position;
        }
       
        //Pan = ALT + RIGHT MOUSE BUTTON
        if (Input.GetKey(KeyCode.LeftAlt) &&  Input.GetMouseButton(1) ) {
        aimer.position = target.position;
        target = aimer;  
        aimer.Translate(transform.right * -Input.GetAxis("Mouse X") * panSpeed, Space.World);
        aimer.Translate(transform.up * -Input.GetAxis("Mouse Y") * panSpeed, Space.World); 
        var rotation1 = Quaternion.Euler(y, x, z);
        var position1 = rotation1 * Vector3(0.0, 0.0, -distance) + aimer.position;
        transform.rotation = rotation1;
        transform.position = position1;
 
        }
        //Zoom = ALT + MIDDLE MOUSE BUTTON
     if (Input.GetKey(KeyCode.LeftAlt)  && Input.GetMouseButton(2) ) {
        aimer.Translate(transform.forward * -Input.GetAxis("Mouse X") * panSpeed, Space.World);
        transform.Translate(transform.forward * -Input.GetAxis("Mouse X") * panSpeed, Space.World);
        }
    }
 
static function ClampAngle (angle : float, min : float, max : float) {
    if (angle < -360)
        angle += 360;
    if (angle > 360)
        angle -= 360;
    return Mathf.Clamp (angle, min, max);
}