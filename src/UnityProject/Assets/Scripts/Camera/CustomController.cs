using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomController : MonoBehaviour {

    public float translateSpeed = 0.2f;
    public float mouseXModifier = 1f;
    public float mouseYModifier = 1f;
	// Update is called every frame, if the
	// MonoBehaviour is enabled.
	void Update () {
        Vector3 direction = transform.localPosition;

      /*  if (Input.GetKey(KeyCode.W))
        {
            direction.z += translateSpeed;
        }*/
		if (Input.GetKey(KeyCode.E))
		{
			direction.z += translateSpeed;
		}
        else if (Input.GetKey(KeyCode.S))
        {
            direction.z -= translateSpeed;
        }
        if (Input.GetKey(KeyCode.A))
        {
            direction.x -= translateSpeed;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            direction.x += translateSpeed;
        }

        float rotY = 0f;
        float rotz = 0f;
        if (Input.GetMouseButton(1))
        {
            rotY = Input.GetAxis("Mouse X") * mouseXModifier;
            rotz = Input.GetAxis("Mouse Y") * -1 * mouseYModifier;
           // Debug.Log(string.Format("RotY: {0}, RotZ: {1}", rotY, rotz));
            transform.eulerAngles = transform.eulerAngles + new Vector3(rotz, rotY, 0);
        }
        if (direction != Vector3.zero)
        {
            transform.localPosition = direction;
        }

	}
	
}
