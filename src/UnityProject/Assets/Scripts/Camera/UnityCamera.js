var rotateSpeed : float = 5.0f;
var moveSpeed : float = 1.0f;
var zoomSpeed : float = 20.0f;
var speedModifier : float = 5.0f;
 
function Update() {
    GetComponent.<Camera>().fieldOfView -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
 
    var sensitivity : float = 1.0f;
    if(Input.GetKeyDown("left shift") || Input.GetKeyDown("right shift"))
        sensitivity *= speedModifier;
 
   var x : float = Input.GetAxisRaw("Horizontal") * Time.deltaTime * 10.0;
    var y : float = Input.GetAxisRaw("Vertical") * Time.deltaTime * 10.0;
 
    transform.position.x += x * moveSpeed * speedModifier;
    transform.position.z += y * moveSpeed * speedModifier;
 
    x = Input.GetAxis("Mouse X");
    y = Input.GetAxis("Mouse Y");
 
    if(Input.GetMouseButton(1)) {//RMB
        if(Input.GetKey("right alt") || Input.GetKey("left alt")) {
            //You might want something more clever here
            var zoomAmount = (x - y) * zoomSpeed * sensitivity;
            if(GetComponent.<Camera>().fieldOfView - zoomAmount < 1)
                GetComponent.<Camera>().fieldOfView = 1;
            else if(GetComponent.<Camera>().fieldOfView - zoomAmount > 179)
                GetComponent.<Camera>().fieldOfView = 179;
            else GetComponent.<Camera>().fieldOfView -= zoomAmount;
        }
        else {
            transform.eulerAngles.y += x * rotateSpeed;
            transform.eulerAngles.x -= y * rotateSpeed;
        }
    }
    if(Input.GetMouseButton(2)) {//MMB
        transform.position.x -= x * moveSpeed * sensitivity;
        transform.position.y -= y * moveSpeed * sensitivity;
    }
}