using UnityEngine;
using System.Collections;
using Assets.Scripts.Camera;
using Assets.Scripts.InputProviders;

public class ExtendedFlycam : MonoBehaviour
{

    /*
    EXTENDED FLYCAM
        Desi Quintans (CowfaceGames.com), 17 August 2012.
        Based on FlyThrough.js by Slin (http://wiki.unity3d.com/index.php/FlyThrough), 17 May 2011.
 
    LICENSE
        Free as in speech, and free as in beer.
 
    FEATURES
        WASD/Arrows:    Movement
                  Q:    Climb
                  E:    Drop
                      Shift:    Move faster
                    Control:    Move slower
                        End:    Toggle cursor locking to screen (you can also press Ctrl+P to toggle play mode on and off).
    */

    public float cameraSensitivity = 90;
    public float climbSpeed = 4;
    public float normalMoveSpeed = 10;
    public float slowMoveFactor = 0.25f;
    public float fastMoveFactor = 3;

    public float rotationX = 0.0f;
    public float rotationY = 0.0f;
	public float rotationZ = 0f;

    public bool moveOnlyWithMouse = true;

    public Assets.Scripts.IInputProvider mouseAndKeyboardProvider;
    public Assets.Scripts.IInputProvider virtualPadProvider;

    void Start()
    {
        mouseAndKeyboardProvider = new MouseAndKeyboardProviders();
        virtualPadProvider = GameObject.FindObjectOfType<VirtualPadController>();
        Screen.lockCursor = true;

        Vector3 initial = transform.rotation.eulerAngles;
        rotationX = initial.y;
        rotationY = initial.x *-1;

    }

    void Update()
    {
        UserInput input = GetInput();
        if (!moveOnlyWithMouse || input.IsRightPressed)
        {
            rotationX += input.PointerHorizontalAxis * cameraSensitivity * Time.deltaTime;
            rotationY += input.PointerVerticalAxis * cameraSensitivity * Time.deltaTime;
            rotationY = Mathf.Clamp(rotationY, -90, 90);

            Screen.lockCursor = mouseAndKeyboardProvider.GetInput().IsRightPressed;
        }
        else
        {

            Screen.lockCursor = false;
        }

        transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);

        float horizontalAxis = input.MovementHorizontalAxis;
        if (horizontalAxis == 0)
        {
            if (input.IsMiddlePressed)
            {
                horizontalAxis = input.PointerHorizontalAxis;
            }
        }
        float verticalAxis = input.MovementVerticalAxis;
        if (verticalAxis == 0)
        {
            verticalAxis = input.ScrollWheelAxis * 2;
        }
        if (input.MoveFast)
        {
            transform.position += transform.forward * (normalMoveSpeed * fastMoveFactor) * verticalAxis * Time.deltaTime;
            transform.position += transform.right * (normalMoveSpeed * fastMoveFactor) * horizontalAxis * Time.deltaTime;
        }
        else if (input.MoveSlow)
        {
            transform.position += transform.forward * (normalMoveSpeed * slowMoveFactor) * verticalAxis * Time.deltaTime;
            transform.position += transform.right * (normalMoveSpeed * slowMoveFactor) * horizontalAxis * Time.deltaTime;
        }
        else
        {
            transform.position += transform.forward * normalMoveSpeed * verticalAxis * Time.deltaTime;
            transform.position += transform.right * normalMoveSpeed * horizontalAxis * Time.deltaTime;
        }
        float zoom = 0;
        if (input.IsMiddlePressed)
        {
            zoom = input.PointerVerticalAxis;
        }
        if (zoom > 0) { transform.position += transform.up * climbSpeed * Time.deltaTime; }
        if (zoom < 0) { transform.position -= transform.up * climbSpeed * Time.deltaTime; }

        if (input.ToggleLock)
        {
            Screen.lockCursor = (Screen.lockCursor == false) ? true : false;
        }
    }

    private UserInput GetInput()
    {
        UserInput userInput = mouseAndKeyboardProvider.GetInput();
        if (virtualPadProvider != null)
        {
            userInput += virtualPadProvider.GetInput();
        }
        return userInput;
    }

	public void ReinicioCamRotation(){
		Vector3 initial = transform.rotation.eulerAngles;
		rotationX = 0f;
		rotationY = -10f;
		rotationZ = 0f;//1.699173f;


		
		
	}
}