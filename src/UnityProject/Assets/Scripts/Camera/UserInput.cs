﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Camera
{
    [Serializable]
    public class UserInput
    {
        public bool IsRightPressed;
        public float PointerHorizontalAxis;
        public float PointerVerticalAxis;
        public float MovementHorizontalAxis;
        public float MovementVerticalAxis;
        public bool IsMiddlePressed;
        public float ScrollWheelAxis;
        public bool MoveFast;
        public bool MoveSlow;
        public bool ToggleLock;

        public static UserInput operator +(UserInput one, UserInput other)
        {
            UserInput result = new UserInput();
            result.IsRightPressed = one.IsRightPressed || other.IsRightPressed;
            result.IsMiddlePressed = one.IsMiddlePressed || other.IsMiddlePressed;
            result.PointerHorizontalAxis = Mathf.Clamp(one.PointerHorizontalAxis + other.PointerHorizontalAxis, -1f, 1f);
            result.PointerVerticalAxis = Mathf.Clamp(one.PointerVerticalAxis + other.PointerVerticalAxis, -1f, 1f);
            result.MovementHorizontalAxis = Mathf.Clamp(one.MovementHorizontalAxis + other.MovementHorizontalAxis, -1f, 1f);
            result.MovementVerticalAxis = Mathf.Clamp(one.MovementVerticalAxis + other.MovementVerticalAxis, -1f, 1f);
            result.ScrollWheelAxis = Mathf.Clamp(one.ScrollWheelAxis + other.ScrollWheelAxis, -1f, 1f);
            result.MoveFast = one.MoveFast || other.MoveFast;
            result.MoveSlow = one.MoveSlow || other.MoveSlow;
            result.ToggleLock = one.ToggleLock || other.ToggleLock;

            return result;
        }
    }
}
