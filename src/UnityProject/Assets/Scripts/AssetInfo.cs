﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class AssetInfo : IEqualityComparer<AssetInfo>
{
    public enum AssetType
    {
        Texture,
        Thumbnail
    }
    public AssetInfo(AssetType type, string path)
    {
        Type = type;
        Path = path;
    }
    public AssetType Type;
    public string Path;

    #region IEqualityComparer implementation

    public bool Equals(AssetInfo x, AssetInfo y)
    {
        return x.Path == y.Path && x.Type == y.Type;
    }

    public int GetHashCode(AssetInfo obj)
    {
        return obj.Path.GetHashCode() + obj.Type.GetHashCode();
    }

    #endregion
}