﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using System;

[Obsolete("Se usa todavia?",true)]
public class WallTexturesSwatch : MonoBehaviour {
    protected class WallTextureInfo
    {
        public CatalogueWallTexture WallTexture;
        public GameObject GameObject;
        public Texture LoadedTexture;
        

        public WallTextureInfo(CatalogueWallTexture wallTexture, UnityEngine.GameObject gameObject)
        {
            this.WallTexture = wallTexture;
            this.GameObject = gameObject;
        }

        public WallTextureInfo(CatalogueWallTexture wallTexture, UnityEngine.GameObject gameObject, Texture texture)
            : this(wallTexture, gameObject)
        {
            this.LoadedTexture = texture;
        }
    }
    SceneManager SceneManager;
    Transform TexturesContainer;
    List<WallTextureInfo> TextureMapping = new List<WallTextureInfo>();

    public GameObject SwatchModel;
    
    public int MaxAmount = 0;
    public int CurrentIndex = 0;
    public Vector2 SwatchSize = new Vector2(60, 60);
	// Use this for initialization
	void Start () {

        var managerObject = GameObject.FindGameObjectWithTag("SceneManager");
        if (managerObject != null) this.SceneManager = managerObject.GetComponent<SceneManager>();
        this.SceneManager.CatalogueUpdated +=UpdateTextures;
        TexturesContainer = this.transform.Find("Textures");
        UpdateTextures();
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < TextureMapping.Count; i++)
        {
            if (TextureMapping[i].LoadedTexture != null)
            {
                UITexture texture = TextureMapping[i].GameObject.GetComponentInChildren<UITexture>();
                if (texture.mainTexture == null)
                    texture.mainTexture = TextureMapping[i].LoadedTexture;
            }
            else
            {
                var texture = SceneManager.GetTexture(gameObject, TextureMapping[i].WallTexture.texture);
                if (texture != null)
                {
                    TextureMapping[i].GameObject.GetComponentInChildren<UITexture>().mainTexture = texture;
                }
            }
        }
	}

    void UpdateTextures()
    {
        if (this.SceneManager.Catalogue != null && this.SceneManager.Catalogue.WallTextures.Count > 0)
        {
            List<CatalogueWallTexture> textures = this.SceneManager.Catalogue.WallTextures;
            for (int i = 0; i < TexturesContainer.childCount; i++)
            {
                TexturesContainer.GetChild(i).gameObject.SetActive(false);
            }
            int swatchAmount = MaxAmount == 0 ? textures.Count : Mathf.Min(MaxAmount, textures.Count);
            List<AssetInfo> assets = new List<AssetInfo>();
            for (int i = 0; i < swatchAmount; i++)
            {
                CatalogueWallTexture wallTexture = textures[i];
                GameObject newSwatch = GameObject.Instantiate(SwatchModel) as GameObject; //TexturesContainer.GetChild(i).gameObject;
                newSwatch.transform.parent = TexturesContainer;
                newSwatch.transform.localScale = new Vector3(1, 1, 1);
                newSwatch.transform.localPosition = new Vector3(0, 0, 1);
                newSwatch.transform.localPosition = new Vector3(60 * i * -1, 0, 1);
                newSwatch.SetActive(true);
                EventDelegate.Add(newSwatch.GetComponent<UIButton>().onClick, ()=>SetTexture(wallTexture));
                TextureMapping.Add(new WallTextureInfo(wallTexture, newSwatch, null));
                AssetInfo assetToDownload = LoadMaterialForWall(wallTexture, newSwatch);
                if (assetToDownload != null) assets.Add(assetToDownload);
            }
            //TexturesContainer.GetComponent<UITable>().repositionNow = true;
            if (assets.Count > 0)
            {
                SceneManager.QueueForDownload(gameObject, assets.ToArray());
            }
        }
    }

    private void SetTexture(CatalogueWallTexture wallTexture)
    {
        SceneManager.SetWallTexture(wallTexture);
    }

    private AssetInfo LoadMaterialForWall(CatalogueWallTexture wallTexture, GameObject newSwatch)
    {
        AssetInfo asset = null;
        var material = this.SceneManager.GetMaterial(wallTexture.materialName);
        var texture = this.SceneManager.GetTexture(newSwatch, wallTexture.texture);
        UITexture textureSprite = newSwatch.GetComponentInChildren<UITexture>();
        //textureSprite.material = material;


        if (texture == null)
        {
            asset = new AssetInfo(AssetInfo.AssetType.Texture, wallTexture.texture);
        }
        else
        {
            textureSprite.mainTexture = texture;
            //textureSprite.GetComponent<UIWidget>().SetDimensions((int)SwatchSize.x, (int)SwatchSize.y);
            TextureMapping.First(tm => tm.WallTexture == wallTexture).LoadedTexture = texture;
        }
        return asset;
    }
    public void TextureDownloaded(KeyValuePair<string, Texture2D> textureKVP)
    {
        for (int i = 0; i < TextureMapping.Count; i++)
        {
            if (TextureMapping[i].LoadedTexture==null && textureKVP.Key== TextureMapping[i].WallTexture.texture)
            {
                AssetInfo assetToDownload = LoadMaterialForWall(TextureMapping[i].WallTexture, TextureMapping[i].GameObject);
                //if (assetToDownload != null) assets.Add(assetToDownload);
            }
        }
    }

}
