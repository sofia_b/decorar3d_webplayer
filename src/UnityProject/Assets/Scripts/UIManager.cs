﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Catalogue;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public bool CatalogueVisible = false;
    public CatalogueView CatalogueViewer;
    public GameObject DataPanel;

    public Button DeleteButton;

    public Text ModelText;
    public Text VariationText;
    public Text DescriptionText;
    public Text MeasuresText;

    public UIFont Font;

    protected RoomUIBehavior SelectedRoom;
    public RoomSelector RoomSelector;
    SceneManager SceneManager;
    public UIAtlas VariationAtlas;
    public string VariationSpriteName;

    public GameObject Rotator ;
	public GameObject PanelInfo;

    void Awake()
    {
        SceneManager = SceneManager ?? this.GetComponent<SceneManager>();
        SetCatalogueList();
    }
    // Use this for initialization
    void Start()
    {
        SceneManager = SceneManager ?? this.GetComponent<SceneManager>();
    }

    // Update is called once per frame
    void Update()
    {
        SetCatalogueList();

        UpdateView();
    }

    void UpdateMode()
    {

    }
    void SetCatalogueList()
    {
        if (!CatalogueVisible)
        {
            HideCatalogueList();
        }
        else if (CatalogueVisible)
        {
            ShowCatalogueList();
        }
    }

    void HideCatalogueList()
    {
        CatalogueVisible = false;
        CatalogueViewer.Hide();
    }
    void ShowCatalogueList()
    {
        CatalogueVisible = true;
        CatalogueViewer.Show();
    }
    public void ToggleCatalogueList()
    {

        CatalogueVisible = !CatalogueVisible;
        CatalogueViewer.SetVisible(CatalogueVisible);
    }
    public void UpdateView()
    {
    }

	//------------------------------------------------------------------//
	//Verificacion y activacion de rotacion

    public void SetSelected(GameObject selectedObject)
    {
		if (selectedObject == null || selectedObject.GetComponent<SceneObject> () == null) {
						DataPanel.SetActive (false);
						Rotator.SetActive (false);
						//PanelInfo.SetActive(false);
				} else {
						SceneObject so = selectedObject.GetComponent<SceneObject> ();
						UpdateRotationButton (so);
						//for (int i = selectedObject.transform.childCount - 1; i >= 0; i--)
						//{
						//    var child = selectedObject.transform.GetChild(i);
						//    child.gameObject.SetActive(false);
						//}
						ShowVariationInfo (so.Item, so.CurrentVariation, so);
						//hasVariations = false;
						//GameObject.GetComponent<SceneManager>().RotationSelected();
						/*
						so.CurrentVariation = so.Item.variations[index];
						so.ApplyVariation();
						*/
						
						if (SceneManager.GetComponent<SceneManager>().RotacionActiva == true) {
								 Rotator.SetActive(true);//sofi
						}else{
								 Rotator.SetActive(false);//sofi
					
						}
				}
    }
	//Fin de Verificacion y activacion de rotacion


    private void ShowVariationInfo(CatalogueItem item, CatalogueItemVariation variation, SceneObject so = null)
    {

		if (ModelText != null) ModelText.text = item.name;
        if (VariationText != null) VariationText.text = variation != null ? variation.name : item.name;
        if (DescriptionText != null) DescriptionText.text = item.description;
		if (MeasuresText != null) MeasuresText.text = string.Format("Altura: {0:F2}cm Largo: {1:F2}cm Ancho: {2:F2}cm", item.height, item.deep, item.width);
        DataPanel.SetActive(true);
     //   DeleteButton.gameObject.SetActive(so != null);
    }
    public void SetVariation(int index)
    {
        var so = SceneManager.SelectedObject.GetComponent<SceneObject>();
        so.CurrentVariation = so.Item.variations[index];
        so.ApplyVariation();
    }

    public void UpdateRotationButton(SceneObject sceneObject)
    {
		//SMB: ------>>>>> AttachedToWall: Chequeo en Update Roration Button si es a "pared".
		if (sceneObject.Item.fijadoA == "pared" )//SMB: AttachedToWall: Chequeo de objeto, tomado por el SceneObject.
        {
			Rotator.SetActive(false);//SMB: AttachedToWall: Esto desactiva el "girar" si es un objeto de pared desde la base de datos.
			//if(GetComponent<Room>){}//SMB?????: AttachedToWall:
			//Debug.Log("attachedToWall = true");//SMB?????: AttachedToWall:
        }else{
		//SMB: ------>>>>> AttachedToWall: End Check
		
			// Rotator.SetActive(true);//sofi
            var renderCameraPos = Camera.main.WorldToScreenPoint(sceneObject.GetRotationAnchor().transform.position);
            // Take the 3D point and convert it to screen
            Vector3 uiWorldPos = UICamera.mainCamera.ScreenToWorldPoint(renderCameraPos);


            // Now take this world point and make it relative to the widget you want to reposition
            Vector3 pixelPos = Rotator.transform.parent.transform.InverseTransformPoint(uiWorldPos);

            Rotator.transform.localPosition = pixelPos;
            Rotator.GetComponent<SpinWithMouse>().target = sceneObject.transform;

		//SMB: ------>>>>> AttachedToWall: Add {}
		}//SMB: AttachedToWall: Add {}
		//SMB: ------>>>>> AttachedToWall: End Add {}

    }

    internal bool IsSelectingRoom()
    {
        return SelectedRoom != null;
    }

    internal void ShowRoomSelector()
    {
        this.RoomSelector.gameObject.SetActive(true);
    }

    internal void SetSelectedRoom(RoomUIBehavior selectedRoom)
    {
        this.SelectedRoom = selectedRoom;
        CatalogueVisible = SelectedRoom != null;
    }

   public void ShowInfo(CatalogueItem item, CatalogueItemVariation Variation)
    {
        if (Variation != null)
        {
            ShowVariationInfo(item, Variation);
			this.DataPanel.SetActive(true);//sofia-mod 
        }
    }

    internal void HideInfo()
    {
        if (SceneManager.SelectedObject != null)
        {
            var so = SceneManager.SelectedObject.GetComponent<SceneObject>();
            ShowVariationInfo(so.Item, so.CurrentVariation, so);
        }
        else
        {
            this.DataPanel.SetActive(false);
        }
    }
}
