﻿using Assets.Scripts.Catalogue;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Plugins.Lights
{
    public class LightPluginBuilder : IPluginBuilder
    {
        public void Build(SceneObject target, ICatalogueItemPluginInfo info)
        {
            if ((info as LightsPluginInfo)==null)
            {
                throw new ArgumentException("The type " + info.ToString() + " isn't a valid type for this builder");
            }
            if (target.GetComponent<LightsPluginBehavior>() != null) return;
            LightsPluginBehavior behavior = target.gameObject.AddComponent<LightsPluginBehavior>();
            behavior.Create(info as LightsPluginInfo);
        }

        public Catalogue.ICatalogueItemPluginInfo GetPluginMetadata(SimpleJSON.JSONNode pluginNode)
        {
            LightsPluginInfo info = new LightsPluginInfo();
            LightsPluginInfo.LightInfo light = new LightsPluginInfo.LightInfo();
/*-*///NewJSOn//            light.Color = CatalogueParser.GetColor(pluginNode["color"]);/
			/*-*///NewJSOn//         light.Position = CatalogueParser.GetVector3(pluginNode["position"], Vector3.zero);
            light.Intensity = pluginNode["intensity"].AsFloat;
            info.Lights.Add(light);

            return info;
        }
    }
}
