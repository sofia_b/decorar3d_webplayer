﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Plugins.Lights
{
    public class LightsPluginBehavior : MonoBehaviour
    {
        public List<Light> lights = new List<Light>();
        public void Create(LightsPluginInfo lightsPluginBehavior)
        {
            for (int i = 0; i < lightsPluginBehavior.Lights.Count; i++)
            {
                LightsPluginInfo.LightInfo info = lightsPluginBehavior.Lights[i];
                GameObject lightObject = new GameObject();
                lightObject.transform.parent = transform;
                lightObject.transform.localScale = Vector3.one;
                lightObject.name = "Light " + i.ToString();
                lightObject.SetActive(true);
                var light = lightObject.AddComponent<Light>();
                light.color = info.Color;
                light.transform.localPosition = info.Position;
                light.intensity = info.Intensity;
                lights.Add(light);
            }
        }
       // void OnDisable()
       // {
       //     Debug.Log("Disabled!");
      //  }

      //  void OnEnable()
      //  {
      //  }


    }
}
