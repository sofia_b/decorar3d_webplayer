﻿using Assets.Scripts.Catalogue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Plugins.Lights
{
    public class LightsPluginInfo : ICatalogueItemPluginInfo
    {
        public class LightInfo
        {
            public Vector3 Position;
            public Color Color = Color.white;
            public float Intensity = 0f;
        }
        public List<LightInfo> Lights = new List<LightInfo>();
        public LightsPluginInfo()
        {

        }

        public string PluginName
        {
            get
            {
                return "Lights";
            }
        }
    }
}
