﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Plugins
{
    public static class PluginResolver
    {
        static Dictionary<string, IPluginBuilder> instances = new Dictionary<string, IPluginBuilder>();
        public static IPluginBuilder GetBuilder(string pluginName)
        {
            if (instances.ContainsKey(pluginName.ToLower()))
            {
                return instances[pluginName.ToLower()];
            }
            IPluginBuilder builder = null;
            if (pluginName.ToLower() == "lights")
            {
                builder = new Lights.LightPluginBuilder();
                instances.Add(pluginName.ToLower(), builder);
            }
            return builder;
        }
    }
}
