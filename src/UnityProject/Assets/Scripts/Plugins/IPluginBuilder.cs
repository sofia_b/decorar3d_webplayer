﻿using Assets.Scripts.Catalogue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Plugins
{
    public interface IPluginBuilder
    {
        void Build(SceneObject target, ICatalogueItemPluginInfo info);

        Assets.Scripts.Catalogue.ICatalogueItemPluginInfo GetPluginMetadata(SimpleJSON.JSONNode jSONNode);
    }
}
