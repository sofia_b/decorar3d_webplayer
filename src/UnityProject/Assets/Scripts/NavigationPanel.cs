using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NavigationPanel : MonoBehaviour {


	public CameraController Controller;
	// Start is called just before any of the
	// Update methods is called the first time.
	void Start () {
		
	}
	
	// Update is called every frame, if the
	// MonoBehaviour is enabled.
	void Update () {
		
	}


	public void ZoomUp()
	{
		Controller.RequestZoomUp = true;
	}
	public void ZoomDown()
	{
		
		Controller.RequestZoomDown = true;
	}
	public void PanUp()
	{
		
		Controller.RequestPanUp = true;
	}
	public void PanDown()
	{
		Controller.RequestPanDown = true;
	}
	public void PanRight()
	{
		Controller.RequestPanRight = true;
	}
	public void PanLeft()
	{
		Controller.RequestPanLeft = true;
	}
}
