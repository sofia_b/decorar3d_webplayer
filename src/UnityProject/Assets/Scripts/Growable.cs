﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class Growable : MonoBehaviour
{
	//---------------------------------------
	public Vector3 EscalaFloorActual;//SMB: Este Vector3 mantiene la escala del Floor inicial donde esta la textura, para poder hacer un resize en caso de ser necesario, luego va cambiando.
	//---------------------------------------

	public Guid GrowableID;

    public bool AffectChildren = false;


    public int NorthSize { get; private set; }
    public int SouthSize { get; private set; }
    public int EastSize { get; private set; }
    public int WestSize { get; private set; }


    public int UserTargetNorthSize = 1;
    public int UserTargetSouthSize = 1;
    public int UserTargetEastSize = 1;
    public int UserTargetWestSize = 1;

	private int TargetNorthSize = 1;
    private int TargetSouthSize = 1;
    private int TargetEastSize = 1;
    private int TargetWestSize = 1;


    public Dictionary<Sides, int> TargetSizes = new Dictionary<Sides, int>();

    public Dictionary<Sides, List<PlaceableFloor>> TilesInSide = new Dictionary<Sides, List<PlaceableFloor>>();
    public bool IsResizing = false;

    PlaceableFloor start;
    // Use this for initialization
    void Start()
    {
        start = this.GetComponent<PlaceableFloor>();
        NorthSize = 1;
        SouthSize = 1;
        EastSize = 1;
        WestSize = 1;
        GrowableID = Guid.NewGuid();
        TargetSizes.Add(Sides.North, 1);
        TargetSizes.Add(Sides.South, 1);
        TargetSizes.Add(Sides.East, 1);
        TargetSizes.Add(Sides.West, 1);

        TilesInSide.Add(Sides.North, new List<PlaceableFloor>() { start });
        TilesInSide.Add(Sides.South, new List<PlaceableFloor>() { start });
        TilesInSide.Add(Sides.East, new List<PlaceableFloor>() { start });
        TilesInSide.Add(Sides.West, new List<PlaceableFloor>() { start });

		//----------------------------------------------------
		GetComponentInChildren<Floor>().SetInitialSize(EscalaFloorActual);//SMB: Esto llama a una clase instanciada en un Children, a la cual directamente accedo a sus metodos para setear el valor inicial.
		//----------------------------------------------------
	}
    void OnDisable()
    {
        (this.GetComponent<Collider>() as BoxCollider).size = new Vector3(1, 0.25f, 1);
        transform.FindChild("S Handler").gameObject.SetActive(false);
        transform.FindChild("N Handler").gameObject.SetActive(false);
        transform.FindChild("W Handler").gameObject.SetActive(false);
        transform.FindChild("E Handler").gameObject.SetActive(false);
    }

    void OnEnable()
    {
        (this.GetComponent<Collider>() as BoxCollider).size = new Vector3(1f, 0.25f, 1f);
        transform.FindChild("S Handler").gameObject.SetActive(true);
        transform.FindChild("N Handler").gameObject.SetActive(true);
        transform.FindChild("W Handler").gameObject.SetActive(true);
        transform.FindChild("E Handler").gameObject.SetActive(true);
    }


    protected BuildingManager BuildingManager
    {
        get;
        private set;
    }
    private BuildingManager GetBuildingManager()
    {

        return BuildingManager ?? GameObject.FindGameObjectWithTag("SceneManager").GetComponent<BuildingManager>(); ;
    }

    // Update is called once per frame
    void Update()
    {
		if (UserTargetNorthSize != 0)
        {
            TargetSizes[Sides.North] = UserTargetNorthSize;//genera el piso. tengo que dividir el tamaño elegido por piso por el tamaño de la textura para que se generen las divisiones de manera correcta
            UserTargetNorthSize = 0;
        }
        if (UserTargetSouthSize != 0)
        {
            TargetSizes[Sides.South] = UserTargetSouthSize;
            UserTargetSouthSize = 0;
        }
        if (UserTargetEastSize != 0)
        {
            TargetSizes[Sides.East] = UserTargetEastSize;
            UserTargetEastSize = 0;
        }
        if (UserTargetWestSize != 0)
        {
            TargetSizes[Sides.West] = UserTargetWestSize;
            UserTargetWestSize = 0;
        }

        TargetNorthSize = TargetSizes[Sides.North];
        TargetSouthSize = TargetSizes[Sides.South];
        TargetEastSize = TargetSizes[Sides.East];
        TargetWestSize = TargetSizes[Sides.West];

        TargetNorthSize = Mathf.Max(1, TargetNorthSize);
        TargetEastSize = Mathf.Max(1, TargetEastSize);
        TargetWestSize = Mathf.Max(1, TargetWestSize);
        TargetSouthSize = Mathf.Max(1, TargetSouthSize);
        if (NorthSize != TargetNorthSize && !IsResizing)
        {
            SetSideSize(TargetNorthSize, Sides.North);
            NorthSize = TargetNorthSize;
        }
        if (EastSize != TargetEastSize && !IsResizing)
        {
            SetSideSize(TargetEastSize, Sides.East);
            if (AffectChildren)
            {
                SetChildrenSideSize(Sides.North, Sides.East, TargetEastSize);
                SetChildrenSideSize(Sides.South, Sides.East, TargetEastSize);
            }
            EastSize = TargetEastSize;
        }
        if (WestSize != TargetWestSize && !IsResizing)
        {
            SetSideSize(TargetWestSize, Sides.West);
            if (AffectChildren)
            {
                SetChildrenSideSize(Sides.North, Sides.West, TargetEastSize);
                SetChildrenSideSize(Sides.South, Sides.West, TargetEastSize);
            }
            WestSize = TargetWestSize;
        }
        if (SouthSize != TargetSouthSize && !IsResizing)
        {
            SetSideSize(TargetSouthSize, Sides.South);
            SouthSize = TargetSouthSize;
        }

		//----------------------------------------------------
		EscalaFloorActual = this.GetComponent<Transform>().transform.localScale;//SMB: Esto se usa para poder obtener el tamaño del transform actual
		GetComponentInChildren<Floor>().SetNewSize(EscalaFloorActual);//SMB: Esto llama a una clase instanciada en un Children, a la cual directamente accedo a sus metodos para setear el valor actual y que re-calcule.
		//----------------------------------------------------
	}

    private void SetChildrenSideSize(Sides side, Sides childrenSideToResize, int newSize)
    {
        for (int i = 1; i < TilesInSide[side].Count; i++)
        {
            TilesInSide[side][i].GetComponent<Growable>().TargetSizes[childrenSideToResize] = newSize;
        }
    }

    public void SetSideSize(int newSize, Sides side)
    {
        if (IsResizing) return;
        IsResizing = true;
        PlaceableFloor lastTile = null;
        if (TilesInSide[side].Count < newSize)
        {
            for (int i = TilesInSide[side].Count - 1; i < newSize; i++)
            {
                PlaceableFloor currentTile = null;
                if (lastTile == null)
                {
                   // Debug.Log(string.Format("Side: {0} - Obtaining start tile: {1}. Current Size: {2}. New Size: {3}. Tiles: {4}", side, i, TilesInSide[side].Count, newSize, TilesInSide[side].Count));
                    currentTile = TilesInSide[side][i];
                }
                else
                {
                   // Debug.Log(string.Format("Side: {0} - Obtaining neighbour from last tile: {1}. Current Size: {2}. New Size: {3}. Tiles: {4}", side, i, TilesInSide[side].Count, newSize, TilesInSide[side].Count));
                    currentTile = lastTile.GetNeighbour(side);
                    if (currentTile == null && lastTile.CreateNeighbour(side))
                    {
                   //     Debug.Log(string.Format("Side: {0} - Creating neighbour from last tile: {1}. Current Size: {2}. New Size: {3}. Tiles: {4}", side, i, TilesInSide[side].Count, newSize, TilesInSide[side].Count));
                        currentTile = lastTile.GetNeighbour(side);
                    }
                    else
                    {
                        if (currentTile == null)
                        {
                       //     Debug.LogWarning(string.Format("Side: {0} - Creating neighbour from last tile failed: {1}. Current Size: {2}. New Size: {3}. Tiles: {4}", side, i, TilesInSide[side].Count, newSize, TilesInSide[side].Count));
                        }
                    }
                }
                if (currentTile != null)
                {
                    if (!currentTile.Owners.Contains(GrowableID))
                    {
                        currentTile.Owners.Add(GrowableID);

                    }
                    if (!TilesInSide[side].Contains(currentTile))
                    {
                    //    Debug.Log(string.Format("Side: {2} - Adding tile to list. Index: {0}. Total Tiles: {1}", i, TilesInSide[side].Count, side));

                        TilesInSide[side].Add(currentTile);
                    }
                    lastTile = currentTile;
                }
            }
        }
        else if (TilesInSide[side].Count > newSize)
        {
            for (int i = TilesInSide[side].Count - 1; i >= newSize; i--)
            {
               // Debug.Log(string.Format("Obtaining target from {0} to {1}. Removing tile i: {2}. Total Tiles: {3}", TilesInSide[side].Count, newSize, i, TilesInSide[side].Count));
                PlaceableFloor target = TilesInSide[side][i];
                if (i > 0)
                {
                  //  Debug.Log(string.Format("Reducing from {0} to {1}. Removing tile i: {2}. Total Tiles: {3}", TilesInSide[side].Count, newSize, i, TilesInSide[side].Count));
                    TilesInSide[side].Remove(target);
                    if (target.Owners.Count == 1 && target.Owners[0] == GrowableID)
                    {
                  //      Debug.Log(string.Format("Removing floor i: {0}", i));
                        GetBuildingManager().RemoveFloor(target);
                    }
                }
            }
        }
      //  Debug.Log(string.Format("Side: {3} - Resized from {0} to {1}. Final Tile Count: {2}", TilesInSide[side].Count, newSize, TilesInSide[side].Count, side));
        IsResizing = false;
    }
}
