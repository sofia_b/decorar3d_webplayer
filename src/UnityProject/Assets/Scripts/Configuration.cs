public static class Configuration
{
	static string endpoint = "";
	public static string Endpoint {
		get {
			return endpoint;
		}
		set {
			endpoint = value;
		}
	}
}