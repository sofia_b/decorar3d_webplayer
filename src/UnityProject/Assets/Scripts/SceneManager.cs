using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;
using System.Linq;
using System.Collections.Generic;

using Assets.Scripts.Catalogue;
using System.Net;
using System.IO;
using LitJson;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text;
using System.Text.RegularExpressions;



public class SceneManager : MonoBehaviour
{
	public static string DefaultTag = "SceneManager";
	
	public static SceneManager SceneManagerInstance;
	public bool RoomSelected = false;
	public ActionMode Mode = ActionMode.None;
	public Transform ItemContainer;
	public string CatalogueURL;
	public string DecorarURL;//Solo almacena previsoriamente
	public string User;//Solo almacena previsoriamente

	public string newApiUrl;
	public string CategoryUrlItems;//Solo almacena previsoriamente


	public string RoomObjectsUrlItems;//Solo almacena previsoriamente
	public string WallUrlItems;//Solo almacena previsoriamente
	public string FloorUrlItems;//Solo almacena previsoriamente
	public Catalogue Catalogue;
	public LayerMask HudLayer;
	public LayerMask PositionLayer;
	public LayerMask WallLayer;
	public LayerMask FloorLayer;
	public LayerMask SelectionLayer;
	public GameObject SelectedObject;
	
	
	//MATERIALES
	public Material DefaultMaterial;
	public Material TransparentMaterial;
	
	
	//CAMARA
	public UICamera UICamera;
	
	//PANEL MUEBLE
	public bool RotacionActiva = false;//variable de verificacion de activacion-sofia
	public bool MovActivo = true;//variable de verificacion de activacion-sofia
	public bool ReinicioActivo = false;//Variable de verificacion para reinicio de aplicacion-sofia
	public bool InfoActivo = false;//Variable de verificacion para activacion de informacion
	public GameObject PanelInfo;
	
	//HABITACION
	public GameObject Room;
	public RoomSelector RoomSelector;
	
	[Multiline]
	public string TextData;
	
	public event Action CatalogueUpdated;
	
	public GameObject Camera3D;
	public GameObject UIRoot;
	public UIManager UIManager;
	public Material DimensionLetterMaterial;
	public Font DimensionFont;
	public Material[] DefaultMaterials;
	
	
	public List<SceneObject> LoadedObjects = new List<SceneObject>();
	
	public Queue<AssetInfo> AssetsQueue = new Queue<AssetInfo>();
	public List<string> DownloadingQueue = new List<string>();
	//public Dictionary<string, Texture2D> Thumbnails = new Dictionary<string, Texture2D> ();//*Originalmente desactivado
	public Dictionary<string, Texture> Textures = new Dictionary<string, Texture>();
	public Dictionary<string, Sprite> Sprites = new Dictionary<string, Sprite>();
	
	protected float RotationXDeg;
	protected float RotationYDeg;
	public float RotationSpeed = 1f;
	public float RotationFriction = 1f;
	
	
	public float amountOfTimeToMove = 0f;
	
	private float pressedCounter = 0f;
	
	public string Endpoint = null;
	
	protected float RotationLerpSpeed;
	public UnityEngine.EventSystems.EventSystem eventSystem;
	
	public Texture2D CursorMoving;
	public Texture2D CursorRotating;
	public Texture2D CursorPanning;
	public Texture2D CursorPlacing;
	public Texture2D CursorPainting;
	
	//: Var. ref. del objeto para varias parte del codigo
	public CatalogueItem CurrentCatalogueItem;// Objeto actual tomado de catalogo (sirve para saber en que estas parado).
	// End Def. Var. Ref.
	
	//Constantes Flotantes para definir altura de Ventanas y dePuertasrespectivamente.
	public float MinFurniturePositionATWDoors = 0.2f ; //0f default Posicion en Y del objeto Puerta
	public float MinFurniturePositionATWWindows = 0.2f ; //0f default Posicion en Y del objeto Ventana
	//End Defs.
	
	public bool FirstPosition;//posicion inicial del objeto seleccionado
	
	
	bool Descargado = false;//Booleano para identificar que se descargo el catalogo.
	//Optimizing
	string noCache;
	WWW www;
	
	GameObject copy;
	
	public SceneObject sceneObject ;
	TranslationHandler translationHandler;
	RotationHandler rotationHandler;
	
	
	RaycastHit hitinfo;
	
	RaycastHit uiHitinfo;
	private SceneObject CurrentBuildObject;
	private bool justSet = false;
	
	RaycastHit hitInfo;
	SceneObject so;
	Quaternion oldRotation;
	
	AssetInfo assetInfo;
	string path;
	
	AssetInfo asset;
	
	
	//Dictionary<string, Texture2D> textures;
	//Dictionary<string, Sprite> sprites ;
	KeyValuePair<string, Sprite> sprite;
	bool isDone = false;
	bool hasError = false;
	
	Texture2D loadedTexture;
	Sprite loadedSprite;
	bool assetIsReady;
	bool isInDownloadingQueue;
	bool isAssetInQueue;
	//bool assetIsReady;
	
	MeshCollider collider;
	BoxCollider boxCollider;
	
	List<Wall> walls;
	bool isNotInLayer ;
	Vector3 targetPosition;
	
	bool CountDownload = false;
	
	//	public GameObject ModelVerification;
	
	void Awake()
	{
		eventSystem = GameObject.FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
		LoadConfiguration();
		// CatalogueURL = string.IsNullOrEmpty(CatalogueURL) ? (Configuration.Endpoint + "catalogue.json.txt") : CatalogueURL;//Verifica y valida una de las 2 opciones
		UIManager = this.GetComponent<UIManager>();
		SceneManagerInstance = this;
		MovActivo = true;
		
	}
	//carga la configuracion de endPoint_smb
	void LoadConfiguration()
	{
		Configuration.Endpoint = string.IsNullOrEmpty(Endpoint) ? Application.dataPath + "/" : Endpoint;
		
		
		
	}
	
	public bool UseDebugCatalogue;
	// Use this for initialization
	void Start()
	{
		LoadMaterials();
		//RoomSelector = RoomSelector ?? this.GetComponent<RoomSelector>();
		
		
		#if !UNITY_EDITOR
		UseDebugCatalogue = false;
		 Application.ExternalCall("initPlayer","");
		#else
		Init();
		#endif
		
		logo ();
	}
	public void SetFeaturedPropertyName(string propertyName)
	{
		FeaturedPropertyName = propertyName;
		//Debug.Log("Setting FeaturedPropertyName: " + propertyName);
	}
	//Seteo de endPoint_smb
	public void SetEndpoint(string endpoint)
	{
		Endpoint = endpoint;
		//Debug.Log("Setting Endpoint: " + Endpoint);
	}

	//DebugTexts
	public Text TExtoUrl;
	public Text TExtoUrl2;
	public Text TExtoUrl3;
	//End of DebugTexts

	public string urlID;
	public string IDLogin;
	public string urlID3;
	public string UserLogId= "qg==";

	public void SetCatalogueID(string id)
	{

		urlID = id;


		//IDLogin= id;
		TExtoUrl2.text = urlID;
		//IDLogin= urlID.Remove(0,45);
		UserLogId = id.Remove (0, 3);
		TExtoUrl3.text = UserLogId;
	}

	//Seteo de Catalogo_smbs
	public void SetCatalogueUrl(string url)
	{
		//CatalogueURL = url+ IDLogin;
	//	CatalogueURL = CatalogueURL+ UserLogId;
		CatalogueURL = CatalogueURL;

		TExtoUrl.text = CatalogueURL;

		//Debug.Log("Setting Url: " + CatalogueURL);
	}
	
	
	
	
	
	public void SetHomeMaxAmount(string homeMaxAmount)
	{
		//Debug.Log("Setting Home Max Amount: " + homeMaxAmount);
		this.UIManager.CatalogueViewer.HomeMaxAmount = int.Parse(homeMaxAmount);
	}
	public void SetHomeOnlyFeatured(string homeOnlyFeatured)
	{
		//Debug.Log("Setting Home Only Featured: " + homeOnlyFeatured);
		this.UIManager.CatalogueViewer.showOnlyFeaturedInHome = homeOnlyFeatured.ToLower()=="true";
		
	}
	
	
	
	public void Init()
	{
		//Debug.Log("Initializing");
		Configuration.Endpoint = Endpoint;
		if (UseDebugCatalogue)
		{
			LoadCatalogue(this.TextData);
		}
		else
		{
			//Descarga de catalogo1
			StartCoroutine(DownloadCatalogue(CatalogueURL));
			StartCoroutine(LanguageGet());
			
			
			
		}
	}
	//public CatalogueMaterial Mat;
	public CatalogueItemVariation CItemVar;
	public CatalogueMaterial CMaterial;
	public CatalogueMaterialesSet CMaterialSet;
	
	public Material [] materialsInScene;
	void LoadMaterials()
	{ 
		//Carga de materiales !!!!!!!!!!!!!!!!
		CatalogueParser.DefaultMaterial = DefaultMaterial;
		
		Material[] materialsInScene = DefaultMaterials;
		
		foreach (var mat in materialsInScene)
		{
			if (!materials.ContainsKey(mat.name))
			{
				materials.Add(mat.name, mat);
			}
		}
	}
	//-----------------------------------------------------
	
	public ActionMode lastMode;
	// Update is called once per frame
	void Update()
	{
		
		StartCoroutine(DownloadAssets());
		//Debug.Log(DateTime.Now.TimeOfDay.ToString() + " SceneManager Update");
		if (!RoomSelected && !UIManager.IsSelectingRoom ()) {
			
			
			UIManager.ShowRoomSelector ();
		}
		//full Screen Mode 
		/*	if (Input.GetKeyUp(KeyCode.B))
			       {
            toggleFullScreen();
        }*/
		
		
		
		var ignoreClick = IgnoreGUIClick();
		
		if (!ignoreClick)
		{
			HandleActionMode();
		}
		else
		{
			//Debug.Log("Ignore Click: " + ignoreClick);
		}
		lastMode = Mode;
		
		
		
		// ModelVerification = CurrentCatalogueItem.modelObject;	
	}
	
	private bool IgnoreGUIClick()
	{
		var ignoreClick = false;
		if (eventSystem.currentSelectedGameObject != null)
		{
			return true;
		}
		if (Input.GetMouseButtonDown(0))
		{
			if (eventSystem.IsPointerOverGameObject())
			{
				return true;
			}
			var guimouseRay = this.UICamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
			
			//			Debug.Log("RayCast - SceneManager (9)");
			if (Physics.Raycast(guimouseRay, out uiHitinfo, 100, HudLayer))
			{
				Debug.DrawRay(guimouseRay.origin, guimouseRay.direction, Color.red, 1000);
				ignoreClick = true;
			}
			Debug.DrawRay(guimouseRay.origin, guimouseRay.direction, Color.magenta, 1000);
		}
		return ignoreClick;
	}
	
	private void HandleActionMode()
	{
		if (Mode == ActionMode.None)
		{
			HandleActionNone();
		}
		if (Mode == ActionMode.Rotating)
		{
			HandleActionRotation();
		}
		if (Mode == ActionMode.Moving)
		{
			HandleActionMove();
		}
		if (Mode == ActionMode.Painting)
		{
			HandleActionPaint();
		}
		if (Mode == ActionMode.Adding)
		{
			HandleActionAdd();
		}
	}
	
	
	public void SetCurrentBuildObject(SceneObject sceneObject)
	{
		CurrentBuildObject = sceneObject;
		justSet = true;
	}
	
	private void HandleActionAdd()
	{
		if (CurrentBuildObject == null)
		{
			SetMode(ActionMode.None);
		}
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		
		//	Debug.Log("RayCast - SceneManager (10)");
		//	Debug.DrawRay(ray.origin, ray.direction, Color.yellow, 2f);//Este es el RAY graficado
		if (Physics.Raycast(ray, out hitInfo, 5000, PositionLayer))
		{
			var surface = hitInfo.collider.GetComponent<ExampleDragDropSurface>();
			
			if (surface != null)
			{
				Debug.DrawLine(ray.origin, ray.origin + (ray.direction * 5), Color.green, 2f);
				CurrentBuildObject.SetSelected(true);
				if (CurrentBuildObject.RotationOperation.IsActive) CurrentBuildObject.RotationOperation.Cancel();
				
				CurrentBuildObject.TranslationOperation.StartAndSet(ClampPosition(hitInfo.point,CurrentBuildObject.AlturaModelo));//SMB: Esto es un set de la posicion del objeto interactua en TranslationOperation y sceneObject. ****
				
				
			}
			else
			{
				Debug.DrawLine(ray.origin, ray.origin + (ray.direction * 5), Color.red, 2f);
			}
		}
		else
		{
			Debug.DrawLine(ray.origin, ray.origin + (ray.direction * 5), Color.white, 2f);
		}
		
		if (!justSet && Input.GetMouseButtonUp(0))
		{
			CurrentBuildObject.TranslationOperation.End();
			SetSelected(CurrentBuildObject.gameObject);
			SetCurrentBuildObject(null);
			SetMode(ActionMode.None);
		}
		justSet = false;
	}
	
	
	
	//------------------------------------------------------------------//
	//------------------------------------------------------------------//
	//------------------------------------------------------------------//
	//------------------------------------------------------------------//
	//------------------------------------------------------------------//
	//------------------------------------------------------------------//
	
	public bool ActivaMouse;
	public int NroMatSM;
	public bool SelMat = false;
	//DATos del mueble
	public string nombreMueble;
	public string nombreSet;
	public int NroSetsSelected;
	public string idSelected;
	
	public string[] NombresA;
	
	
	public int SetSeleccionado;
	public GameObject PanelBotonesS;
	
	public void ChangeMaterialsSet(int valor)
	{
		SelectedObject.GetComponent<SceneObject> ().CatMatToUse = valor;
	}
	
	public void SetValuesSetsColores(){
		NroSetsSelected = sceneObject.CatMatMax;
	//	Debug.Log ("NRoSetCreated: " + NroSetsSelected); 
		GameObject BotonSetsSs = GameObject.Find("ScriptSets");
		BotonesSetsColores SceneScriptSs = BotonSetsSs.GetComponent<BotonesSetsColores>();
		SceneScriptSs.GetNroMat();
		
	}
	public GameObject PanelSetsModelo;
	public void PerformMatButtomSet (GameObject GO)
	{
		
		
		
		//PanelSets = GameObject.FindWithTag ("PanelColores");
		
		
		//	if (PanelSetsModelo.activeSelf == true) {
		
		
		//		PanelSetsModelo.SetActive (false);
		//	}
		
		
		foreach(Component child in PanelBotonesS.GetComponentsInChildren<Button>())
		{
			//Debug.Log ("Botones Conteo Delete");
			//Destroy(child);
			DestroyObject(child.gameObject);
		}
		
		nombreMueble = GO.GetComponent<SceneObject>().name;//toma los datos del mueble seleccionado.
		//Debug.Log ("mueble: " + nombreMueble);
		NroSetsSelected = GO.GetComponent<SceneObject>().CatMatMax;
	//	Debug.Log ("NROSETS SM: " + NroSetsSelected); //
		idSelected = GO.GetComponent<SceneObject> ().idItem;
	//	Debug.Log ("IdCreated: " + NroSetsSelected); //
		nombreSet = GO.GetComponent<SceneObject>().NombreSets;
		
		if (GO != null)
		{
			if (GO.GetComponent<SceneObject> ().GetNombresArray () != null)
			{
				NombresA = GO.GetComponent<SceneObject> ().GetNombresArray ();
			}
		}
		
		GameObject BotonSets = GameObject.Find("ScriptSets");
		BotonesSetsColores SceneScript = BotonSets.GetComponent<BotonesSetsColores>();
		
		SceneScript.GetNroMat();
	}
	
	
	//Seleccion de elementos _smb
	private void HandleActionNone()
	{
		
		if (Input.GetMouseButtonDown(0))// si se presiona el boton izquierdo
		{
			var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			//  RaycastHit hitinfo;
			//	Debug.Log("RayCast - SceneManager (11)");
			if (Physics.Raycast(mouseRay, out hitinfo, 100, SelectionLayer))
			{
				//     Debug.Log("Selected!");
				SetSelected(hitinfo.collider.gameObject);
				
				
				nombreMueble = SelectedObject.GetComponent<SceneObject>().name;//toma los datos del mueble seleccionado.
			//	Debug.Log ("mueble: " + nombreMueble);
				NroSetsSelected = SelectedObject.GetComponent<SceneObject>().CatMatMax;
			//	Debug.Log ("NROSETS SM: " + NroSetsSelected); //
				idSelected = SelectedObject.GetComponent<SceneObject> ().idItem;
			//	Debug.Log ("IdCreated: " + NroSetsSelected); //
				nombreSet = SelectedObject.GetComponent<SceneObject>().NombreSets;
				
				
				
				
				//NombresA = new string[SelectedObject.GetComponent<SceneObject>().GetNombresArray().Length];//.NombresArray.Length];
				if (SelectedObject != null)
				{
					if (SelectedObject.GetComponent<SceneObject> ().GetNombresArray () != null)
					{
						NombresA = SelectedObject.GetComponent<SceneObject> ().GetNombresArray ();
					}
				}
				
				//for(int i=0; i<NombresA.Length;i++){
				//	NombresA[i] = new string();
				//	NombresA[i] = SelectedObject.GetComponent<SceneObject>().NombresArray[i];
				//}
				
				
				GameObject BotonSets = GameObject.Find("ScriptSets");
				BotonesSetsColores SceneScript = BotonSets.GetComponent<BotonesSetsColores>();
				
				SceneScript.GetNroMat();
				//SceneScript.ChangeSet();
				
			}
			else
			{
				//       Debug.Log("Nothing Selected!");
				SetSelected(null);
			}
			
		}
		else if (Input.GetMouseButtonDown(1))
		{
			SetSelected(null);
		}
		
		//------------------------------------------------------------------//
		//------------------------------------------------------------------//
		//movimiento de elementos -smb
		
		if (Input.GetMouseButton(0) && SelectedObject != null && Mode == ActionMode.None)
		{
			
			
			if (MovActivo == true)//sofia- Activo el movimiento para que solamente funcione si la rotacion esta inactiva
			{
				var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				
				//	Debug.Log("RayCast - SceneManager (12)");
				
				
				
				ActivaMouse = true;
				//Debug.Log("RayCast - SceneManager (12-A)");
				if (Physics.Raycast(mouseRay, out hitinfo, 100, SelectionLayer))
				{
					if (Mode == ActionMode.None && hitinfo.collider.gameObject == SelectedObject)
					{
						pressedCounter += Time.deltaTime;
						if (pressedCounter > amountOfTimeToMove)
						{
							pressedCounter = 0f;
							SetMode(ActionMode.Moving);
							so = SelectedObject.GetComponent<SceneObject>();
							so.RotationOperation.Cancel();
							SendMessage("UpdateView");
						}
					}
					else
					{
						pressedCounter = 0f;
					}
				}
				else
				{
					pressedCounter = 0f;
				}
			}
		}
		else
		{
			pressedCounter = 0f;
			//	Debug.Log("RayCast - SceneManager (12-B)");
			//aca va la variable que activa la posicion valida para luego activar la colision.
			ActivaMouse = false;
		}
	}
	
	//------------------------------------------------------------------//
	
	//Seleccion de rotacion - smb
	private void HandleActionRotation()
	{
		if (SelectedObject != null)
		{
			so = SelectedObject.GetComponent<SceneObject>();
			if (so.TranslationOperation.IsActive)
			{
				so.TranslationOperation.Cancel();
			}
			if (Input.GetMouseButton(1))
			{
				so.RotationOperation.Cancel();
				SetSelected(null);
				SetMode(ActionMode.None);
				SendMessage("UpdateView");
			}
			else if (Input.GetMouseButtonDown(0) & !so.RotationOperation.IsActive)
			{
				so.RotationOperation.Start();
			}
			else if (Input.GetMouseButtonUp(0) & so.RotationOperation.IsActive)
			{
				so.RotationOperation.Finish();
				SetMode(ActionMode.None);
			}
			if (Input.GetMouseButton(0) && so.RotationOperation.IsActive)
			{
				var y = Input.GetAxis("Mouse X") * -1;
				oldRotation = SelectedObject.transform.rotation;
				SelectedObject.transform.Rotate(0, y * Time.deltaTime * RotationSpeed, 0);
				if (oldRotation != SelectedObject.transform.rotation)
				{
					so.UpdateBounds();
				}
			}
		}
	}
	
	//------------------------------------------------------------------//
	
	//Seleccion de pintar - smb
	
	private void HandleActionPaint()
	{
		if (Input.GetMouseButtonDown(1))
		{
			SetModeNone();
		}
	}
	
	
	//------------------------------------------------------------------//
	
	//Seleccion de Movimiento- smb
	
	
	private void HandleActionMove()
	{
		if (SelectedObject != null)
		{
			SelectedObject.GetComponent<TranslationHandler>().TranslationEnabled = true;
			//SelectedObject.GetComponent<TranslationHandler>().IsFristMousePositionMouse = true;//SMB: Activa esto cada vez que se cambia la objeto y setea el primer valor de la posicion del mouse para que se tome cottectamente esto. ****
		}
		else
		{
			//   Debug.Log("Not selected");
		}
	}
	
	
	public void SetSelected(GameObject go)
	{
		if (SelectedObject != null && SelectedObject.GetComponent<SceneObject>() != null)
		{
			SelectedObject.GetComponent<SceneObject>().SetSelected(false);
			//Camera3D.GetComponent<MouseOrbitOnDemand>().target = Room.transform.FindChild("Center");
		}
		SelectedObject = go;
		UIManager.SetSelected(SelectedObject);//Muestra/activa panel de modelo
		
		if (SelectedObject != null && SelectedObject.GetComponent<SceneObject>() != null)
		{
			
			SelectedObject.GetComponent<SceneObject>().SetSelected(true);
			//Camera3D.GetComponent<MouseOrbitOnDemand>().target = SelectedObject.transform;
		}
		
	}
	
	//------------------------------------------------------------------//
	
	//funcion de FullScreen (pantalla completa) de la aplicacion.- smb
	public void toggleFullScreen()
	{
		//Debug.Log(string.Format("{0}x{1}", Screen.currentResolution.width, Screen.currentResolution.height));
		if (Screen.fullScreen)
		{
			// Set your windowed resolution to whatever you want here.
			Screen.SetResolution(760, 600, false);
		}
		else
		{
			// Switch to the desktop resolution in fullscreen mode.
			Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
		}
		// Debug.Log(string.Format("{0}x{1}", Screen.currentResolution.width, Screen.currentResolution.height));
	}
	
	//------------------------------------------------------------------//
	public GameObject Language;
	public Text idiomadebug;
	public GameObject CamLang;
	//Idioma
	IEnumerator LanguageGet(){
		//TEST//string urltest = "http://test.decorar3d.com/index.php/api/jsondev";
		//TEST//WWW lanwww = new WWW(urltest);

		WWW lanwww = new WWW(CatalogueURL);

		yield return lanwww;
		Debug.Log ("lanwww" + lanwww.text);

		var M = JSON.Parse (lanwww.text);

		string CutLang = lanwww.text;
		CutLang = "{"+CutLang.Substring (lanwww.text.Length - (lanwww.text.Length - lanwww.text.IndexOf ("\"usuario\": {")));
		Debug.Log ("SubString 1 = "+CutLang);
		JsonData LangData = JsonMapper.ToObject(CutLang);
		string idioma = LangData ["usuario"] ["lenguaje"].ToString();
	
			//AuxValor = M ["mensaje"][0]["valor"];
		//	string idioma = M ["usuario"][0]["lenguaje"];
			//idiomadebug.text = idioma;
		Debug.Log ("idioma" + idioma);


			switch (idioma){
			case "es":
				//Language.gameObject.GetComponent<ChangeLang>().ChangeL("es");
			CamLang.GetComponent<ChangeLang>().ChangeL("es");

			Debug.Log ("idioma" + idioma + "DONE!!");
				break;

			case "en":
			//	Language.gameObject.GetComponent<ChangeLang>().ChangeL("en");
			CamLang.GetComponent<ChangeLang>().ChangeL("en-US");
			Debug.Log ("idioma" + idioma + "DONE!!");
				break;
			case "pt":
				//Language.gameObject.GetComponent<ChangeLang>().ChangeL("pt");
			CamLang.GetComponent<ChangeLang>().ChangeL("pt");
			Debug.Log ("idioma" + idioma + "DONE!!");
				break;
			}

		}

	//Descarga de Catalogo- smb
	
public	IEnumerator DownloadCatalogue(string catalogueUrl)
	{
		//catalogueUrl = "http://test.decorar3d.com/index.php/api/wallpainting/z/qg==/c/619";//SMB: To Test Hardcoding!!
	//	catalogueUrl = "http://sistema.decorar3d.com/index.php/api/categorias/z/qg==";

		noCache = catalogueUrl + (CatalogueURL.IndexOf("?") == -1 ? "?" : "&") + "time=" + DateTime.Now.Ticks.ToString();
		noCache = catalogueUrl;
		www = new WWW(noCache);
		
		WindowManager.Instance.ShowProgress("CatalogueDownload", www.progress);
		Debug.Log ("Descarga URL!!");
		//Load the data and yield (wait) till it's ready before we continue executing the rest of this method.
		yield return www;
		
		
		
		
		if (www.error == null) {
			WindowManager.Instance.HideProgress ("CatalogueDownload");
			//Sucessfully loaded the JSON string
			//	Debug.Log (string.Format ("Loaded from {0} following JSON string: {1}", noCache, www.text));
			
			//Process books found in JSON file
			LoadCatalogue (www.text);

		
		


			
			
		} else {
			WindowManager.Instance.HideProgress ("CatalogueDownload");
			WindowManager.Instance.ShowAlert ("CatalogueDownloadError", "Error al descargar catalogo", www.error);
			//		Debug.Log (string.Concat ("ERROR: ", noCache, " - ", www.error));
		}
		
	}
	public	IEnumerator DownloadCatalogue2(string catalogueUrl)
	{
		//catalogueUrl = "http://test.decorar3d.com/index.php/api/wallpainting/z/qg==/c/619";//SMB: To Test Hardcoding!!
		//	catalogueUrl = "http://sistema.decorar3d.com/index.php/api/categorias/z/qg==";
		

		noCache = catalogueUrl;
		www = new WWW(noCache);
		

		Debug.Log ("Descarga URL!!");
		//Load the data and yield (wait) till it's ready before we continue executing the rest of this method.
		yield return www;
		LoadCatalogue (www.text);

			
			
			
			
			
			
			

		
	}


	string TemPathURL;

	public void DownloadCatalogueObjects(string catalogueUrlnum)
	{
		TemPathURL = catalogueUrlnum;

		Debug.Log ("TemPathURL=" + TemPathURL);
		//string jsontext = new ;
		//LoadCatalogue(StartCoroutine(DownloadObjects(catalogueUrlnum)));
	

		StartCoroutine(DownloadObjects());



	}


	public IEnumerator DownloadObjects()
	{
		Debug.Log ("DownloadCatalogueObjects "+TemPathURL);
		WWW Objwww = new WWW(TemPathURL);
	


		yield return Objwww;
		Debug.Log ("lanwww" + Objwww.text);
		LoadCatalogue2(Objwww.text);


		
	}

	//------------------------------------------------------------------//
	//Idiomas - language



	//------------------------------------------------------------------//
	
	//Carga de catalogo- smb
	public void LoadCatalogue(string catalogueData)
	{
		Debug.Log("LoadCatalogue ("+catalogueData+")");
		CatalogueParser.FeaturedPropertyName = this.FeaturedPropertyName;
		this.Catalogue = CatalogueParser.Parse(catalogueData);
		if (this.CatalogueUpdated != null)
		{
			this.CatalogueUpdated();
		}
	}
	public Catalogue Catalogue2;
public	Action CatalogueUpdated2;
	public void LoadCatalogue2(string catalogueData)
	{
		Debug.Log("LoadCatalogue ("+catalogueData+")");
		CatalogueParser.FeaturedPropertyName = this.FeaturedPropertyName;
		this.Catalogue2 = CatalogueParser.Parse2(catalogueData);
		if (this.CatalogueUpdated2 != null)
		{
			this.CatalogueUpdated2();
		}
	}
	
	//------------------------------------------------------------------//
	//public var collisionDetectionMode: CollisionDetectionMode; 
	//Asignacion de objetos de catalogo a objetos que se presentaran en la escena
	//***
	public SceneObject CreateSceneObject(CatalogueItem item)
	{
		//Crea el objeto/modelo y lo asigna dentro del elemento Item
		copy = Instantiate(item.modelObject) as GameObject;
		copy.transform.parent = this.ItemContainer;
		copy.tag = "Object";
		copy.layer = LayerMask.NameToLayer(ItemsLayerName);
		ApplyCatalogueToObject(item, copy);
		
		sceneObject = copy.AddComponent<SceneObject>();
		sceneObject.Item = item;
		/**/ sceneObject.CurrentVariation = item.variations[0];
		
		
		translationHandler = copy.AddComponent<TranslationHandler>();
		rotationHandler = copy.AddComponent<RotationHandler>();
		
		sceneObject.Init();
		sceneObject.ApplyVariation();
		sceneObject.transform.Rotate (0, -180, 0);
		/*
        DimBoxes_DimBox dimbox = copy.AddComponent<DimBoxes_DimBox>();
        dimbox.enabled = false;
        dimbox.widthPlacement = w_placement.top_front;
        dimbox.heightPlacement = h_placement.front_left;
        dimbox.depthPlacement = d_placement.top_left;
        dimbox.extensions = new bool_xyz();
        dimbox.extensions.depth = true;
        dimbox.extensions.height = true;
        dimbox.extensions.width = true;
        dimbox.faceCamera = new bool_xyz();
        dimbox.faceCamera.depth = true;
        dimbox.faceCamera.width = true;
        dimbox.faceCamera.height = true;
        dimbox.letterMaterial = DimensionLetterMaterial;
        dimbox.Font1 = DimensionFont;
        dimbox.DoStartup(true);*/
		sceneObject.UpdateInfo();
		
		
		copy.AddComponent<Rigidbody>().isKinematic = true;
		//copy.AddComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic; 
		copy.SetActive(true);
		LoadedObjects.Add(sceneObject);
		
		
		
		
		//NombresA = new string[SelectedObject.GetComponent<SceneObject>().GetNombresArray().Length];//.NombresArray.Length];
		if (SelectedObject != null)
		{
			if (SelectedObject.GetComponent<SceneObject> ().GetNombresArray () != null)
			{
				NombresA = SelectedObject.GetComponent<SceneObject> ().GetNombresArray ();
			}
		}
		
		
		nombreMueble = copy.name;//toma los datos del mueble seleccionado.
	//	Debug.Log ("muebleCreated: " + nombreMueble);
		
		
		NroSetsSelected = sceneObject.CatMatMax;
	//	Debug.Log ("NRoSetCreated: " + NroSetsSelected); //
		
		
		idSelected = sceneObject.idItem;
	//	Debug.Log ("IdCreated: " + NroSetsSelected); //
		
		GameObject BotonSetsS = GameObject.Find("ScriptSets");
		BotonesSetsColores SceneScriptS = BotonSetsS.GetComponent<BotonesSetsColores>();
		SceneScriptS.GetNroMat();
		
		/*	GameObject BotonSets = GameObject.Find("ScriptSets");
		BotonesSetsColores SceneScript = BotonSets.GetComponent<BotonesSetsColores>();
		SceneScript.GetNroMat();*/
		
		
		return sceneObject;
	}
	
	
	//------------------------------------------------------------------//
	
	//Setea el cursor en base al modo de accion.
	public void SetMode(ActionMode mode)
	{
		
		Mode = mode;
		if (Mode == ActionMode.None)
		{
			this.SetCursor(Cursors.None);
		}
		else if (Mode == ActionMode.Adding)
		{
			this.SetCursor(Cursors.Add);
		}
		else if (Mode == ActionMode.Rotating)
		{
			this.SetCursor(Cursors.Rotate);
		}
		else if (Mode == ActionMode.Moving)
		{
			this.SetCursor(Cursors.Move);
		}
		else if (Mode == ActionMode.Painting)
		{
			this.SetCursor(Cursors.Painting);
		}
	}
	public void SetModeMove()
	{
		SetMode(ActionMode.Moving);
	}
	public void SetModeRotate()
	{
		SetMode(ActionMode.Rotating);
	}
	internal void SetModePainting()
	{
		SetMode(ActionMode.Painting);
	}
	internal void SetModeNone()
	{
		SetMode(ActionMode.None);
	}
	
	
	//------------------------------------------------------------------//
	
	//Crea diccionario de los materiales de los objetos- smb
	private Dictionary<string, Material> materials = new Dictionary<string, Material>();
	public Material GetMaterial(string name)
	{
		return !string.IsNullOrEmpty(name) && materials.ContainsKey(name) ? materials[name] : DefaultMaterial;
	}
	
	public Dictionary<String,List<GameObject>> AssetsSuscribers = new Dictionary<string, List<GameObject>>();

		public void QueueForDownload(GameObject suscriber, AssetInfo[] assets)
	{
		//if (CountDownload = false) {
		for (int i = 0; i < assets.Length; i++) {
			assetInfo = assets [i];
			path = assetInfo.Path.Trim ();
			if (!AssetsSuscribers.ContainsKey (path)) {
				AssetsSuscribers.Add (path, new List<GameObject> () { suscriber });
			} else if (!AssetsSuscribers [path].Contains (suscriber)) {
				AssetsSuscribers [path].Add (suscriber);
			}
			if (assetInfo.Type == AssetInfo.AssetType.Thumbnail) {
				lock (Sprites) {
					if (!Sprites.ContainsKey (path) && !AssetsQueue.Contains (assetInfo) && !DownloadingQueue.Contains (path)) {
						AssetsQueue.Enqueue (assetInfo);
					}
				}
			} else {
				lock (Textures) {
					if (!Textures.ContainsKey (path) && !AssetsQueue.Contains (assetInfo) && !DownloadingQueue.Contains (path)) {
						AssetsQueue.Enqueue (assetInfo);
					}
				}
			}
				/*if (i == assets.Length) {
										CountDownload = true;
								}*/
	}
			//}
	}



/*	public void QueueForDownload(GameObject suscriber, AssetInfo[] assets)
	{
		//if (CountDownload = false) {
		for (int i = 0; i < assets.Length; i++) {
			assetInfo = assets [i];
			path = assetInfo.Path.Trim ();
			if (!AssetsSuscribers.ContainsKey (path)) {
				AssetsSuscribers.Add (path, new List<GameObject> () { suscriber });
			} else if (!AssetsSuscribers [path].Contains (suscriber)) {
				AssetsSuscribers [path].Add (suscriber);
			}
			if (assetInfo.Type == AssetInfo.AssetType.Thumbnail) {
				lock (Sprites) {
					if (!Sprites.ContainsKey (path) && !AssetsQueue.Contains (assetInfo) && !DownloadingQueue.Contains (path)) {
						AssetsQueue.Enqueue (assetInfo);
					}
				}
			} else {
				lock (Textures) {
					if (!Textures.ContainsKey (path) && !AssetsQueue.Contains (assetInfo) && !DownloadingQueue.Contains (path)) {
						AssetsQueue.Enqueue (assetInfo);
					}
				}
			}
			/*	if (i == assets.Length) {
										CountDownload = true;
								}*/
		/*}
		//	}
	}*/
	
	
	//------------------------------------------------------------------//
	
	//**//
	//Descarga de los Thumbnails desde la base de datos- smb
	//Descarga de los Thumbnails desde la base de datos- smb
	int freeAmount = 3;
	public Shader GhostShader;
	private IEnumerator DownloadAssets()
	{
		Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();
		Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
		while (freeAmount > 0 && AssetsQueue.Count() > 0)
		{
			if (freeAmount > 0)
			{
				freeAmount--;
				
				
				AssetInfo asset = AssetsQueue.Dequeue();
				DownloadingQueue.Add(asset.Path);
				yield return StartCoroutine(GetWWWFiles(asset, textures, sprites));
				freeAmount++;
				if (textures.ContainsKey(asset.Path) && !Textures.ContainsKey(asset.Path))
				{
					Textures.Add(asset.Path, textures[asset.Path]);
					KeyValuePair<string, Texture2D> texture = textures.FirstOrDefault(a=>a.Key==asset.Path);
					if (AssetsSuscribers.ContainsKey(asset.Path))
					{
						for (int i = 0; i < AssetsSuscribers[asset.Path].Count; i++)
						{
							//							/**/Debug.Log("Notifying AssetsDownloaded" + asset.Path, AssetsSuscribers[asset.Path][i]);
							AssetsSuscribers[asset.Path][i].SendMessage("AssetsDownloaded", texture, SendMessageOptions.DontRequireReceiver);
							//							/**/Debug.Log("Notifying TextureDownloaded: " + asset.Path, AssetsSuscribers[asset.Path][i]);
							AssetsSuscribers[asset.Path][i].SendMessage("TextureDownloaded", texture, SendMessageOptions.DontRequireReceiver);
						}
					}
					else
					{
					//	Debug.Log("Broadcasting AssetsDownloaded: " + asset.Path, this);
						Room.BroadcastMessage("AssetsDownloaded", texture, SendMessageOptions.DontRequireReceiver);
					//	Debug.Log("Broadcasting TextureDownloaded: " + asset.Path, this);
						this.UIRoot.BroadcastMessage("TextureDownloaded", texture, SendMessageOptions.DontRequireReceiver);
					}
				}
				if (asset.Type == AssetInfo.AssetType.Thumbnail && sprites.ContainsKey(asset.Path) && !Sprites.ContainsKey(asset.Path))
				{
					Sprites.Add(asset.Path, sprites[asset.Path]);
					KeyValuePair<string, Sprite> sprite = sprites.FirstOrDefault(a => a.Key == asset.Path);
					if (AssetsSuscribers.ContainsKey(asset.Path))
					{
						for (int i = 0; i < AssetsSuscribers[asset.Path].Count; i++)
						{
							//Debug.Log("Notifying SpriteDownloaded: " + asset.Path, AssetsSuscribers[asset.Path][i]);
							AssetsSuscribers[asset.Path][i].SendMessage("SpriteDownloaded", sprite, SendMessageOptions.DontRequireReceiver);
						}
					}
					else
					{
					//	Debug.Log("Broadcasting SpriteDownloaded: " + asset.Path, this);
						//Room.BroadcastMessage("AssetsDownloaded", sprites.FirstOrDefault(a => a.Key == asset.Path), SendMessageOptions.DontRequireReceiver);
						this.UIRoot.BroadcastMessage("SpriteDownloaded", sprite, SendMessageOptions.DontRequireReceiver);
					}
				}
				else if ((textures.ContainsKey(asset.Path) && !Textures.ContainsKey(asset.Path)) || (sprites.ContainsKey(asset.Path) && !sprites.ContainsKey(asset.Path)))
				{
				//	Debug.Log("Not Broadcasting " + asset.Type + " downloaded... ");
				}
				else
				{
					
				}
				DownloadingQueue.Remove(asset.Path);
			}
		}
	}
	private IEnumerator GetWWWFiles(AssetInfo asset, Dictionary<string, Texture2D> textures, Dictionary<string, Sprite> sprites)
	{
		
		
		//						Debug.Log (string.Format ("Start Downloading asset: {0}", asset.Path));
		
		if (asset.Path != "" && !Textures.ContainsKey (asset.Path)) {
			var www = new WWW (asset.Path);
			
			while (!www.isDone) {
				//	Debug.Log (string.Format ("Downloading asset: ({1}) {0}", asset.Path, www.progress * 100));
				//objData.SetProgress (progress + (www.progress / numberOfTextures) * .5f);
				yield return null;
			}
			
			isDone = true;
			if (www.error != null) {
				//	Debug.LogError ("Error loading " + asset.Path + ": " + www.error);
				hasError = true;
				yield break;
			}
			if (asset.Type == AssetInfo.AssetType.Texture) {
				loadedTexture = new Texture2D (4, 4);
				www.LoadImageIntoTexture (loadedTexture);
				textures.Add (asset.Path, loadedTexture);
			} else if (asset.Type == AssetInfo.AssetType.Thumbnail) {
				textures.Add (asset.Path, www.texture);
				loadedSprite = Sprite.Create (www.texture, new Rect (0, 0, www.texture.width, www.texture.height), new Vector2 (0, 0));
				sprites.Add (asset.Path, loadedSprite);
			} else {
				//Debug.LogError (string.Format ("Asset type not recognized: {0} - {1}", asset.Type, asset.Path));
			}
			
			
		}
		
		//Debug.Log(string.Format("End Downloading asset ({1}) {{{2}-{3}}}: {0}", asset.Path, asset.Type, hasError, isDone));
		//	Debug.Log("fin");
		
	}
	
	
	
	
	public Texture GetTexture(GameObject suscriber, string path)
	{
		if (!string.IsNullOrEmpty(path))
		{
			assetIsReady = Textures.ContainsKey(path);
			//Debug.Log(string.Format("Requested texture for {0}. Exists: {1}", path, assetIsReady));
			
			if (assetIsReady)
			{
				return Textures[path];
			}
			else
			{
				isInDownloadingQueue = DownloadingQueue.Exists((q) => q == path);
				isAssetInQueue = AssetsQueue.Any(a => a.Path == path);
				//Debug.Log(string.Concat("Asset isn't ready. IsDownloading: ", isInDownloadingQueue, ". Is Asset In Queue: ", isAssetInQueue), this);
				if (!isAssetInQueue)
				{
					QueueForDownload(suscriber, new AssetInfo[1] { new AssetInfo(AssetInfo.AssetType.Texture, path) });
				}
			}
		}
		return null;
	}
	
	
	public Sprite GetSprite(string path)
	{
		if (!string.IsNullOrEmpty(path))
		{
			assetIsReady = Sprites.ContainsKey(path);
			//Debug.Log(string.Format("Requested sprite for {0}. Exists: {1}", path, assetIsReady));
			
			if (assetIsReady)
			{
				return Sprites[path];
			}
		}
		return null;
	}
	public PhysicMaterial MuebleIN;
	
	//aca se aplican los valores que se encuentran en el catalogo a ITEM- smb
	public void ApplyCatalogueToObject(CatalogueItem item, GameObject gameObject)
	{
		gameObject.name = item.name;// se traspasa el nombre
		gameObject.transform.localScale = item.scale;// se traspasa la escala
		gameObject.layer = this.ItemContainer.gameObject.layer;// se traspasa el layer
		if (gameObject.GetComponent<Collider>() == null)// se corrobora si la colision es nula
		{
			
			
		}
	}
	
	
	//*** Variation Materials
	public void ApplyVariationToObject(CatalogueItemVariation variation, GameObject gameObject)
	{
		if (variation.plugins.Count > 0)
		{
			foreach (var pluginInfo in variation.plugins)
			{
				Assets.Scripts.Plugins.PluginResolver.GetBuilder(pluginInfo.PluginName).Build(gameObject.GetComponent<SceneObject>(), pluginInfo);
			}
		}
		if (variation.Setmaterials.Count > 0)
		{
			List<string> texturePaths = new List<string>();
			Material[] materials = new Material[gameObject.GetComponent<Renderer>().materials.Length];
			
			for (int i = 0; i < variation.Setmaterials.Count; i++)//Son los sets de materiales
			{
				if (i < materials.Length)
				{
					if (!string.IsNullOrEmpty(variation.Setmaterials[i].name))
					{
						materials[i] = GetMaterial(variation.Setmaterials[i].name);
					}
					else
					{
						materials[i] = gameObject.GetComponent<Renderer>().materials[i];
					}
				}
				
			}
			gameObject.GetComponent<Renderer>().materials = materials;
			
			/*for (int i = 0; i < variation.Setmaterials.Count; i++)
            {
                if (i < materials.Length)
                {
                  //  if (variation.Setmaterials[i].colorSet.HasValue)
					if (variation.Setmaterials[i].materialesInternos[i].colorSet.HasValue)
					
                    {
                        //gameObject.GetComponent<Renderer>().materials[i].color = variation.Setmaterials[i].colorSet.Value;
						gameObject.GetComponent<Renderer>().materials[i].color = variation.Setmaterials[i].materialesInternos[i].colorSet.Value;
                    }
					if (!string.IsNullOrEmpty(variation.Setmaterials[i].texture))
                    {
						var texture = GetTexture(gameObject, variation.Setmaterials[i].texture);
                        if (texture != null)
                        {
                            gameObject.GetComponent<Renderer>().materials[i].mainTexture = texture;
                        }
                        else
                        {
							texturePaths.Add(variation.Setmaterials[i].texture);
                        }
                    }
                }
            }*/
			
			if (texturePaths.Count > 0)
			{
				QueueForDownload(gameObject, texturePaths.Select(tp => new AssetInfo(AssetInfo.AssetType.Texture, tp.IndexOf("http") > -1 ? tp : Configuration.Endpoint + tp)).ToArray());
			}
		}
	}
	
	
	internal void SetCursor(Cursors cursor)
	{
		Texture2D texture = null;
		switch (cursor)
		{
		case Cursors.None:
			break;
		case Cursors.Move:
			texture = CursorMoving;
			break;
		case Cursors.Rotate:
			texture = CursorRotating;
			break;
		case Cursors.Pan:
			texture = CursorPanning;
			break;
		case Cursors.Add:
			texture = CursorPlacing;
			break;
		case Cursors.Painting:
			texture = CursorPainting;
			break;
		case Cursors.Remove:
			//texture = CursorRemove;
			break;
		default:
			break;
		}
		Cursor.SetCursor(texture, new Vector2(), CursorMode.Auto);
	}
	
	
	
	internal void SetWallTexture(CatalogueWallTexture wallTexture)
	{
		SetWallTexture(wallTexture.materialName, wallTexture.texture);
	}
	
	
	internal void SetWallTexture(string materialName, string textureName)
	{
	//	Debug.Log(string.Concat("Setting wall type. Material: ", materialName, ". Texture: ", textureName));
		walls = GameObject.FindObjectsOfType<Wall>().ToList();
		Material  material = GetMaterial(materialName);
		
		foreach (var wall in walls)
		{
			Texture texture = GetTexture(wall.gameObject, textureName);
			/* if (texture != null)
            {
                wall.SetTexture(material, texture);//esta es la asignacion concreta de la pared, l cual se deberia realizar en el primer intento y no en el segundo
				Debug.Log("Setting floor texture!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				PisosPinturas pisospinturas = GetComponent<PisosPinturas>();
				pisospinturas.SetNombresPintura(materialName);
				//pisospinturas.SetNombresPiso(Item.name);
            }*/
			if (texture != null)
			{
				wall.SetTexture(material, texture);//original
				GameObject pisospinturas = GameObject.Find("infoPisoPintura");
				
				
				pisospinturas.GetComponent<PisosPinturas>().SetNombresPintura(materialName);
				
			}
		}
	}
	
	public string FloorNombre;
	public string WallNombre;
	
	internal void SetFloorTexture(string materialName, string textureName, string FloorCategory)
	{
		
	//	Debug.Log(string.Concat("Setting floor type. Material: ", materialName, ". Texture: ", textureName));
		List<Floor> floors = GameObject.FindObjectsOfType<Floor>().ToList();
		
		//variationAux.Setmaterials[0].name;
		Material material = GetMaterial(materialName);
		
		foreach (var floor in floors)
		{
			Texture  texture = GetTexture(floor.gameObject, textureName);
			if (texture != null)
			{
				
				floor.SetTexture(material, texture);//esta es la asignacion concreta del piso, l cual se deberia realizar en el primer intento y no en el segundo
			//	Debug.Log("Setting floor texture!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				GameObject pisospinturas = GameObject.Find("infoPisoPintura");
				pisospinturas.GetComponent<PisosPinturas>().SetNombresPiso(materialName);
				//string Fcat = floor.gameObject.name;
			
				pisospinturas.GetComponent<PisosPinturas>().SetCategoryPiso(FloorCategory);
				
				
				
			}
		}
	}
	
	
	//------------------------------------------------------------------//
	
	//Inicio de Botones del panel del modelo en escena- smb
	//boton eliminar modelo
	public void DeleteSelected()// Con esta funcion se elimina el objecto- smb
	{
		if (SelectedObject != null)//si el objeto es seleccionado- smb
		{
			so = SelectedObject.GetComponent<SceneObject>();//reconoce el objeto seleccionado dentro de la escena
			
			AssetsSuscribers.Values.ToList().ForEach(v => v.Remove(SelectedObject));
			GameObject.Destroy(SelectedObject);
			SetSelected(null);
			InfoActivo = false;
			
		}
	}
	
	
	
	public void DeleteAll()// Con esta funcion se elimina el objecto- smb
	{
		AssetsSuscribers.Clear ();
		SetSelected(null);
		InfoActivo = false;
		foreach (Transform child in GameObject.FindWithTag("Items").transform) {
			child.GetComponent<SceneObject> ().BorrarSceneObject ();
		}
	}
	public GameObject ReinicioBox;
	public GameObject ReinicioL;
	public GameObject ReinicioT;
	public Texture WallOrigin;
	public Texture FloorOrigin;
	public GameObject WallsS;
	public GameObject RoomBuilder;
	RoomSelectButton resetButton;
	
	public void DeleteAllScene()// Con esta funcion se elimina todo- smb
	{
		AssetsSuscribers.Clear ();
		SetSelected(null);
		InfoActivo = false;
		foreach (Transform child in GameObject.FindWithTag("Items").transform) {
			child.GetComponent<SceneObject> ().BorrarSceneObject ();
		}
		//BoxRoom
		ReinicioBox.GetComponent<BoxRoom> ().WallHeight = 3;
		ReinicioBox.GetComponent<BoxRoom> ().TopHallLength = 4;
		ReinicioBox.GetComponent<BoxRoom> ().DownHallLength = 4;
		//LRoom
		
		ReinicioL.GetComponent<LRoom> ().WallHeight = 3;
		ReinicioL.GetComponent<LRoom> ().TopHallLength = 4;
		ReinicioL.GetComponent<LRoom> ().DownHallLength = 4;
		ReinicioL.GetComponent<LRoom> ().RightLength = 2;
		ReinicioL.GetComponent<LRoom> ().DownLength = 3;
		//TRoom
		ReinicioT.GetComponent<TRoom> ().WallHeight = 3;
		ReinicioT.GetComponent<TRoom> ().TopHallLength = 4;
		ReinicioT.GetComponent<TRoom> ().DownHallLength = 4;
		ReinicioT.GetComponent<TRoom> ().RightLength = 2;
		ReinicioT.GetComponent<TRoom> ().DownLength = 3;
		//
		
		RoomSelected = false;
		UIManager.ShowRoomSelector ();
		
		
		//RoomBuilder.SetActive(true);
		//RoomBuilder.GetComponent<RoomSelector> ().Reset2();
		
		//resetButton.ResetSelectButton ();
		
		//Reinicio Textura Pared
		//GetComponent<Renderer>().materials[0].mainTexture = texture;
		
		//	WallsS.GetComponentsInChildren<>
		//.GetComponent<Renderer> ().materials [0].mainTexture = FloorOrigin;
	}
	
	
	//sofia- Activo rotacion cuando el objeto es seleccionado- smb
	//boton rotacion
	public void RotationSelected()
	{
		if (SelectedObject != null)
		{
			so = SelectedObject.GetComponent<SceneObject>();
			RotacionActiva = true;// rotacion activa
			MovActivo = false;// movimiento inactivo
			this.GetComponent<UIManager>().Rotator.SetActive(true);//sofi
			InfoActivo = false;
		}
	}
	//sofia
	
	
	//sofia- Activo boton "informacion"- smb
	//boton para mover
	
	public void infoSelected()
	{
		if (SelectedObject != null)
		{	
			MovActivo = false;// movimiento inactivo
			RotacionActiva = false;// rotacion activa
			
			
			
			
		}
	}
	
	
	//sofia- Activo movimiento cuando el objeto es seleccionado- smb
	//boton para mover
	
	public void MoveSelected()
	{
		if (SelectedObject != null)
		{
			so = SelectedObject.GetComponent<SceneObject>();
			MovActivo = true;//movimiento activo
			//SMB: Aca setea la posicion primaria del mouse para mover?
			RotacionActiva =false;// rotacion inactiva
			this.GetComponent<UIManager>().Rotator.SetActive(false);//sofi
			InfoActivo = false;
			
			
		}
	}
	//------------------------------------------------------------------//
	//Boton full screen -smb
	public void FullScreenBoton(){
		if (SelectedObject != null) {
			so = SelectedObject.GetComponent<SceneObject>();
			toggleFullScreen();
		}
		
		
	}
	//FullScreenActivo- smb
	//sofia
	
	//Fin de Botones del panel del modelo en escena
	
	//------------------------------------------------------------------//
	
	//Inicio de boton para reiniciar la aplicacion- smb
	//public RoomSelector RoomSelectorUse;
	public void ReinicioBoton(){
		//<a href="/decorar-3d">
		
		if (SelectedObject != null) {
			
			so = SelectedObject.GetComponent<SceneObject>();
			ReinicioActivo = true;
			Application.OpenURL("http://sistema.decorar3d.com/decorar-3d");
			
		}
		
		//Application.LoadLevel(1);
		//Awake();
		//Start ();
		//	LoadConfiguration ();
		
		//this.RoomSelector.Reset ();
		//GameObject.FindGameObjectWithTag ("RoomBuilder").GetComponent<RoomSelector>().Reset();
		//RoomSelectorUse.Reset();
		
		
		
	}
	
	
	
	// fin de boton para reiniciar la aplicacion- smb
	//------------------------------------------------------------------//
	
	
	//Inicio de captura de pantalla-fotografia- smb
	private string _data = string.Empty;
	public Texture2D bg;
	public float MinFurniturePosition = 0f ; //0f default Posicion en Y del objeto Mueble
	
	
	public string ItemsLayerName;
	public string FeaturedPropertyName = "Featured";
	
	//---screenshot smb
	public string imageName = "Screenshot_";
	public string customPath = "";/*"C:/Users/default/Desktop/UnityScreenshots/";*/ // leave blank for project file location
	//public string posturl = "http://test.decorar3d.com/index.php/uploads/presupuestos";
	public bool resetIndex = false;
	private int indexShot = 0;
	public int resolution = 3; // 1= default, 2= 2x default, etc. Full Screen
	//----
	//captura de pantalla con boton photo
	public void TakeScreenshot()
	{
		StartCoroutine(CaptureAndSave());
		
		
	}
	
	//Captura de pantalla
	IEnumerator CaptureAndSave()
	{
		yield return new WaitForEndOfFrame();
		//------------------------------------------
		//var tarWidth = Mathf.Min(Screen.width, bg.width);//SMB: Esto setea el ancho de la pantalla comparado con otro valor de "bg"
		//var tarHeight = Mathf.Min(Screen.height, bg.height);//SMB: Esto setea el alto de la pantalla comparado con otro valor de "bg"
		var tarWidth = Screen.width;//SMB: Esto setea el ancho de la pantalla *****
		var tarHeight = Screen.height;//SMB: Esto setea el alto de la pantalla *****
		
		//------------------------------------------
		
		//--------------------
		var newTexture = ScreenShoot(Camera.main, tarWidth, tarHeight);//SMB: Esto setea cual sera la imagen que captura
		//var newTexture = ScreenShoot(Camera3D Camera.main, tarWidth, tarHeight);//SMB: Modificado para que tome de 3D Camera
		//--------------------
		
		//------------------------------------------
		LerpTexture(bg, ref newTexture);//SMB: Esto hace un quitado de los colores de fondo respecto de la textura que se capturo antesd. *****
		//------------------------------------------
		_data = System.Convert.ToBase64String(newTexture.EncodeToJPG());
		// Destroy(newTexture);
		Application.ExternalCall("DownloadImage", "data:image/octet-stream;base64," + _data);
		Enviar ();
	}
	
	
	private static Texture2D ScreenShoot(Camera srcCamera, int width, int height)
	{
	//	Debug.Log(string.Format("Screen: {0}x{1}. Texture: {2}x{3}", Screen.width, Screen.height, width, height));
		var renderTexture = new RenderTexture(Screen.width, Screen.height, 24);
		var targetTexture = new Texture2D(width, height, TextureFormat.RGB24, false);//MipMap: When set to true, rendering into this render texture will create and generate mipmap levels.
		srcCamera.targetTexture = renderTexture;
		srcCamera.Render();
		RenderTexture.active = renderTexture;
		targetTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		targetTexture.Apply();
		srcCamera.targetTexture = null;
		RenderTexture.active = null;
		srcCamera.ResetAspect();
		return targetTexture;
	}
	private static void LerpTexture(Texture2D alphaTexture, ref Texture2D texture)
	{
		//SMB: Anulamos el Lerp... *****
		var bgColors = alphaTexture.GetPixels();
		var tarCols = texture.GetPixels();
		// Debug.Log(string.Format("Lerping the texture. Alpha: {0}. Texture: {1}.", bgColors.Length, tarCols.Length));
		for (var i = 0; i < tarCols.Length; i++)
		{
			Color targColor = tarCols[i];
			Color backColor = bgColors[i];
			tarCols[i] = backColor.a > 0.99f ? backColor : Color.Lerp(targColor, backColor, backColor.a);
		}
		texture.SetPixels(tarCols);
		texture.Apply();
		//SMB: Anulamos el Lerp... *****
	}
	
	
	public void Enviar(){
	}
	
	
	//--aca reconoce la colision de los modelos- smb 
	
	internal bool IsConflictingCollider(SceneObject sceneObject, Collider collider)
	{
		if (Tools.IsInLayerMask(collider.gameObject, PositionLayer))
		{
			
			// Reconoce si los modelos van en la pared si hay conflicto en el collider (Is Conflict Collider)
			if ( sceneObject.Item.fijadoA == "pared")//SMB: AttachedToWall: Chequeo de Sting por si es pared
			{
				isNotInLayer = !Tools.IsInLayerMask(collider.gameObject, WallLayer);//SMB: AttachedToWall: Verificar?????
				//Debug.Log(string.Format("IsNotInLayer: {0}. Coll. Layer: ({1}){2}. WallLayer: {3}", isNotInLayer, collider.gameObject.layer, LayerMask.LayerToName(collider.gameObject.layer), WallLayer.value));//SMB: AttachedToWall: Verificar?????
				return isNotInLayer;//SMB: AttachedToWall: Verificar?????
			}else{
				return !Tools.IsInLayerMask(collider.gameObject, FloorLayer);
				
			}
			
		}else{
			return true;
		}
	}
	
	
	
	
	public float PosicionAuxiliarX;
	public float PosicionAuxiliarZ;
	
	
	internal Vector3 ClampPosition(Vector3 position, int sceneObjectAlturaModelo)
	{	
		
		
		if (sceneObject.Fijado != "pared") {
			
			//Muebles de suelo
			
			if (FirstPosition == true) {
				
				if (position.y <= MinFurniturePosition) {//smb
					//Aca tengo que crear un if que reconosca si hay otro objeto en esa posicion
					FirstPosition = false;//Desactivo el valor para que no sea el inicial y pueda moverse libremente el modelo
					//var objeto : GameObject = Instantiate(Warthog,gameObject.transform.position,Quaternion.Euler(0,90,0)); 
					return new Vector3 (position.x, MinFurniturePosition, (position.z - 1.7f));//Modificacion para que el objeto aparesca dentro de la habitacion sin importar donde se clickee por primera vez .
				}
				
			} else {
				
				if (position.y <= MinFurniturePosition) {
					return new Vector3 (position.x, MinFurniturePosition, position.z);
					
				}
			}
			
		} else {
			
			
			//Muebles de pared
			if (sceneObjectAlturaModelo == 1) {
				//Ventanas
			//	Debug.Log ("sceneObjectAlturaModelo (x1) == " + sceneObjectAlturaModelo);
				if (FirstPosition == true) {
					
					
					if (position.y <= MinFurniturePosition) {//smb
						
						//Aca tengo que crear un if que reconosca si hay otro objeto en esa posicion
						FirstPosition = false;//Desactivo el valor para que no sea el inicial y pueda moverse libremente el modelo
						//var objeto : GameObject = Instantiate(Warthog,gameObject.transform.position,Quaternion.Euler(0,90,0)); 
						return new Vector3 (position.x, MinFurniturePosition, (position.z - 1.7f));//Modificacion para que el objeto aparesca dentro de la habitacion sin importar donde se clickee por primera vez .
						
						
					}
				} else {
					
					if (position.y <= MinFurniturePosition) {
						
						return new Vector3 (position.x, MinFurniturePosition, position.z);//Modelo sigue la posiscion del mouse. Se coloca en ese lugar.
					}
					
				}
				return position;
				
				//SMB: ------>>>>> AttachedToWall: Add {}
				//}//SMB: AttachedToWall: Add {}
				//SMB: ------>>>>> AttachedToWall: End Add {}
			} else {
				
				
				//Puertas
			//	Debug.Log ("sceneObjectAlturaModelo (<>1) == " + sceneObjectAlturaModelo);				
				if (sceneObjectAlturaModelo == 0) {
					//Puertas
					
					return new Vector3 (position.x, 0.1f, position.z);//Modelo sigue la posiscion del mouse pero solo en horizontal. Se bloque a Y.
				}
			}
		}
		return position;
		
		
	}
	
	public CatalogueItemVariation variationAux;
	
	
	
	
	internal SceneObject SpawnModel(CatalogueItem catalogueItem, CatalogueItemVariation variation, Vector3 futureSpawnPoint)
	{
		
		
		
		
		sceneObject = this.CreateSceneObject(catalogueItem);
		
		//--intento set Nombres sets
		
		
	//	Debug.Log ("----------------------------->>>>>>>>>>>>>>>>>> SceneObject.SpawnModel() => FirstPosition=True");
		FirstPosition = true;//Inicializo en true para que el objeto comienze dentro de la habitacion.
		
		if (variation != null)
		{
			sceneObject.CurrentVariation = variation;
			sceneObject.ApplyVariation();
		}
		LocateModel(futureSpawnPoint, sceneObject);
		return sceneObject;
	}
	
	private SceneObject LocateModel(Vector3 futureSpawnPoint, SceneObject sceneObject)
	{
		
		//----intento nombre sets
		
		targetPosition = ClampPosition(futureSpawnPoint, sceneObject.AlturaModelo);
		//Debug.Log(string.Format("LocateModel: Clamping position from {0} to {1} - Colliders: {2}", futureSpawnPoint, targetPosition,(sceneObject.colliders!=null) ? sceneObject.colliders.Length : 0));
		sceneObject.transform.position = targetPosition;
		sceneObject.TranslationOperation.SetCurrentAsValid();
		return sceneObject;
	}
	
	internal void TryToSpawnModel(CatalogueItem catalogueItem, Vector3 futureSpawnPoint, CatalogueItemVariation variation = null)
	{
		if (Mode == ActionMode.None || Mode == ActionMode.Adding)
		{
			
			this.CurrentCatalogueItem = catalogueItem;
			this.SetMode(ActionMode.Adding);
			//  Debug.Log("TryToSpawnModel. " + futureSpawnPoint.ToString());
			this.SetCurrentBuildObject(SpawnModel(catalogueItem, variation, futureSpawnPoint));
		}
		
	}
	internal void TryToLocateModel(CatalogueItem catalogueItem, Vector3 futureSpawnPoint)
	{
		if (Mode == ActionMode.None || Mode == ActionMode.Adding)
		{
			
			this.CurrentCatalogueItem = catalogueItem;
			this.SetMode(ActionMode.Adding);
			// Debug.Log("TryToLocateModel. " + futureSpawnPoint.ToString());
			this.SetCurrentBuildObject(SpawnModel(catalogueItem, null, futureSpawnPoint));
		}
		
	}
	
	//--Logo
	// public JsonData LogoData;
	public string LogoId;
	public string LogoImagen;
	
	//public string PruebaJSON = "http://sistema.decorar3d.com/index.php/api/json";
	
	
	public void logo(){
		StartCoroutine(LogoGet ());
		//StartCoroutine (MaterialsGet ());
	}
	//sistema
	IEnumerator LogoGet(){
		
		yield return new WaitForEndOfFrame();
		//CatalogueURL
		var LogoForm = new WWWForm ();
		
		var LogoUp = new WWW(CatalogueURL,LogoForm);
		
		yield return LogoUp;
		
		string CutUsuario = LogoUp.text;
		CutUsuario = "{"+CutUsuario.Substring (LogoUp.text.Length - (LogoUp.text.Length - LogoUp.text.IndexOf ("\"usuario\": {")));
	//	Debug.Log ("SubString 1 = "+CutUsuario);
		JsonData LogoData = JsonMapper.ToObject(CutUsuario);
		LogoId = LogoData ["usuario"] ["id"].ToString();
		LogoImagen = Endpoint+"uploads/imagen/"+ LogoData ["usuario"] ["imagen"].ToString();
	
		

		

		
		
		
	}
	//fin sistema
	//--- Fin Logo 
	
	
}
