﻿using UnityEngine;

public class PlatfomMessageManager : MonoBehaviour {

    private static PlatfomMessageManager _instance = new PlatfomMessageManager();
	
	private void Awake ()
    {
        CheckPlatform();
	    _instance = this;
    }

    /// <summary>
    /// Con este Singleton se deberá acceder antes
    /// de la carga de los datos para checkear si la plataforma
    /// es compatible
    /// </summary>
    public static PlatfomMessageManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject _messageManager = new GameObject("_MessageManager");
                DontDestroyOnLoad(_messageManager);
                _instance = _messageManager.AddComponent<PlatfomMessageManager>();
            }

            return _instance;
        }
    }

	public GameObject ImagenPre;
    public void CheckPlatform()
    {
        #region Check Platform Message

        if (Application.platform == RuntimePlatform.Android)
        {
            print("####ANDROID####");
			ImagenPre.SetActive(true);
        }
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            print("####iPhone####");
			ImagenPre.SetActive(true);
        }

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            print("####WEBGL####");
			ImagenPre.SetActive(true);
        }

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            print("####WINDOWS EDITOR####");
			ImagenPre.SetActive(false);
			Application.LoadLevel(1);

        }
		if (Application.platform == RuntimePlatform.WindowsWebPlayer)
		{
			print("####WINDOWS EDITOR####");
			ImagenPre.SetActive(false);
			Application.LoadLevel(1);
			
		}

        #endregion
    }
	
	
}
