using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using System;
using UnityEngine.UI;

public class CatalogueView : MonoBehaviour
{
	public static CatalogueView Instance;
	public GameObject CategoryButtonPrefab;
	public GameObject VariationButtonPrefab;
	public GameObject MenuObject;
	public GridText CategoryGrid;
	public ThumbGrid VariationGrid;
	protected Assets.Scripts.Catalogue.Catalogue Catalogue = null;
	public bool RequiresFilter = false;
	protected Dictionary<string, CategoryButton> CategoryButtons = new Dictionary<string, CategoryButton>();
	protected Dictionary<string, List<CatalogueItemVariation>> CategorizedItems = new Dictionary<string, List<CatalogueItemVariation>>();
	
	
	public CategoryButton CategoryLevel1;
	public CategoryButton CategoryLevel2;
	public CategoryButton CategoryLevel3;
	public Text CategoryTitle;
	public bool showOnlyFeaturedInHome = false;
	public int HomeMaxAmount = 20;
	
	public CatalogueCategory CurrentCategory { get; protected set; }
	// Awake is called when the script instance
	// is being loaded.
	void Awake()
	{
		Instance = this;
	}
	
	// Start is called just before any of the
	// Update methods is called the first time.
	void Start()
	{
		SceneManager.SceneManagerInstance.CatalogueUpdated += CatalogueUpdated;
	}
	
	
	// Update is called every frame, if the
	// MonoBehaviour is enabled.
	void Update()
	{
		if (RequiresFilter)
		{
			if (Catalogue != null)
			{
				CatalogueCategory firstCategory = Catalogue.Categories.Count > 0 ? Catalogue.Categories[0] : null;
				if (firstCategory == null)
				{
					LoadCategory(firstCategory);
				}
				//else
				//{
				//    CategoryButtons[firstCategory.id].Clicked();
				//}
				
				RequiresFilter = false;
			}
		}
	}
	
	
	void OnEnable()
	{
		SceneManager.SceneManagerInstance.CatalogueUpdated += CatalogueUpdated;
		if (Catalogue == null && SceneManager.SceneManagerInstance.Catalogue != null)
		{
			CatalogueUpdated();
		}
	}
	void CatalogueUpdated()
	{
		Catalogue = SceneManager.SceneManagerInstance.Catalogue;
		Show();
		LoadCategory();
		var variations = Catalogue.Items.SelectMany(i => i.variations);
		foreach (var variation in variations)
		{
			CategorizeItem(variation.Item, variation);
		}
		LoadVariations(variations);
		RequiresFilter = true;
	}
	
	public void LoadCategory()
	{
		LoadCategory(CurrentCategory);
	}
	public void LoadCategory(CatalogueCategory toLoad)
	{
		if (toLoad != null)
		{
			Debug.Log("Loading category " + toLoad.name);
		}
		CurrentCategory = toLoad;
		var nextLevelCats = new List<GridText.IGridTextInfo>();
		
		
		if (CurrentCategory == null)
		{
			nextLevelCats.AddRange(Catalogue.Categories.Where(c => c.level == 0).Select(c => new GridText.GridTextInfo() { UID = c.UID, Text = c.name, Data = c }).Cast<GridText.IGridTextInfo>());
		}
		else
		{
			nextLevelCats.AddRange(CurrentCategory.children.Select(c => new GridText.GridTextInfo() { UID = c.UID, Text = c.name, Data = c }).Cast<GridText.IGridTextInfo>());
		}
		SetNavigation(toLoad);
		CategoryGrid.BuildGrid(nextLevelCats);
		FilterByCategory(toLoad);
	}
	
	private void SetNavigation(CatalogueCategory toLoad)
	{
		
		CategoryLevel1.gameObject.SetActive(false);
		CategoryLevel2.gameObject.SetActive(false);
		CategoryLevel3.gameObject.SetActive(false);
		if (toLoad != null)
		{
			CategoryTitle.text = toLoad.name + " Categorias";
			if (toLoad.level == 0)
			{
				CategoryLevel1.gameObject.SetActive(true);
				CategoryLevel1.GetComponentInChildren<Text>().text = toLoad.name;
				CategoryLevel1.Category = toLoad;
			}
			else if (toLoad.level == 1)
			{
				CategoryLevel1.gameObject.SetActive(true);
				CategoryLevel1.GetComponentInChildren<Text>().text = toLoad.parent.name;
				CategoryLevel1.Category = toLoad.parent;
				
				CategoryLevel2.gameObject.SetActive(true);
				CategoryLevel2.GetComponentInChildren<Text>().text = toLoad.name;
				CategoryLevel2.Category = toLoad;
			}
			else if (toLoad.level >= 2)
			{
				CategoryLevel1.gameObject.SetActive(true);
				CategoryLevel1.GetComponentInChildren<Text>().text = toLoad.parent.parent.name;
				CategoryLevel1.Category = toLoad.parent.parent;
				
				CategoryLevel2.gameObject.SetActive(true);
				CategoryLevel2.GetComponentInChildren<Text>().text = toLoad.parent.name;
				CategoryLevel2.Category = toLoad.parent;
				
				CategoryLevel3.gameObject.SetActive(true);
				CategoryLevel3.GetComponentInChildren<Text>().text = toLoad.name;
				CategoryLevel3.Category = toLoad;
			}
		}
		else
		{
			CategoryTitle.text = "Categorias";
		}
	}
	
	private void LoadVariations(IEnumerable<CatalogueItemVariation> variations)
	{
		Debug.Log("Loading variations " + variations.Count());
		FilterByCategory(null);
	}
	
	private void CreateVariationButtonForVariation(CatalogueItemVariation variation)
	{
		var variationGameObject = GameObject.Instantiate(this.VariationButtonPrefab) as GameObject;
		variationGameObject.name = string.Format("Variation_{0}_{1}", variation.Item.name.Replace(" ", ""), variation.name.Replace(" ", ""));
		var variationButton = variationGameObject.GetComponent<VariationButton>();
		variationGameObject.transform.parent = VariationGrid.transform;
		variationGameObject.transform.position = new Vector3();
		variationGameObject.transform.localScale = new Vector3(1, 1, 1);
		CategorizeItem(variation.Item, variation);
		variationButton.Item = variation.Item;
		variationButton.Variation = variation;
		variationButton.Load();
	}
	
	private void CategorizeItem(CatalogueItem item, CatalogueItemVariation variation)
	{
		if (!CategorizedItems.ContainsKey(item.category.ToUpper()))
		{
			CategorizedItems.Add(item.category.ToUpper(), new List<CatalogueItemVariation>() { variation });
		}
		else if (!CategorizedItems[item.category.ToUpper()].Contains(variation))
		{
			CategorizedItems[item.category.ToUpper()].Add(variation);
		}
	}
	
	private void FilterByCategory(CatalogueCategory category)
	{
		CurrentCategory = category;
		Debug.Log("Filtering current category: " + (category != null ? category.id : "NONE"));
		var activeItems = CategorizedItems.Where(cat => (category == null) || cat.Key == category.id.ToUpper()).SelectMany(c => c.Value).ToList();
		if (category == null)
		{
			if (showOnlyFeaturedInHome)
			{
				activeItems = activeItems.Where(it => it.Item.Featured).ToList();
			}
			VariationGrid.MaxAmount = HomeMaxAmount;
		}
		else
		{
			VariationGrid.MaxAmount = 0;
		}
		
		
		VariationGrid.BuildGrid(activeItems);
	}
	void SpriteDownloaded(KeyValuePair<string, Sprite> spriteKVP)
	{
		MenuObject.BroadcastMessage("SpriteDownloaded", spriteKVP, SendMessageOptions.DontRequireReceiver);
	}
	
	internal void Hide()
	{
		SetVisible(false);
	}
	
	internal void Show()
	{
		SetVisible(true);
	}
	
	internal void SetVisible(bool visible)
	{
		CanvasGroup group = MenuObject.GetComponent<CanvasGroup>();
		if (visible)
		{
			group.alpha = 1;
			group.interactable = true;
			group.blocksRaycasts = true;
		}
		else
		{
			group.alpha = 0;
			group.interactable = false;
			group.blocksRaycasts = false;
		}
	}
}
