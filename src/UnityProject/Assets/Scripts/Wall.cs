﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public GameObject Front;
    public GameObject Back;
    public Sides Side;
    public PlaceableFloor Parent;

    private bool BackVisible = false;

	void Start()
	{
		Front.GetComponent<MeshRenderer> ().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;

	
	}


    public void ToggleBackVisible()
    {
        SetBackVisible(!BackVisible);
    }
    public void SetBackVisible(bool visible)
    {
        BackVisible = visible;
        Back.SetActive(visible);
    }

    internal void AlignToFloor(PlaceableFloor floor)
    {
        Parent = floor;
        Vector3 oldScale = transform.localScale;
        var material = Front.GetComponent<Renderer>().material;
        if (Side == Sides.North || Side == Sides.South)
        {

            transform.localScale = new Vector3(floor.transform.localScale.z, transform.localScale.y, floor.transform.localScale.x);

        }
        else
        {
            transform.localScale = new Vector3(floor.transform.localScale.x, transform.localScale.y, floor.transform.localScale.z);

        }


       // Debug.Log(String.Format("Old Scale: {0} || {1}", oldScale, transform.localScale));
        float xPercent = (transform.localScale.x / oldScale.x) * 100;
        float zPercent = (transform.localScale.z / oldScale.z) * 100;
        //Debug.Log(String.Format("Scale Ratio: {0} / {1} || {2} / {3}", xPercent, zPercent, xPercent/100, zPercent/100));
        float targetXScaling = Front.GetComponent<Renderer>().material.mainTextureScale.x;// *(xPercent * -1 / 100);
        float targetYScaling = Front.GetComponent<Renderer>().material.mainTextureScale.y * (xPercent*-1 / 100);
        material.mainTextureScale = new Vector2(1, transform.lossyScale.z);
        
    }

    internal void RotateToSide()
    {
        switch (Side)
        {
            case Sides.North:
                transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
				transform.tag = "North";

                break;
            case Sides.South:
                transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
				transform.tag = "South";
				break;
            case Sides.West:
                transform.rotation = Quaternion.Euler(new Vector3(0, -180, 0));
			transform.tag = "West";
		

		
				break;
            case Sides.East:
			transform.tag = "East";
                break;
            default:
                break;
        }
        Front.gameObject.name = gameObject.name + " Front";
        Back.gameObject.name = gameObject.name + " Back";
		Front.GetComponent<MeshRenderer>().material.shader = Shader.Find("Custom/DiffuseNotEqualOne");

    }

   public void SetTexture(Material material, Texture texture)
    {

		Front.GetComponent<Renderer>().materials[0] = material;
        Back.GetComponent<Renderer>().materials[0] = material;



        Front.GetComponent<Renderer>().materials[0].mainTexture = texture;
	
        Back.GetComponent<Renderer>().materials[0].mainTexture = texture;
		//material = new Material(Shader.Find("Custom/WallCut"));
		//Front.GetComponent<Renderer>().materials[0] = new Material(Shader.Find("Custom/WallCut"));
    }
}
