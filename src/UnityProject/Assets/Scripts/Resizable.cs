using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Resizable : MonoBehaviour {

    public UIWidget ParentWidget;
    private UIPanel Panel;
    public UIGrid Grid;
    void Start()
    {
        Panel = this.GetComponent<UIPanel>();
        Resize();
    }
	// Update is called every frame, if the
	// MonoBehaviour is enabled.
    void Update()
    {
	}

    public bool UpdatePosition = false;
    public bool ResetAfterPositioning = true;

    public bool UpdateCenter = false;
    public bool ResetUpdateCenter = false;

    public bool UpdateSize = false;
    public bool ResetUpdateSize = false;

    public float CenterXModifier = 0.5f;
    public float CenterYModifier = 1;

    public float WidthModifier = 1;
    public float HeightModifier = 1;

    public float PositionOffsetXModifier = 0.01f;
    public float PositionOffsetYModifier = 0.01f;
    void Resize()
    {
        Vector2 center = new Vector2(Panel.baseClipRegion.x, Panel.baseClipRegion.y);
        Vector2 size = new Vector2(Panel.baseClipRegion.z, Panel.baseClipRegion.w);
        if (UpdateCenter)
        {
            center = new Vector2(ParentWidget.width * CenterXModifier, ParentWidget.height * CenterYModifier);
            if (ResetUpdateCenter)
                UpdateCenter = false;
        }
        if (UpdateSize)
        {
            size = new Vector2(ParentWidget.width * WidthModifier, ParentWidget.height * HeightModifier);
            if (ResetUpdateSize)
                UpdateSize = false;
        }

        Panel.baseClipRegion = new Vector4(center.x, center.y, size.x, size.y);
        if (Grid != null)
        {
            int amount = Grid.maxPerLine;
            Grid.maxPerLine = Mathf.FloorToInt(size.x / 48);
            if (amount != Grid.maxPerLine)
            {
                Grid.repositionNow = true;
            }
        }
        if (UpdatePosition)
        {
            Panel.transform.localPosition = new Vector3(ParentWidget.transform.localPosition.x + (size.x * PositionOffsetXModifier),
                                                        ParentWidget.transform.localPosition.y + (size.y * PositionOffsetYModifier),
                                                        ParentWidget.transform.localPosition.z);
            if (ResetAfterPositioning)
                UpdatePosition = false;
        }
        //Debug.Log(string.Format("Center: X: {0} Y: {1}.", Panel.baseClipRegion.x, Panel.baseClipRegion.y));
       // Debug.Log(string.Format("Size: X: {0} Y: {1}.", Panel.baseClipRegion.z, Panel.baseClipRegion.w));
    }

    public void StopUpdatePosition()
    {
        UpdatePosition = false;
    }
    public void StartUpdatePosition()
    {
        UpdatePosition = true;
    }
	
	// LateUpdate is called every frame, if
	// the Behaviour is enabled.
	void LateUpdate () {

        Resize();
	}
	
}
