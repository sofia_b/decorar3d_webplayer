﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Catalogue;

public class Floor : MonoBehaviour {

	public Vector2 EscalaImagen;//SMB: Este Vector2 mantiene la escala que se ira actualizando constantemente.
	public Vector3 EscalaFloorInicial;//SMB: Este Vector3 mantiene la escala del Floor inicial donde esta la textura, para poder hacer un resize en caso de ser necesario, luego va cambiando.
//<<<<<<< HEAD
	public string FloorCategory;
//=======
    
    private static  Floor _instance = new Floor();

    private void Awake()
    {
        _instance = this;
    }

    public static Floor Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject _floor = new GameObject("FloorManager");
                DontDestroyOnLoad(_floor);
                _instance = _floor.AddComponent<Floor>();
            }
            return _instance;
        }
    }
//>>>>>>> 86aabf30e8c81e1a65059d3c1938db4dcb210caa

	// Use this for initialization
	void Start () {
		EscalaImagen = this.GetComponent<MeshRenderer>().materials[0].mainTextureScale * 3;//SMB: Setea inicialmente el valor de cuanto es el Scale inicial.
		//EscalaFloorInicial = this.GetComponent<Transform>().transform.localScale;//SMB: Esto se usa para poder obtener el tamaño del transform actual

	}
	
	// Update is called once per frame
	void Update () {
	}
	
	public void SetInitialSize(Vector3 EFloorInicial){
		//SMB: Esta funcion setea el tamaño inicial sin afectar al tamaño de la textura, es solo a modos de uso en el Init del objeto padre
		EscalaFloorInicial = EFloorInicial;
	}
	public Vector3 SizeFloor;
	public void SetNewSize(Vector3 EFloorActual)
    {
		//SMB: Esta funcion lo que hace es hacer un resize en base a la dimension 3D pasada, consideranco el minimo de 
		
		//------------------------------------------------------------------------------
		//SMB: Calculo de redimensionamiento bi-dimensional en comparacion con el valor 3D pasado y el original (solo se usan X and Z ("Y" determina la altura, segun eje cartesiano Unity).
		//------------------------------------------------------------------------------
		
		float ValorCompresionX;
		float ValorCompresionY;
		
		ValorCompresionX = 1 + ((EFloorActual.x - EscalaFloorInicial.x) / EscalaFloorInicial.x);//SMB: Calcula el % de crecimiento/decrecimiento total de X en base a la diferencia en X del Floor.
		ValorCompresionY = 1 + ((EFloorActual.z - EscalaFloorInicial.z) / EscalaFloorInicial.z);//SMB: Calcula el % de crecimiento/decrecimiento total de Y en base a la diferencia en Z del Floor (recuerden que "Y" representa altura).
		
		EscalaImagen.x = EscalaImagen.x * ValorCompresionX;//SMB: Redimenciona concretamente el valor de Z del tamaño de la imagen.
		EscalaImagen.y = EscalaImagen.y * ValorCompresionY;//SMB: Redimenciona concretamente el valor de Y del tamaño de la imagen.
		
		EscalaFloorInicial = EFloorActual;//SMB: Esto toma el vqalor actual y lo almacena como el valor inicial para el proximo calculo.
		//------------------------------------------------------------------------------
		
		this.GetComponent<MeshRenderer>().materials[0].mainTextureScale = EscalaImagen;//SMB: Este comando cambia la escala de la textura en base a un valor de Vector2
		SizeFloor = this.GetComponent<Renderer>().bounds.size;


        //if (roomType == 1)
        //{

        //    ValorCompresionX = 2 + ((EFloorActual.x - EscalaFloorInicial.x) / EscalaFloorInicial.x);
        //    ValorCompresionY = 2 + ((EFloorActual.z - EscalaFloorInicial.z) / EscalaFloorInicial.z);

        //    EscalaImagen.x = EscalaImagen.x * ValorCompresionX;
        //    EscalaImagen.y = EscalaImagen.y * ValorCompresionY;

        //    EscalaFloorInicial = EFloorActual;

        //}
    }
	
	internal void SetTexture(Material material, Texture texture)
	{
//		Debug.Log(material);
		this.GetComponent<MeshRenderer>().materials[0] = material;
		this.GetComponent<MeshRenderer>().materials[0].mainTexture =texture;

		
	}
}