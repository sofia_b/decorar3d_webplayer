using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour
{
		Camera currentCamera;
		SceneManager sceneManager;
		public NavigationPanel NavigationPanel;

		public float MousePanHorizontalSpeed = 0.25f;
		public float MousePanVerticalSpeed = 0.25f;

		public bool RequestZoomUp;
		public bool RequestZoomDown;
		public bool RequestPanUp;
		public bool RequestPanDown;
		public bool RequestPanLeft;
		public bool RequestPanRight;
		public float ZoomMinDistance = 1;
		public float ZoomMaxDistance = 10;
		public float ZoomStep = 4;
		float ZoomRequestWeight = 0.2f;
		float MoveRequestWeight = 0.2f;

	bool setPosition;
	Vector3 position;
	float zoom;	
	Vector2 movement;

	float scrollAmount;
	float horizontal;
	float vertical ;
	Vector2 touchDeltaPosition;
	float zoomDistance;

		void Awake ()
		{
				sceneManager = GameObject.FindGameObjectWithTag ("SceneManager").GetComponent<SceneManager> ();
				Input.simulateMouseWithTouches = true;
				if (NavigationPanel != null) {
						NavigationPanel.gameObject.SetActive (!Input.mousePresent);
	
				}
		}
		// Start is called just before any of the
		// Update methods is called the first time.
		void Start ()
		{
				currentCamera = this.GetComponent<Camera> ();
		
				if (currentCamera.orthographic) {
						transform.position = new Vector3 (transform.position.x, currentCamera.orthographicSize, transform.position.z);
				} else {
						currentCamera.orthographicSize = transform.position.y;
				}
		}
	
		// Update is called every frame, if the
		// MonoBehaviour is enabled.
		void Update ()
		{ 

		setPosition = false;
				 position = transform.position;
		
				zoom = GetZoomAmount ();
				if (zoom != 0) {
						 zoomDistance = position.y;
						if (currentCamera.orthographic) {
								zoomDistance = currentCamera.orthographicSize + zoom * (ZoomStep * -1);
						} else {
								zoomDistance += zoom * (ZoomStep * -1);
								setPosition = true;
						}
						zoomDistance = Mathf.Clamp (zoomDistance, ZoomMinDistance, ZoomMaxDistance);
						currentCamera.orthographicSize = zoomDistance;
						position.y = zoomDistance;
				}
				if (InputPanCamera ()) {
						var movement = GetMovement ();
						
						position.x += movement.x * -1;
						position.z += movement.y * -1;
						setPosition = true;
						
				}
				if (setPosition) {
						transform.position = position;
				}
		}
		public void OnGUI ()
		{
				if (Event.current.type == EventType.MouseDrag) {
				} else if (Event.current.type != EventType.Layout
						&& Event.current.type != EventType.repaint) {
				}
		}
	
		float GetZoomAmount ()
		{
	
		scrollAmount= Input.GetAxis ("Mouse ScrollWheel");
				if (scrollAmount == 0f) {
					
						if (RequestZoomUp) {
								scrollAmount += ZoomRequestWeight;
								RequestZoomUp = false;
						} else if (RequestZoomDown) {
								scrollAmount -= ZoomRequestWeight;
								RequestZoomDown = false;
						}
				}
				return scrollAmount;
		}

		bool InputPanCamera ()
		{
				return Input.GetMouseButton (1) || (RequestPanUp || RequestPanRight || RequestPanLeft || RequestPanDown);
		}

		public void SwitchCamera (bool isIsometric)
		{
				currentCamera.orthographic = isIsometric;
		}

		public void ToggleView ()
		{
				currentCamera.orthographic = !currentCamera.orthographic;
		}

		Vector2 GetMovement ()
		{
	

			movement = new Vector2 ();
				if (Input.mousePresent) {


			horizontal= Input.GetAxis ("Mouse X") * MousePanHorizontalSpeed;

			vertical= Input.GetAxis ("Mouse Y") * MousePanVerticalSpeed;
						movement = movement + new Vector2 (horizontal, vertical);
				}
				if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {
			
						// Get movement of the finger since last frame
						 touchDeltaPosition = Input.GetTouch (0).deltaPosition;
			
						// Move object across XY plane
						movement = movement + touchDeltaPosition;
				}
				if (movement == default(Vector2)) {
						if (RequestPanRight || RequestPanLeft) {
								movement.x = MoveRequestWeight * (RequestPanLeft ? 1 : -1);
						}
						if (RequestPanUp || RequestPanDown) {
								movement.y = MoveRequestWeight * (RequestPanDown ? 1 : -1);
						}
						RequestPanUp = RequestPanDown = RequestPanLeft = RequestPanRight = false;
				}
				Debug.Log (string.Format ("After Touch: {0}", movement));
				return movement;
		}
}
