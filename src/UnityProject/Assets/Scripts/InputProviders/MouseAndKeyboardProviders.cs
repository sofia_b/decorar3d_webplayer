﻿using Assets.Scripts.Camera;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.InputProviders
{
    public class MouseAndKeyboardProviders : IInputProvider
    {
        public UserInput GetInput()
        {
            UserInput userInput = new UserInput();
            userInput.IsRightPressed = Input.GetMouseButton(1);
            userInput.IsMiddlePressed = Input.GetMouseButton(2);
            userInput.PointerHorizontalAxis = Input.GetAxis("Mouse X");
            userInput.PointerVerticalAxis = Input.GetAxis("Mouse Y");
            userInput.MovementHorizontalAxis = Input.GetAxis("Horizontal");
            userInput.MovementVerticalAxis = Input.GetAxis("Vertical");
            userInput.ScrollWheelAxis = Input.GetAxis("Mouse ScrollWheel");
            userInput.MoveFast = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
			userInput.MoveSlow = Input.GetKey (KeyCode.LeftControl);//|| Input.GetKey(KeyCode.RightControl);
            userInput.ToggleLock = Input.GetKeyDown(KeyCode.End);
            return userInput;
        }
    }
}
