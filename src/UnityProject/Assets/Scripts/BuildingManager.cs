﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingManager : MonoBehaviour
{
    public LayerMask InteractionLayer;
    public bool AllowToBuild = true;
    public GameObject FloorContainer;
    public GameObject FloorPrefab;
    public GameObject WallContainer;
    public GameObject WallPrefab;
    Dictionary<System.Guid, PlaceableFloor> Floors = new Dictionary<System.Guid, PlaceableFloor>();
	RaycastHit hitinfo;
	ProxyToFloor proxy;
	PlaceableFloor floor;
	DimBoxes_DimBox box;
	Transform transform;
	PlaceableFloor[] children;
	Wall wall;
	Vector3 targetPosition ;

    private int floorCount = 1;

	public string FloorName;
    void Awake()
    {
    }

    void Start()
    {
    }

    void Update()
    {
        if (AllowToBuild)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    		 

			//Debug.Log("RayCast - BuildingManager (6)");
            if (Physics.Raycast(ray, out hitinfo, 100, InteractionLayer))
            {
                //            Debug.Log(hitinfo.collider);
               // Debug.DrawLine(Camera.main.transform.position, hitinfo.point);
                if (hitinfo.collider.transform.tag == "Handler")
                {
                     proxy = hitinfo.collider.GetComponent<ProxyToFloor>();
                    if (Input.GetMouseButton(0))
                    {
                        if (proxy.floorObject.CreateNeighbour(proxy.Side))
                            floorCount++;
                    }
                    else if (Input.GetMouseButton(2) && floorCount > 1)
                    {
                        RemoveFloor(proxy.floorObject);
                        RefreshFloorContainerMeasures();
                    }
                }
                else if (hitinfo.collider.transform.tag == "Floor")
                {
                    if (Input.GetMouseButton(2) && floorCount > 1)
                    {
                        floor = hitinfo.collider.GetComponent<PlaceableFloor>();
                        RemoveFloor(floor);
                        RefreshFloorContainerMeasures();
                    }
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.W))
            CreateAllWalls();

       /* if (Input.GetKeyUp(KeyCode.B))
            ToggleAllBackWalls();*/





    }

    private void RefreshFloorContainerMeasures()
    {
        box =  FloorContainer.GetComponent<DimBoxes_DimBox>();
        if (box!=null) box.DoStartup(true);
    }

    public void RemoveFloor(PlaceableFloor floor)
    {
        floor.RemoveSelf();
        floor.gameObject.SetActive(false);
        //GameObject.Destroy(floor.gameObject);

        floorCount--;
    }
    internal void Register(System.Guid ID, PlaceableFloor placeableFloor)
    {
        if (!Floors.ContainsKey(ID))
        {
            Floors.Add(ID, placeableFloor);
        }
    }
    internal void Unregister(System.Guid ID)
    {
        if (Floors.ContainsKey(ID))
        {
            Floors.Remove(ID);
        }
    }
    private bool allBackVisible = false;
    private void ToggleAllBackWalls()
    {
        allBackVisible = !allBackVisible;
        int childCount = WallContainer.transform.GetChildCount();
        for (int i = 0; i < childCount; i++)
        {
             transform = WallContainer.transform.GetChild(i);
            wall = transform.GetComponent<Wall>();
            if (wall != null)
            {
                wall.SetBackVisible(allBackVisible);
            }
        }
    }

    void CreateAllWalls()
    {
		children = FloorContainer.GetComponentsInChildren<PlaceableFloor>();//llamada a placeable floor

        for (int i = 0; i < children.Length; i++)
        {
            PlaceableFloor floor = children[i];
            if (floor != null)
            {
                floor.UpdateWalls(true);
            }
        }
    }

    public void CreateWall(PlaceableFloor floor, Sides side)
    {
        targetPosition = floor.CalculateWallPosition(side);


        var copy = GameObject.Instantiate(WallPrefab) as GameObject;
        copy.transform.position = targetPosition;
        copy.name = string.Concat(floor.gameObject.name, " ",  side, " Wall");
		//Debug.Log ("*********" +  copy.name);
         wall = copy.GetComponent<Wall>();
        wall.Side = side;
        wall.RotateToSide();

        floor.AddWallToSide(side, copy);
       wall.SetBackVisible(allBackVisible);
        wall.AlignToFloor(floor);
        copy.transform.parent = WallContainer.transform;
    }

    internal void CreateFloor(Vector3 targetPosition)
    {
        Debug.DrawLine(targetPosition, targetPosition + new Vector3(0, 10, 0), Color.white, 3);
        var copy = GameObject.Instantiate(FloorPrefab) as GameObject;
        copy.transform.position = targetPosition;
        copy.transform.parent = FloorContainer.transform;


    }



}