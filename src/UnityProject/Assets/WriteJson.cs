using LitJson;
using System;
using System.Text;

public class WriteJson
{
	public static void Main()
	{
		StringBuilder sb = new StringBuilder();
		JsonWriter writer = new JsonWriter(sb);
		
		writer.WriteArrayStart();
		writer.Write(1);
		writer.Write(2);
		writer.Write(3);
		
		writer.WriteObjectStart();
		writer.WritePropertyName("color");
		writer.Write("blue");
		writer.WriteObjectEnd();
		
		writer.WriteArrayEnd();
		
		Console.WriteLine(sb.ToString());
	}
}


