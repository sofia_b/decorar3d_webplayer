﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;

public class CategoriasPresupuestar : MonoBehaviour {
	public bool ValidarSitio;//Es necesario crear un parametro en DB 
	public bool ValidadoPrevio;//Es necesario crear un parametro en DB
	public GameObject PanelPresupuestar;
	public Toggle Deco;
	public Toggle Abert;
	public Toggle Ilum;
	public Toggle Electro;


	public bool Decoracion;
	public bool Aberturas;
	public bool Iluminacion;
	public bool Electrodomesticos;
	public bool Muebles=true;

	// Use this for initialization
	void Awake(){

		//StartCoroutine(YaPresupuestadas());
	}
	void Start () {

		if (ValidadoPrevio == true) {
						PanelPresupuestar.SetActive (false);
				} else {
			PanelPresupuestar.SetActive (true);
				}

		if (Decoracion == true) {
						Deco.isOn = true;
				} else {
			Deco.isOn = false;
				}

		if (Aberturas == true) {
			Abert.isOn = true;
		} else {
			Abert.isOn = false;
		}

		if (Iluminacion == true) {
			Ilum.isOn = true;
		} else {
			Ilum.isOn = false;
		}

		if (Electrodomesticos == true) {
			Electro.isOn = true;
		} else {
			Electro.isOn = false;
		}



		
	}
	
	// Update is called once per frame
	void Update () {

	}

	//Verificacion de valores traidos desde la base de datos
	string PresupuestadasURL;
	IEnumerator YaPresupuestadas(){
		yield return new WaitForEndOfFrame();

		var TextPresupuestadas = new WWW(PresupuestadasURL);
		yield return TextPresupuestadas;

		var Pres = JSON.Parse (TextPresupuestadas.text);
		ValidarSitio = Pres ["Usuario"]["ValidarSitio"].AsBool;
		ValidadoPrevio = Pres["Usuario"]["ValidadoPrevio"].AsBool;
		Decoracion = Pres ["Usuario"]["Decoracion"].AsBool;
		Aberturas = Pres ["Usuario"]["Aberturas"].AsBool;
		Iluminacion = Pres ["Usuario"]["Iluminacion"].AsBool;
		Electrodomesticos = Pres ["Usuario"]["Electrodomesticos"].AsBool;

		}

	public void DecoValue(){
		
		if (Deco.isOn) {
			Decoracion = true;
		} else {
			Decoracion = false;		
		}
		//Debug.Log("Decoracion= " + Decoracion);
	}
	
	public void AbertValue(){
		if (Abert.isOn) {
			Aberturas = true;
		} else {
			Aberturas = false;		
		}
		//Debug.Log("AberturasSN= " + Aberturas);
	}
	public void IlumValue(){
		if (Ilum.isOn) {
			Iluminacion = true;
		} else {
			Iluminacion = false;
		}
		//Debug.Log("Iluminacion= " + Iluminacion);
	}
	public void ElectroValue(){
		if (Electro.isOn) {
			Electrodomesticos = true;
		} else {
			Electrodomesticos = false;
		}
		//Debug.Log("Electrodomesticos= " + Electrodomesticos);
	}

	public void EnviarDatosPresupuestar(){
		//StartCoroutine(IEnumerator APresupuestar());
		PanelPresupuestar.SetActive (false);
			
	}
	public string PresupCategoriasURL;
	IEnumerator APresupuestar(){

		yield return new WaitForEndOfFrame();
		var postForm = new WWWForm();
		postForm.AddField ("Presupuestar_Deco", Decoracion.ToString());
		postForm.AddField ("Presupuestar_Ab", Aberturas.ToString());
		postForm.AddField ("Presupuestar_Ilum", Iluminacion.ToString());
		postForm.AddField ("Presupuestar_Electro", Electrodomesticos.ToString());

		var uploadPresupCat = new WWW (PresupCategoriasURL, postForm);
		yield return uploadPresupCat;
	}

}
