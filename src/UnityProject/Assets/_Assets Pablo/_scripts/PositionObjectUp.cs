﻿using UnityEngine;
using System.Collections;

public class PositionObjectUp : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        SetUpObjectCollision(other.transform);
    }
    private void SetUpObjectCollision(Transform objectCollision)
    {
        float yScale = objectCollision.localScale.y;

        transform.position = new Vector3(objectCollision.position.x, objectCollision.position.y + yScale / 2, objectCollision.position.z);
        print("+++++++++COLLIDED+++++++++");
    }
}
