﻿using UnityEngine;
using System.Collections;

public class ChangeLang : MonoBehaviour {
	public void ChangeL(string lang){
		switch (lang) {
		case "es":
			//Debug.Log("Change to Spanish");
			this.GetComponent<LocalizationEditor.LELanguageSelectSample>().ChangeLang(0);
			break;
		case "en-US":
		//	Debug.Log("Change to EN - EEUU");
			this.GetComponent<LocalizationEditor.LELanguageSelectSample>().ChangeLang(1);
			break;
		case "pt":
		//	Debug.Log("Change to Portugal");
			this.GetComponent<LocalizationEditor.LELanguageSelectSample>().ChangeLang(2);
			break;
		default:
			//Debug.Log("Not find: "+lang);
			break;
		}
	}
}
