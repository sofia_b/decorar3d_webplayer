﻿using UnityEngine;
using System.Collections;

public class ControlLuz : MonoBehaviour {


	
	public int lightFactor = 0;
	public int ValorIntercambio;
	
	// Light that should be controlled by the LightController
	public Light controlledLight01 = null;
	public GameObject CamaraSky;
	public Color Fondo;

	/// Array de Ventanas y puertas con vidrio

	Color ChangeVidrio;

	void Start () {

		ValorIntercambio = 0;
		Fondo = CamaraSky.GetComponent<Camera> ().backgroundColor;

		//Debug.Log ("Color inicial Luz"+controlledLight01.color);
		//Debug.Log ("Color inicial intensidad"+controlledLight01.intensity);
		//Debug.Log ("FondoInicial:" + Fondo);

	


	}



	public GameObject[] VidriosAb = new GameObject[16]; 
	public int CantAberturas;
	public bool ForceUpdateAberturas;

	/*public void Force(){
		ForceUpdateAberturas=true;

	}*/

	void Update () {



		CantAberturas = GameObject.FindGameObjectsWithTag ("Aberturas").Length;
		if((VidriosAb.Length != CantAberturas) || ForceUpdateAberturas){
			VidriosAb = new GameObject[0];
			VidriosAb = GameObject.FindGameObjectsWithTag ("Aberturas");

			if(VidriosAb.Length>0){
				foreach(GameObject AberturaObj in VidriosAb){
					int largo=AberturaObj.GetComponent<MeshRenderer>().materials.Length;
					if(largo>0){
						foreach(Material MatAbObj in AberturaObj.GetComponent<MeshRenderer>().materials){
							Debug.Log ("====>>> Material= '"+MatAbObj.name+"'");
							if(MatAbObj.name=="VIDRIO (Instance)"){
								MatAbObj.color=Fondo;
								Debug.Log ("===========>>> Change color. Color="+Fondo);

							}
						}
					}
				}
			}
			//ForceUpdateAberturas=false;
		}

		
	}



	public void Atardecer(){
		Light l = controlledLight01.GetComponent<Light>(); // Get the Light component
		Color c = new Color();
		Debug.Log ("LuzInicial:" + l.color);


		/*c.r= 1f - lightFactor / 100f;
			c.g= lightFactor / 100f;
			//c.b= lightFactor / 100f;
			c.a=1f;

		*/
	switch (ValorIntercambio) {

		case 0:
		

			c.r = 176/100f;
			c.g = 62/100f;
			c.b = 36/100f;
			c.a = 255/100f;
			
			l.color = c;
			l.intensity = 1f;
			
			
			Fondo.r = 176/100f;
			Fondo.g = 62/100f;
			Fondo.b = 36/100f;
			CamaraSky.GetComponent<Camera> ().backgroundColor = Fondo;


			Debug.Log ("color" + l.color);
			ValorIntercambio = 1;


			break;
		
		case 1:
			
			c.r = 8/100f;
			c.g = 21/100f;
			c.b = 109/100f;
			c.a = 255/100f;
			
			l.color = c;
			l.intensity = 1f;

			//cambiar skybox
			
			
			Fondo.r = 21/100f;
			Fondo.g = 44/100f;
			Fondo.b = 71/100f;
			
			CamaraSky.GetComponent<Camera> ().backgroundColor = Fondo;
			ValorIntercambio = 2;
			Debug.Log("Valor de intercambio: "+ ValorIntercambio);
			break;
		case 2:
		
			c.r = 2.550f;
			c.g = 2.550f;
			c.b = 2.550f;
			c.a = 1.550f;
			
			l.color = c;
			l.intensity = 0.5f;

			//(0.37,0.639,0.851,1.00);
			Fondo.r = 0.37f;
			Fondo.g = 0.639f;
			Fondo.b = 0.851f;
			Fondo.a = 1f;

			
			CamaraSky.GetComponent<Camera> ().backgroundColor = Fondo;
			ValorIntercambio = 0;
			Debug.Log("Valor de intercambio: "+ ValorIntercambio);


			break;

		

		
		}

		ForceUpdateAberturas = true;

	}



}

		