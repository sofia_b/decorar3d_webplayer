﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEngine.UI;

public class VirtualPadController : MonoBehaviour, IInputProvider {

    // Use this for initialization
    void Start() {

    }

    public Assets.Scripts.Camera.UserInput currentInput = null;
    public void StartMoveForward() { IsMovingForward = true; }
    public void StartMoveBackward() { IsMovingBackward = true; }
    public void StartMoveRight() { IsMovingRight = true; }
    public void StartMoveLeft() { IsMovingLeft = true; }
    public void StartRotateForward() { IsRotatingForward = true; }
    public void StartRotateBackward() { IsRotatingBackward = true; }
    public void StartRotateRight() { IsRotatingRight = true; }
    public void StartRotateLeft() { IsRotatingLeft = true; }
    public void StartZoomUp() { IsZoomUp = true; }
    public void StartZoomDown() { IsZoomDown = true; }

    public void StopMoveForward() { IsMovingForward =false; }
    public void StopMoveBackward() { IsMovingBackward =false; }
    public void StopMoveRight() { IsMovingRight =false; }
    public void StopMoveLeft() { IsMovingLeft =false; }
    public void StopRotateForward() { IsRotatingForward =false; }
    public void StopRotateBackward() { IsRotatingBackward =false; }
    public void StopRotateRight() { IsRotatingRight =false; }
    public void StopRotateLeft() { IsRotatingLeft =false; }
    public void StopZoomUp() { IsZoomUp =false; }
    public void StopZoomDown() { IsZoomDown =false; }

    public float MovementSpeed = 0.9f;
    public float RotationSpeed = 1f;

    public bool IsMovingForward;
    public bool IsMovingBackward;
    public bool IsMovingRight;
    public bool IsMovingLeft;
    public bool IsRotatingForward;
    public bool IsRotatingBackward;
    public bool IsRotatingLeft;
    public bool IsRotatingRight;
    public bool IsZoomUp;
    public bool IsZoomDown;
    // Update is called once per frame

    void Update() {
        currentInput = new Assets.Scripts.Camera.UserInput();
        if (IsMovingForward) {
            currentInput.MovementVerticalAxis = MovementSpeed;
        }
        else if (IsMovingBackward) {
            currentInput.MovementVerticalAxis = MovementSpeed * -1;
        }
        if (IsMovingRight) {
            currentInput.MovementHorizontalAxis = MovementSpeed;
        }
        else if (IsMovingLeft) {
            currentInput.MovementHorizontalAxis = MovementSpeed * -1;
        }

        if (IsRotatingForward) {
            currentInput.IsRightPressed = true;
            currentInput.PointerVerticalAxis = RotationSpeed * -1;
        }
        else if (IsRotatingBackward) {
            currentInput.IsRightPressed = true;
            currentInput.PointerVerticalAxis = RotationSpeed;
        }
        if (IsRotatingRight) {
            currentInput.IsRightPressed = true;
            currentInput.PointerHorizontalAxis = RotationSpeed;
        }
        else if (IsRotatingLeft) {
            currentInput.IsRightPressed = true;
            currentInput.PointerHorizontalAxis = RotationSpeed * -1;
        }

        if (IsZoomDown) {
            currentInput.IsMiddlePressed = true;
            currentInput.PointerVerticalAxis = MovementSpeed * -1;
        }
        else if (IsZoomUp) {
            currentInput.IsMiddlePressed = true;
            currentInput.PointerVerticalAxis = MovementSpeed;
        }
        //IsMovingForward = false;
        //IsMovingBackward = false;
        //IsMovingRight = false;
        //IsMovingLeft = false;
        //IsRotatingForward = false;
        //IsRotatingBackward = false;
        //IsRotatingLeft = false;
        //IsRotatingRight = false;
        //IsZoomUp = false;
        //IsZoomDown = false;



		KeyP ();

    }
    public Assets.Scripts.Camera.UserInput GetInput() {
        return currentInput;
    }

	public void KeyP(){
		//---Teclas Izquierda w.s.a.d
		//--W
		if (Input.GetKeyDown (KeyCode.W)) {

			StartZoomUp ();
		} 

		if(Input.GetKeyUp(KeyCode.W)){
			StopZoomUp ();
		
		}

		//---S-------------

		if (Input.GetKeyDown (KeyCode.S)) {
			
			StartZoomDown ();

		} 
		if(Input.GetKeyUp(KeyCode.S)){	
			StopZoomDown ();
		}

		//----------------
		//------R----------
		
		if (Input.GetKeyDown (KeyCode.R)) {
			
			StartRotateLeft ();
			
		} 
		if(Input.GetKeyUp(KeyCode.R)){	
			StopRotateLeft ();
		}
		
		//----------------
		//--------F--------
		
		if (Input.GetKeyDown (KeyCode.F)) {
			
			StartRotateRight ();
			
		} 
		if(Input.GetKeyUp(KeyCode.F)){	
			StopRotateRight ();
		}
		
		//----------------

		//---Teclas Derecha Direccionles
		//--Up
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			
			StartZoomUp ();
		} 
		
		if(Input.GetKeyUp(KeyCode.UpArrow)){
			StopZoomUp ();
			
		}
		
		//--------Down--------
		
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			
			StartZoomDown ();
			
		} 
		if(Input.GetKeyUp(KeyCode.DownArrow)){	
			StopZoomDown ();
		}


		//---RIGHT--
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			
			StartMoveRight ();
			
		} 
		if(Input.GetKeyUp(KeyCode.RightArrow)){	
			StopMoveRight ();
		}

		//--LEFT---
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			
			StartMoveLeft ();
			
		} 
		if(Input.GetKeyUp(KeyCode.LeftArrow)){	
			StopMoveLeft ();
		}
		
		//------CTRL + ----------
		//------CTRL + LEft----------
		
		//if (Input.GetKeyDown (KeyCode.RightControl) && Input.GetKeyDown (KeyCode.LeftArrow)) {
		if (Input.GetKeyDown (KeyCode.M)) {
			
			StartRotateLeft ();
			
		} 
		if(Input.GetKeyUp(KeyCode.M)){	
			StopRotateLeft ();
		}
		
		//------CTRL + Right----------

		
		if (Input.GetKeyDown (KeyCode.N)) {
			
			StartRotateRight ();
			
		} 
		if(Input.GetKeyUp(KeyCode.N)){	
			StopRotateRight ();
		}
		

		//---CTRL + Up
		
		if (Input.GetKeyDown (KeyCode.K)) {
			
			StartMoveForward ();
			
		} 
		if(Input.GetKeyUp(KeyCode.K)){	
			StopMoveForward ();
		}
		//---CTRL + Down

		if (Input.GetKeyDown (KeyCode.J)) {
			
			StartMoveBackward ();
			
		} 
		if(Input.GetKeyUp(KeyCode.J)){	
			StopMoveBackward ();
		}

		if (Input.GetKeyDown (KeyCode.R)) {
			
			StartRotateLeft ();
			
		} 
		if(Input.GetKeyUp(KeyCode.R)){	
			StopRotateLeft ();
		}
		
		//----------------


	}


}
