﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using System;
using UnityEngine.UI;


public class PisosPinturas : MonoBehaviour {

	public string NombrePiso;
	public string CategoryPiso;
	public string CategoryName;
	public string NombrePintura;

	public Text WallText;
	public Text FloorText;


	//VariationButton NameV;
	// Use this for initialization
	void Start () {



	}


	public void SetCategoryPiso(string catPiso){
		CategoryPiso = catPiso;

		switch (CategoryPiso) {
			
	
			case "616":
			CategoryName = "Cerámicos";
			break;
	
			case "617":
			CategoryName = "Maderas";
			break;	
			case "618":
			CategoryName = "Rosbaco";
			break;
			case "629":
			CategoryName = "Formica";
			break;	

		}

	}

	public void SetNombresPiso(string NmPiso){
		NombrePiso = NmPiso +"("+ CategoryName +")";
	}
	public void SetNombresPintura(string NmPintura){
		NombrePintura = NmPintura;
	}
	
	// Update is called once per frame
	void Update () {

		WallText.text = NombrePintura;
		FloorText.text = NombrePiso;
//	NombrePintura = NameV.WallName;
//		NombrePiso = NameV.FloorName;
	}
}
