﻿using UnityEngine;
using System.Collections;

public class VirtualPadTranslation : MonoBehaviour {
	public Animator startButton;
	//public Animator settingsButton;
	public bool verificacion;
	// Use this for initialization
	void Start () {

		//startButton.SetBool ("isHidden", true);
		verificacion = false;
	}
	
	// Update is called once per frame
	void Update () {


	}

	public void OpenSettings() {
		//startButton.SetBool ("isHidden", true);
		if (verificacion == true) {
			startButton.SetBool ("isHidden", true);
			verificacion = false;
		} else {
			startButton.SetBool ("isHidden", false);
			verificacion = true;
		}
	//	settingsButton.SetBool("isHidden", true);
	}
}
