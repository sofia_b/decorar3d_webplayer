﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

    Camera camera;
	// Use this for initialization
	void Start () {
        camera = this.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetFieldOfView(float fov)
    {
        camera.fieldOfView = fov;
    }
}
