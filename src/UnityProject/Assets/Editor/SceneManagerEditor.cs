﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SceneManager)), CanEditMultipleObjects]
public class SceneManagerEditor : Editor
{
	
		public SerializedProperty longStringProp;
	
		void OnEnable ()
		{
				longStringProp = serializedObject.FindProperty ("TextData");
		}
	
		public override void OnInspectorGUI ()
		{
				base.OnInspectorGUI ();
//				serializedObject.Update ();
//				
//				longStringProp.stringValue = EditorGUILayout.TextArea (longStringProp.stringValue, GUILayout.MaxHeight (75));
//				serializedObject.ApplyModifiedProperties ();
		}
}