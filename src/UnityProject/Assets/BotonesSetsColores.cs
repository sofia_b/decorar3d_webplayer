using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class BotonesSetsColores: MonoBehaviour{
//---------------------


	//Botones SetMateriales
	//public GameObject BotonSets;

	public Text TextoBotonSets;

	public String SNombres;
//-------------------
	public CatalogueItem Item;
	public CatalogueItemVariation var;

	//public SceneObject Scene;
	public int CantSetsMat;
	public string IdSetsS;
	public int SetSeleccionado;
	public string TextoSets;
	public string IdSets;

	//botones
//	public Canvas CanvasD;
	public GameObject PanelBotones;

	Text TextoBoton;
	public Font myFont;
	public string[] NombresTxt;
	//
	public int NroSet;

	public Texture textura1;
	public Texture textura2;
	public Material pepematerial;
	public Material titomaterial;

	GameObject SMData;
	SceneManager ManagerScript ;

	public GameObject SM;

	void Start(){
		NroSet = 0;

		//txt = "Hello World";
//		CantSetsMat = Scene.CatMatMax;

//		GameObject Muebles = GameObject.FindWithTag ("Muebles");
//		SceneObject Scene = Muebles.GetComponent<SceneObject>();
//		CantSetsMat = Scene.CatMatMax;
//		Debug.Log ("CantSetsMat" +CantSetsMat);

		/*       
		GameObject thePlayer = GameObject.Find("ThePlayer");
        PlayerScript playerScript = thePlayer.GetComponent<PlayerScript>();
        playerScript.Health -= 10.0f;*/




	}

	void Update()
	{
		/*GameObject SMData = GameObject.Find("SceneManager");
		SceneManager ManagerScript = SMData.GetComponent<SceneManager> ();
		CantSetsMat = ManagerScript.NroMatSM;
		*/
		CantSetsMat = SM.GetComponent<SceneManager>().NroSetsSelected;
		IdSetsS = SM.GetComponent<SceneManager>().idSelected;
		//TextoSets = SM.GetComponent<SceneManager>().nombreMueble;
		TextoSets = SM.GetComponent<SceneManager> ().nombreSet;

		if (IdSets != IdSets) {
				
			GetNroMat();
		
		}
		/*if (PanelBotones.activeSelf == true) {
			GetNroMat();
			Debug.Log("Active/Panel");
		}*/


	}

	public string[] CatMatName;

	public void GetNroMat(){


		//--Destruye botones 
		foreach(Component child in PanelBotones.GetComponentsInChildren<Button>())
		{
			//Debug.Log ("Botones Conteo Delete");
			//Destroy(child);
			DestroyObject(child.gameObject);
		}

		//Debug.Log ("Entra en GetNroMat");

	/*	GameObject Mueble = GameObject.FindWithTag ("Muebles");
		SceneObject SceneScript = Mueble.GetComponent<SceneObject>();
		CantSetsMat = SceneScript.CatMatMax;
*/
		CantSetsMat = SM.GetComponent<SceneManager>().NroSetsSelected;
		IdSetsS = SM.GetComponent<SceneManager>().idSelected;
	//	Debug.Log ("IDSelected" +IdSets);
		//TextoSets = SM.GetComponent<SceneManager>().nombreMueble;
		TextoSets = SM.GetComponent<SceneManager> ().nombreSet;

		Debug.Log ("TextoSelected" +TextoSets);

		//CatMatName = new string[SM.GetComponent<SceneManager> ().CatalogoMaterialesFinalSM.Length];
		//for(int i=0; i<CatMatName.Length;i++){
		//	CatMatName[i] = SM.GetComponent<SceneManager>().CatalogoMaterialesFinalSM[i].nombreSets;
		//}

		SetsColores();

	
	}




	public void SetsColores(){


//		GameObject Mueble = GameObject.FindWithTag ("Muebles");
		//GameObject Mueble = GameObject.FindWithTag ("Object");
		
//		SceneObject SceneScript = Mueble.GetComponent<SceneObject>();
		//CantSetsMat = SceneScript.CatMatMax;

		//IdSets = SceneScript.idItem;

//		TextoSets = SceneScript.TextoSets;

		//Debug.Log ("BOTON ");

		for(int i = 0; i <CantSetsMat; i ++) {

		//	Debug.Log ("BOTON " + i);
			// Create the EventSystem to handle events in the first place (this is required for the events on the canvas and only one is enough even if you have multiple canvases)
			new GameObject ("EventSystem", typeof(EventSystem), typeof(StandaloneInputModule));
	
			// Create button
			//Button button = (new GameObject ("Button", typeof(CanvasRenderer), typeof(Image), typeof(Button), typeof(Text))).GetComponent<Button> ();
			Button button = (new GameObject ("Button", typeof(CanvasRenderer), typeof(Button), typeof(Text), typeof(BoxCollider2D), typeof( ButtonSetsValue))).GetComponent<Button> ();
			// Assign the button to the canvas using hierarchy
			button.transform.parent = PanelBotones.transform;

			//button.tag = IdSets;
			// Add function to trigger on click

			// Place the button at the middle
			RectTransform buttontransform = button.GetComponent<RectTransform> ();
			buttontransform.localPosition = Vector2.zero;

		
			Text TextoBoton = button.GetComponent<Text>();
			if (CantSetsMat >= 15){
				TextoBoton.fontSize = 10;
			}
			//TextoBoton.text = TextoSets;
			//TextoBoton.text = "HOLA";
			if(SM.GetComponent<SceneManager>().NombresA.Length>=i){
				TextoBoton.text = SM.GetComponent<SceneManager>().NombresA[i];
			}else{
				TextoBoton.text = "ERROR...";
			}

			button.GetComponent<ButtonSetsValue>().value = i;


			TextoBoton.font = myFont;
			//---
			TextoBoton.verticalOverflow= VerticalWrapMode.Overflow;

			//---
			BoxCollider2D BoxButton = button.GetComponent<BoxCollider2D>();
			button.GetComponent<Button>().onClick.AddListener(() => ChangeSet(button.GetComponent<ButtonSetsValue>().value));
			button.transition = Selectable.Transition.ColorTint;

			button.targetGraphic = TextoBoton;


			ColorBlock cb = button.colors;
			cb.highlightedColor = new Color( 0.5f, 0.5f, 0.5f, 1f);
       		button.colors = cb;


			//ButtonSetsValue bc = button.AddComponent<ButtonSetsValue>();
			//bc.Setup(name, 0);


		}

	}




	public void ChangeSet(int valor)
	{

		SetSeleccionado = valor;
		//CatMatToUse
		SM.GetComponent<SceneManager> ().ChangeMaterialsSet (valor);




	}


}