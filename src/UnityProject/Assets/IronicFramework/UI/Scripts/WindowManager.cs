﻿using UnityEngine;
using System.Collections;

public class WindowManager : MonoBehaviour {

    public static WindowManager Instance
    {
        get;
        protected set;
    }

    public GameObject AlertPrefab;
    public WindowManager()
    {
        Instance = this;
    }

    protected Canvas Canvas;
	// Use this for initialization
	void Awake () {
        Canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void ShowProgress(string keyName, float progressValue)
    {
        var child = Canvas.transform.FindChild(keyName);
        child.GetComponent<ProgressBar>().currentValue = progressValue;
        child.gameObject.SetActive(true);
    }

    internal void ShowAlert(string keyName, string title, string message, string button = null)
    {
        Debug.LogWarning(string.Format("Alert: {0} - Title: {1} - Button: {2} - Msg: {3}", keyName, title, message, button), this);
        var child = Canvas.transform.FindChild(keyName);
        if (child == null || string.IsNullOrEmpty(keyName))
        {
            GameObject children = GameObject.Instantiate(AlertPrefab) as GameObject;
            if (!string.IsNullOrEmpty(keyName))
            {
                children.name = keyName;
            }
            children.GetComponent<RectTransform>().SetParent(Canvas.transform);
            children.GetComponent<RectTransform>().localPosition = new Vector3();
            children.GetComponent<WindowAlert>().Show(title, message, button);
        }
        else
        {
            Debug.Log(child.name, child);
            child.GetComponent<WindowAlert>().Show(title, message, button);
        }
    }

    internal void HideProgress(string keyName)
    {
        var child = Canvas.transform.FindChild(keyName);
        child.gameObject.SetActive(false);
    }
}
