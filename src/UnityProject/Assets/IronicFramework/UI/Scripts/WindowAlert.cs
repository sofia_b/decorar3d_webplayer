﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WindowAlert : MonoBehaviour {
    protected Text Title;
    protected Button Button;
    protected Text Content;

    private bool Initialized = false;
	// Use this for initialization
	void Start () {
        Init();
	}

    private void Init()
    {
        Content = this.transform.FindChild("Content").GetComponent<Text>();
        Button = this.transform.FindChild("Button").GetComponent<Button>();
        Title = this.transform.FindChild("Title").GetComponentInChildren<Text>();
        Initialized = true;
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    public void Show(string title, string message, string button = null)
    {
        if (!Initialized) Init();
        Title.text = title;
        Content.text = message;
        if (!string.IsNullOrEmpty(button))
        {
            Button.GetComponentInChildren<Text>().text = button;
        }


    }
    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
    public void Close()
    {
        Destroy(this, 1);
        this.gameObject.SetActive(false);
    }
}
