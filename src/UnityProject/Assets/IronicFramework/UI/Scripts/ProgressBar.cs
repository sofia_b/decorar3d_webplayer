﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

    public Image progressValueImage;
    public float currentValue;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        progressValueImage.fillAmount = currentValue / 100;
	}
}
