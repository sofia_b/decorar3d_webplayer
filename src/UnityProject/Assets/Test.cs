﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

    protected ObjReader reader;
	public string RootUrl;// = "http://www.ironicnet.com/Down/Model/WebPlayer/";
    public string ModelUrl = "";
    public bool useMtl;
    public Material DefaultMaterial;
    public Material TransparentMaterial;
	// Use this for initialization
	void Start () {
        reader = this.GetComponent<ObjReader>();
        reader.useMTLFallback = useMtl;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    private ObjReader.ObjData objData;
    private bool loadingObject;

    [ContextMenu("Download")]
    public void StartDownload()
    {
        StartCoroutine(Download());
    }
    protected IEnumerator Download()
    {
        loadingObject = true;
        try
        {
          //  Debug.Log("Downloading (" + useMtl.ToString() + ":" + RootUrl + ModelUrl);
            objData = ObjReader.use.ConvertFileAsync(RootUrl + ModelUrl, useMtl, DefaultMaterial, TransparentMaterial);
        }
        catch (UnityException ex)
        {
            Debug.LogError(ex);
        }
        while (!objData.isDone)
        {
            Debug.Log(objData.progress);
            yield return null;
        }
        loadingObject = false;
        if (objData == null || objData.gameObjects == null)
        {
            Debug.LogError("Error");
            yield return false;
        }
        else
        {

            GameObject obj = objData.gameObjects.Length>1 ? new GameObject() : null;
            for (int i = 0; i < objData.gameObjects.Length; i++)
            {
                if (objData.gameObjects.Length > 1)
                {
                    objData.gameObjects[i].transform.parent = obj.transform;
                }
                else
                {
                    obj = objData.gameObjects[i];
                }
            }

            obj.transform.position = Vector3.zero;
        }
    }
	
}
