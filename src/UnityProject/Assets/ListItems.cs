using UnityEngine;
using System;
using System.Collections.Generic;

public class ListItems
{
	public string idI;
	public string Nombre;
	public Vector3 Posicion;
	public Vector3 Rotacion;
	public string Width;
	public string Height;
	public string Deep;
	public string Cat;

	public ListItems (string idit,string nomb,float px,float py,float pz,float rx,float ry,float rz, string width, string height, string deep, string catid){
		idI = idit;
		Nombre = nomb;
		Posicion.x = px;
		Posicion.y = py;
		Posicion.z = pz;
		Rotacion.x = rx;
		Rotacion.y = ry;
		Rotacion.z = rz;
		Width = width;
		Height = height;
		Deep = deep;
		Cat = catid;
	}
}


