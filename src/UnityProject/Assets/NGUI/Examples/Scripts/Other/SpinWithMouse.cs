using UnityEngine;

[AddComponentMenu("NGUI/Examples/Spin With Mouse")]
public class SpinWithMouse : MonoBehaviour
{
    public Transform target;
    public float speed = 1f;

    SceneManager sceneManager;
    Transform mTrans;

    protected bool WasDragging;
    protected bool IsDragging;

    void Start()
    {
        sceneManager = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<SceneManager>();
        mTrans = transform;
    }
    /*void LateUpdate()
    {
        if (WasDragging && !IsDragging)
        {
            Transform targetObject = GetTargetObject();
            if (targetObject != null)
            {
                Debug.Log("Finishing rotation");
                targetObject.GetComponent<SceneObject>().RotationOperation.Finish();
                this.sceneManager.SetMode(ActionMode.None);
            }
            WasDragging = false;
        }

        if (IsDragging && Input.GetMouseButtonUp(0)) {
            IsDragging = false;
        }
    }*/

    void OnDrag(Vector2 delta)
    {
        IsDragging = true;
        UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;

        Transform targetObject = GetTargetObject();
        if (targetObject != null)
        {
            SceneObject so = targetObject.GetComponent<SceneObject>();
            sceneManager.Mode = ActionMode.Rotating;
            if (so.TranslationOperation.IsActive) so.TranslationOperation.Cancel();
            if (!so.RotationOperation.IsActive) so.RotationOperation.Start();
            so.RotationOperation.StartAndSet(Quaternion.Euler(0f, -0.5f * delta.x * speed, 0f) * targetObject.localRotation);
            WasDragging = true;
        }
        else
        {
            IsDragging = false;
        }
    }

    private Transform GetTargetObject()
    {
        Transform targetObject = target ?? mTrans;
        return targetObject;
    }


}