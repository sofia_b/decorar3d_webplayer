using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScriptBotonInformacion : MonoBehaviour {
	
	// Start is called just before any of the
	// Update methods is called the first time.
	void Start () {
		
	}
	
	// Update is called every frame, if the
	// MonoBehaviour is enabled.
	void Update () {
	}

	public void ActiveDesactivePanel () {
		if (this.gameObject.activeSelf) {
			this.gameObject.SetActive (false);
		} else {
			this.gameObject.SetActive (true);
		}
	}

}
