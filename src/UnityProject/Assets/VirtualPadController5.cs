﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEngine.UI;

public class VirtualPadController5 : MonoBehaviour, IInputProvider {

    // Use this for initialization
    void Start() {

    }

    public Assets.Scripts.Camera.UserInput currentInput = null;
    public void StartMoveForward5() { IsMovingForward5 = true; }
    public void StartMoveBackward5() { IsMovingBackward5 = true; }
    public void StartMoveRight5() { IsMovingRight5 = true; }
    public void StartMoveLeft5() { IsMovingLeft5 = true; }
    public void StartRotateForward5() { IsRotatingForward5 = true; }
    public void StartRotateBackward5() { IsRotatingBackward5 = true; }
    public void StartRotateRight5() { IsRotatingRight5 = true; }
    public void StartRotateLeft5() { IsRotatingLeft5 = true; }
    public void StartZoomUp5() { IsZoomUp5 = true; }
    public void StartZoomDown5() { IsZoomDown5 = true; }

    public void StopMoveForward5() { IsMovingForward5 =false; }
    public void StopMoveBackward5() { IsMovingBackward5 =false; }
    public void StopMoveRight5() { IsMovingRight5 =false; }
    public void StopMoveLeft5() { IsMovingLeft5 =false; }
    public void StopRotateForward5() { IsRotatingForward5 =false; }
    public void StopRotateBackward5() { IsRotatingBackward5 =false; }
    public void StopRotateRight5() { IsRotatingRight5 =false; }
    public void StopRotateLeft5() { IsRotatingLeft5 =false; }
    public void StopZoomUp5() { IsZoomUp5 =false; }
    public void StopZoomDown5() { IsZoomDown5 =false; }

    public float MovementSpeed5 = 0.9f;
    public float RotationSpeed5 = 1f;

    public bool IsMovingForward5;
    public bool IsMovingBackward5;
    public bool IsMovingRight5;
    public bool IsMovingLeft5;
    public bool IsRotatingForward5;
    public bool IsRotatingBackward5;
    public bool IsRotatingLeft5;
    public bool IsRotatingRight5;
    public bool IsZoomUp5;
    public bool IsZoomDown5;
    // Update is called once per frame
    void Update() {
        currentInput = new Assets.Scripts.Camera.UserInput();
        if (IsMovingForward5) {
            currentInput.MovementVerticalAxis = MovementSpeed5;
        }
        else if (IsMovingBackward5) {
            currentInput.MovementVerticalAxis = MovementSpeed5 * -1;
        }
        if (IsMovingRight5) {
            currentInput.MovementHorizontalAxis = MovementSpeed5;
        }
        else if (IsMovingLeft5) {
            currentInput.MovementHorizontalAxis = MovementSpeed5 * -1;
        }

        if (IsRotatingForward5) {
            currentInput.IsRightPressed = true;
            currentInput.PointerVerticalAxis = RotationSpeed5 * -1;
        }
        else if (IsRotatingBackward5) {
            currentInput.IsRightPressed = true;
            currentInput.PointerVerticalAxis = RotationSpeed5;
        }
        if (IsRotatingRight5) {
            currentInput.IsRightPressed = true;
            currentInput.PointerHorizontalAxis = RotationSpeed5;
        }
        else if (IsRotatingLeft5) {
            currentInput.IsRightPressed = true;
            currentInput.PointerHorizontalAxis = RotationSpeed5 * -1;
        }

        if (IsZoomDown5) {
            currentInput.IsMiddlePressed = true;
            currentInput.PointerVerticalAxis = MovementSpeed5 * -1;
        }
        else if (IsZoomUp5) {
            currentInput.IsMiddlePressed = true;
            currentInput.PointerVerticalAxis = MovementSpeed5;
        }
        //IsMovingForward = false;
        //IsMovingBackward = false;
        //IsMovingRight = false;
        //IsMovingLeft = false;
        //IsRotatingForward = false;
        //IsRotatingBackward = false;
        //IsRotatingLeft = false;
        //IsRotatingRight = false;
        //IsZoomUp = false;
        //IsZoomDown = false;
    }
    public Assets.Scripts.Camera.UserInput GetInput() {
        return currentInput;
    }
}
